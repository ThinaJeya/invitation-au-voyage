/* 
** invitation-au-voyage_SQL-script.sql
**
** Script SQL de la base de données du projet invitation-au-voyage
**
** Pirathina Boisson
*/

-- Création des tables.

CREATE TABLE Destination
(
    id_destination          SERIAL,
    continent_destination   CHARACTER VARYING(64)   NOT NULL,
    pays_destination        CHARACTER VARYING(64)   NOT NULL,
    ville_destination       CHARACTER VARYING(64)   NOT NULL,
    prix_base_adulte        NUMERIC(13,2)           NOT NULL,
    PRIMARY KEY(id_destination)
);

CREATE TABLE Vol_depart
(
    id_vol_depart       SERIAL,
    ville_depart        CHARACTER VARYING(64)   NOT NULL,
    prix_vol_adulte     NUMERIC(13,2)           NOT NULL,
    id_destination      INTEGER                 NOT NULL,
    PRIMARY KEY(id_vol_depart),
    FOREIGN KEY(id_destination) REFERENCES Destination(id_destination)
);

CREATE TABLE Hotel
(
    id_hotel            SERIAL,
    nom_hotel           CHARACTER VARYING(64)   NOT NULL,
    nom_chambre         CHARACTER VARYING(64)   NOT NULL,
    PRIMARY KEY(id_hotel)
);

CREATE TABLE Hebergement
(
    id_hebergement       SERIAL,
    prix_nuit_adulte     NUMERIC(13,2)          NOT NULL,
    id_destination       INTEGER                NOT NULL,
    id_hotel             INTEGER                NOT NULL,
    PRIMARY KEY(id_hebergement),
    FOREIGN KEY(id_destination) REFERENCES Destination(id_destination),
    FOREIGN KEY(id_hotel) REFERENCES Hotel(id_hotel)
);

CREATE TABLE Tarif_duree_hebergement
(
    id_duree_hebergement    SERIAL,
    duree_hebergement       INTEGER         NOT NULL,
    prix_hebergement        NUMERIC(13,2)   NOT NULL,
    id_destination          INTEGER         NOT NULL,
    PRIMARY KEY(id_duree_hebergement),
    FOREIGN KEY(id_destination) REFERENCES Destination(id_destination)
);

 CREATE TABLE Devis
(
    id_devis                            SERIAL,
    continent_destination               CHARACTER VARYING(64)   NOT NULL,
    pays_destination                    CHARACTER VARYING(64)   NOT NULL,
    nom_client                          CHARACTER VARYING(64)   NOT NULL,
    email_client                        CHARACTER VARYING(64)   NOT NULL,
    date_depart_souhaitee               DATE                    NOT NULL,
    transport_inclu                     BOOLEAN                 NOT NULL,
    ville_depart                        CHARACTER VARYING(64),
    nb_nuits_souhaitees                 INTEGER                 NOT NULL,
    nb_participants_adulte              INTEGER                 NOT NULL,
    nb_participants_enfant              INTEGER                 NOT NULL,
    nb_participants_bebe                INTEGER                 NOT NULL,
    montant_total_base                  NUMERIC(13,2)           NOT NULL,
    montant_total_vol                   NUMERIC(13,2)           NOT NULL,
    montant_total_hebergement           NUMERIC(13,2)           NOT NULL,
    montant_total_TTC                   NUMERIC(13,2)           NOT NULL,
    id_destination                      INTEGER                 NOT NULL,
    PRIMARY KEY(id_devis),
    FOREIGN KEY(id_destination) REFERENCES Destination(id_destination)
);


-- Table Destination.

INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (1, 'Afrique', 'Tanzanie', 'Zanzibar', 100);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (2, 'Afrique', 'Ile Maurice', 'Flic-en-Flac', 100);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (3, 'Afrique', 'Maroc', 'Marrakech', 100);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (4, 'Afrique', 'Egypte', 'Sharm El Sheikh', 100);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (5, 'Afrique', 'Kenya', 'Diani', 100);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (6, 'Amérique', 'République Dominicaine', 'Punta Cana', 150);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (7, 'Amérique', 'Mexique', 'Puerto Morelos', 150);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (8, 'Amérique', 'Etats-Unis', 'New York', 150);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (9, 'Amérique', 'Cuba', 'La Havane', 150);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (10, 'Asie', 'Maldives', 'Atoll Malé Nord', 200);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (11, 'Asie', 'Thaïlande', 'Khao Lak', 200);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (12, 'Asie', 'Ile de la Réunion', 'Saint Gilles Les Bains', 200);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (13, 'Asie', 'Indonésie', 'Bali', 200);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (14, 'Europe', 'Italie', 'Venise', 50);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (15, 'Europe', 'Italie', 'Rome', 50);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (16, 'Europe', 'Espagne', 'Séville', 50);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (17, 'Europe', 'Baléares', 'Majorque', 50);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (18, 'Europe', 'Grèce', 'Santorin', 50);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (19, 'Europe', 'Suisse', 'Grindelwald', 50);
INSERT INTO public.Destination (id_destination, continent_destination, pays_destination, ville_destination, prix_base_adulte)
VALUES (20, 'Océanie', 'Polynésie Française', 'Tahiti', 250);

-- Table Vol_depart.

INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (1, 'Paris', 625, 1);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (2, 'Marseille', 763, 1);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (3, 'Lyon', 813, 1);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (4, 'Nantes', 975, 1);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (5, 'Paris', 1172, 2);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (6, 'Marseille', 1242, 2);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (7, 'Lyon', 1248, 2);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (8, 'Nantes', 1094, 2);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (9, 'Paris', 180, 3);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (10, 'Marseille', 163, 3);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (11, 'Lyon', 230, 3);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (12, 'Nantes', 294, 3);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (13, 'Paris', 449, 4);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (14, 'Marseille', 408, 4);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (15, 'Lyon', 412, 4);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (16, 'Nantes', 864, 4);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (17, 'Paris', 716, 5);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (18, 'Marseille', 742, 5);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (19, 'Lyon', 883, 5);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (20, 'Nantes', 1216, 5);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (21, 'Paris', 723, 6);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (22, 'Marseille', 873, 6);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (23, 'Lyon', 757, 6);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (24, 'Nantes', 856, 6);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (25, 'Paris', 849, 7);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (26, 'Marseille', 839, 7);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (27, 'Lyon', 1099, 7);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (28, 'Nantes', 1146, 7);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (29, 'Paris', 516, 8);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (30, 'Marseille', 658, 8);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (31, 'Lyon', 687, 8);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (32, 'Nantes', 749, 8);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (33, 'Paris', 749, 9);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (34, 'Marseille', 821, 9);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (35, 'Lyon', 928, 9);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (36, 'Nantes', 820, 9);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (37, 'Paris', 980, 10);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (38, 'Marseille', 1121, 10);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (39, 'Lyon', 884, 10);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (40, 'Nantes', 998, 10);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (41, 'Paris', 733, 11);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (42, 'Marseille', 862, 11);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (43, 'Lyon', 744, 11);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (44, 'Nantes', 1019, 11);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (45, 'Paris', 835, 12);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (46, 'Marseille', 880, 12);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (47, 'Lyon', 839, 12);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (48, 'Nantes', 885, 12);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (49, 'Paris', 1027, 13);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (50, 'Marseille', 939, 13);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (51, 'Lyon', 1003, 13);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (52, 'Nantes', 1342, 13);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (53, 'Paris', 147, 14);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (54, 'Marseille', 174, 14);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (55, 'Lyon', 117, 14);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (56, 'Nantes', 226, 14);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (57, 'Paris', 114, 15);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (58, 'Marseille', 79, 15);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (59, 'Lyon', 195, 15);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (60, 'Nantes', 170, 15);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (61, 'Paris', 85, 16);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (62, 'Marseille', 171, 16);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (63, 'Lyon', 287, 16);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (64, 'Nantes', 213, 16);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (65, 'Paris', 166, 17);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (66, 'Marseille', 141, 17);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (67, 'Lyon', 87, 17);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (68, 'Nantes', 182, 17);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (69, 'Paris', 406, 18);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (70, 'Marseille', 393, 18);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (71, 'Lyon', 444, 18);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (72, 'Nantes', 417, 18);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (73, 'Paris', 200, 19);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (74, 'Marseille', 225, 19);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (75, 'Lyon', 281, 19);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (76, 'Nantes', 122, 19);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (77, 'Paris', 2820, 20);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (78, 'Marseille', 2510, 20);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (79, 'Lyon', 2812, 20);
INSERT INTO public.Vol_depart (id_vol_depart, ville_depart, prix_vol_adulte, id_destination)
VALUES (80, 'Nantes', 2553, 20);


-- Table Hotel.

INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (1, 'Hôtel Dhow Inn 4*', 'Chambre Double Standard');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (2, 'Hôtel Hilton Mauritius Resort & Spa 5*', 'Chambre Lit King-Size Deluxe');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (3, 'Es Saadi Marrakech Resort Hotel 5*', 'Suite Executive');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (4, 'Coral Sea Holiday Resort & Aqua Park 5*', 'Chambre Standard - Vue sur Jardin ou Piscine');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (5, 'Leopard Beach Resort 4*', 'Chambre Deluxe avec Terrasse');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (6, 'Dreams Royal Beach 5*', 'Hébergement Deluxe - Vue Tropicale');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (7, 'Hôtel Now Jade 5*', 'Suite Junior avec Lit King-Size - Vue Tropicale');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (8, 'Hôtel Manhattan Club 4*', 'Suite Métropolitaine');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (9, 'Hôtel Nacional 5*', 'Suite Junior');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (10, 'Hôtel Angsana Ihuru 5*', 'Villa face à la plage');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (11, 'Pullman Khao Lak Resort', 'Chambre Lit King-Size');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (12, 'Résidence l''Archipel', 'Studio');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (13, 'Best Western Premier Agung 4*', 'Chambre Deluxe Double');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (14, 'Hôtel Aquarius Venice 4*', 'Chambre Double');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (15, 'Hôtel Degli Aranci 4*', 'Chambre Double');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (16, 'Patio de la Cartuja', 'Chambre Standard');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (17, 'Son Caliu Hôtel Spa Oasis 4*', 'Chambre Double Standard');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (18, 'Andronis Arcadia Hotel 5*', 'Suite - Vue sur Mer');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (19, 'Romantik Hotel Schweizerhof 5*', 'Chambre Double Confort');
INSERT INTO public.Hotel (id_hotel, nom_hotel, nom_chambre)
VALUES (20, 'Pearl Resorts', 'Chambre Premium avec Jacuzzi - Vue sur Océan');

-- Table Hebergement.

INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (1, 160, 1, 1);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (2, 98, 2, 2);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (3, 201, 3, 3);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (4, 66, 4, 4);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (5, 62, 5, 5);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (6, 107, 6, 6);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (7, 102, 7, 7);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (8, 79, 8, 8);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (9, 75, 9, 9);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (10, 252, 10, 10);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (11, 60, 11, 11);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (12, 50, 12, 12);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (13, 34, 13, 13);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (14, 119, 14, 14);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (15, 56, 15, 15);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (16, 45, 16, 16);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (17, 97, 17, 17);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (18, 720, 18, 18);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (19, 333, 19, 19);
INSERT INTO public.Hebergement (id_hebergement, prix_nuit_adulte, id_destination, id_hotel)
VALUES (20, 143, 20, 20);

-- Table Tarif duree hebergement.

INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (1, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 1), 1);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (2, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 1), 1);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (3, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 1), 1);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (4, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 1), 1);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (5, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 2), 2);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (6, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 2), 2);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (7, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 2), 2);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (8, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 2), 2);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (9, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 3), 3);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (10, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 3), 3);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (11, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 3), 3);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (12, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 3), 3);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (13, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 4), 4);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (14, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 4), 4);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (15, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 4), 4);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (16, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 4), 4);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (17, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 5), 5);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (18, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 5), 5);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (19, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 5), 5);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (20, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 5), 5);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (21, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 6), 6);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (22, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 6), 6);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (23, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 6), 6);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (24, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 6), 6);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (25, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 7), 7);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (26, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 7), 7);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (27, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 7), 7);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (28, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 7), 7);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (29, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 8), 8);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (30, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 8), 8);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (31, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 8), 8);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (32, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 8), 8);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (33, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 9), 9);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (34, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 9), 9);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (35, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 9), 9);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (36, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 9), 9);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (37, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 10), 10);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (38, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 10), 10);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (39, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 10), 10);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (40, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 10), 10);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (41, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 11), 11);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (42, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 11), 11);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (43, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 11), 11);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (44, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 11), 11);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (45, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 12), 12);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (46, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 12), 12);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (47, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 12), 12);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (48, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 12), 12);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (49, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 13), 13);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (50, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 13), 13);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (51, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 13), 13);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (52, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 13), 13);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (53, 3, (SELECT prix_nuit_adulte * 3 FROM Hebergement WHERE id_destination = 14), 14);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (54, 5, (SELECT prix_nuit_adulte * 5 FROM Hebergement WHERE id_destination = 14), 14);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (55, 8, (SELECT prix_nuit_adulte * 8 FROM Hebergement WHERE id_destination = 14), 14);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (56, 10, (SELECT prix_nuit_adulte * 10 FROM Hebergement WHERE id_destination = 14), 14);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (57, 3, (SELECT prix_nuit_adulte * 3 FROM Hebergement WHERE id_destination = 15), 15);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (58, 5, (SELECT prix_nuit_adulte * 5 FROM Hebergement WHERE id_destination = 15), 15);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (59, 8, (SELECT prix_nuit_adulte * 8 FROM Hebergement WHERE id_destination = 15), 15);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (60, 10, (SELECT prix_nuit_adulte * 10 FROM Hebergement WHERE id_destination = 15), 15);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (61, 3, (SELECT prix_nuit_adulte * 3 FROM Hebergement WHERE id_destination = 16), 16);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (62, 5, (SELECT prix_nuit_adulte * 5 FROM Hebergement WHERE id_destination = 16), 16);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (63, 8, (SELECT prix_nuit_adulte * 8 FROM Hebergement WHERE id_destination = 16), 16);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (64, 10, (SELECT prix_nuit_adulte * 10 FROM Hebergement WHERE id_destination = 16), 16);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (65, 3, (SELECT prix_nuit_adulte * 3 FROM Hebergement WHERE id_destination = 17), 17);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (66, 5, (SELECT prix_nuit_adulte * 5 FROM Hebergement WHERE id_destination = 17), 17);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (67, 8, (SELECT prix_nuit_adulte * 8 FROM Hebergement WHERE id_destination = 17), 17);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (68, 10, (SELECT prix_nuit_adulte * 10 FROM Hebergement WHERE id_destination = 17), 17);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (69, 3, (SELECT prix_nuit_adulte * 3 FROM Hebergement WHERE id_destination = 18), 18);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (70, 5, (SELECT prix_nuit_adulte * 5 FROM Hebergement WHERE id_destination = 18), 18);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (71, 8, (SELECT prix_nuit_adulte * 8 FROM Hebergement WHERE id_destination = 18), 18);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (72, 10, (SELECT prix_nuit_adulte * 10 FROM Hebergement WHERE id_destination = 18), 18);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (73, 3, (SELECT prix_nuit_adulte * 3 FROM Hebergement WHERE id_destination = 19), 19);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (74, 5, (SELECT prix_nuit_adulte * 5 FROM Hebergement WHERE id_destination = 19), 19);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (75, 8, (SELECT prix_nuit_adulte * 8 FROM Hebergement WHERE id_destination = 19), 19);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (76, 10, (SELECT prix_nuit_adulte * 10 FROM Hebergement WHERE id_destination = 19), 19);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (77, 7, (SELECT prix_nuit_adulte * 7 FROM Hebergement WHERE id_destination = 20), 20);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (78, 9, (SELECT prix_nuit_adulte * 9 FROM Hebergement WHERE id_destination = 20), 20);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (79, 11, (SELECT prix_nuit_adulte * 11 FROM Hebergement WHERE id_destination = 20), 20);
INSERT INTO public.Tarif_duree_hebergement (id_duree_hebergement, duree_hebergement, prix_hebergement, id_destination)
VALUES (80, 14, (SELECT prix_nuit_adulte * 14 FROM Hebergement WHERE id_destination = 20), 20);

-- Droits.

REVOKE ALL ON Destination       FROM PUBLIC;
REVOKE ALL ON Vol_depart        FROM PUBLIC;
REVOKE ALL ON Hotel             FROM PUBLIC;
REVOKE ALL ON Hebergement       FROM PUBLIC;
REVOKE ALL ON Tarif_duree_hebergement             FROM PUBLIC;
REVOKE ALL ON Devis             FROM PUBLIC;

GRANT SELECT ON Destination     TO lambda;
GRANT SELECT ON Vol_depart      TO lambda;
GRANT SELECT ON Hotel           TO lambda;
GRANT SELECT ON Hebergement     TO lambda;
GRANT SELECT ON Tarif_duree_hebergement           TO lambda;
GRANT SELECT ON Devis           TO lambda;

GRANT INSERT ON Destination     TO lambda;
GRANT INSERT ON Vol_depart      TO lambda;
GRANT INSERT ON Hotel           TO lambda;
GRANT INSERT ON Hebergement     TO lambda;
GRANT INSERT ON Tarif_duree_hebergement           TO lambda;
GRANT INSERT ON Devis           TO lambda;

GRANT UPDATE ON Destination     TO lambda;
GRANT UPDATE ON Vol_depart      TO lambda;
GRANT UPDATE ON Hotel           TO lambda;
GRANT UPDATE ON Hebergement     TO lambda;
GRANT UPDATE ON Tarif_duree_hebergement           TO lambda;
GRANT UPDATE ON Devis           TO lambda;

GRANT DELETE ON Destination     TO lambda;
GRANT DELETE ON Vol_depart      TO lambda;
GRANT DELETE ON Hotel           TO lambda;
GRANT DELETE ON Hebergement     TO lambda;
GRANT DELETE ON Tarif_duree_hebergement           TO lambda;
GRANT DELETE ON Devis           TO lambda;

GRANT ALL ON Destination        TO postgres;
GRANT ALL ON Vol_depart         TO postgres;
GRANT ALL ON Hotel              TO postgres;
GRANT ALL ON Hebergement        TO postgres;
GRANT ALL ON Tarif_duree_hebergement              TO postgres;
GRANT ALL ON Devis              TO postgres;