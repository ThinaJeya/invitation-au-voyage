package voyage.backinvitationauvoyage.testHotelService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.hebergement.bean.Hebergement;
import voyage.backinvitationauvoyage.hebergement.service.HebergementServiceImpl;
import voyage.backinvitationauvoyage.hotel.HotelException;
import voyage.backinvitationauvoyage.hotel.HotelException.MessageHotelException;
import voyage.backinvitationauvoyage.hotel.bean.Hotel;
import voyage.backinvitationauvoyage.hotel.dao.HotelDaoService;
import voyage.backinvitationauvoyage.hotel.service.HotelServiceImpl;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class HotelServiceTest {

	@Mock
	private HotelDaoService hotelDaoService;
	
	@Mock
	private HebergementServiceImpl hebergementServiceImpl;
	
	@InjectMocks
	private HotelServiceImpl hotelServiceImpl;
	
	static List<Hotel> hotels;
	
	static Hotel hotel;
	
	static Hebergement hebergement;
	
	@BeforeEach
	public void before() {
		hotels = new ArrayList<>();
		
		hotels.add(new Hotel(1, "Hôtel Dhow Inn 4*", "Chambre Double Standard"));
		hotels.add(new Hotel(2, "Hôtel Hilton Mauritius Resort & Spa 5*", "Chambre Lit King-Size Deluxe"));
		hotels.add(new Hotel(3, "Es Saadi Marrakech Resort Hotel 5*", "Suite Executive"));
		hotels.add(new Hotel(4, "Coral Sea Holiday Resort & Aqua Park 5*", "Chambre Standard - Vue sur Jardin ou Piscine"));
		hotels.add(new Hotel(5, "Leopard Beach Resort 4*", "Chambre Deluxe avec Terrasse"));
		
		hotel = new Hotel(1, "Dreams Royal Beach 5*", "Hébergement Deluxe - Vue Tropicale");
		
		hebergement = new Hebergement(1, 160, 1, 1);
	}
	
	@Test
	void afficherHotelParIdTest() throws HotelException {
		assertEquals(hotel.getNomChambre(), afficherHotelParIdTest_casClassique().getNomChambre());
		
		Throwable parametreZero = assertThrows(HotelException.class, () -> afficherHotelParIdTest_parametreZero());
		assertEquals(MessageHotelException.PARAMETRE_INCORRECT.getMessage(), parametreZero.getMessage());
	}
	
	public Hotel afficherHotelParIdTest_casClassique() throws HotelException {
		Mockito.when(hotelDaoService.getOne(ArgumentMatchers.anyInt())).thenReturn(hotel);
		return hotelServiceImpl.afficherHotelParId(1);
	}
	
	public Hotel afficherHotelParIdTest_parametreZero() throws HotelException {
		return hotelServiceImpl.afficherHotelParId(0);
	}
	
	@Test
	void afficherHotelParIdDestinationTest() throws HebergementException, HotelException {
		assertEquals(hotel.getNomChambre(), afficherHotelParIdDestinationTest_casClassique().getNomChambre());
		
		Throwable parametreZero = assertThrows(HotelException.class, () -> afficherHotelParIdDestinationTest_parametreZero());
		assertEquals(MessageHotelException.PARAMETRE_INCORRECT.getMessage(), parametreZero.getMessage());
	}
	
	private Hotel afficherHotelParIdDestinationTest_casClassique() throws HebergementException, HotelException {
		Mockito.when(hebergementServiceImpl.afficherHebergementParIdDestination(ArgumentMatchers.anyInt())).thenReturn(hebergement);
		Mockito.when(hotelDaoService.getOne(ArgumentMatchers.anyInt())).thenReturn(hotel);
		return hotelServiceImpl.afficherHotelParIdDestination(1);
	}
	
	private Hotel afficherHotelParIdDestinationTest_parametreZero() throws HebergementException, HotelException {
		return hotelServiceImpl.afficherHotelParIdDestination(0);
	}
	
	
	
}
