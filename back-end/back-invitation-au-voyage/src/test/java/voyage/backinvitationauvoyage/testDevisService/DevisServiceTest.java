package voyage.backinvitationauvoyage.testDevisService;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import voyage.backinvitationauvoyage.destination.DestinationException;
import voyage.backinvitationauvoyage.destination.bean.Destination;
import voyage.backinvitationauvoyage.destination.service.DestinationServiceImpl;
import voyage.backinvitationauvoyage.devis.DevisException;
import voyage.backinvitationauvoyage.devis.DevisException.MessageDevisException;
import voyage.backinvitationauvoyage.devis.bean.Devis;
import voyage.backinvitationauvoyage.devis.dao.DevisDaoService;
import voyage.backinvitationauvoyage.devis.service.DevisServiceImpl;
import voyage.backinvitationauvoyage.dto.FormulaireDevisDTO;
import voyage.backinvitationauvoyage.dto.PrixDTO;
import voyage.backinvitationauvoyage.dto.SejourDTO;
import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.hebergement.bean.Hebergement;
import voyage.backinvitationauvoyage.hebergement.service.HebergementServiceImpl;
import voyage.backinvitationauvoyage.volDepart.VolDepartException;
import voyage.backinvitationauvoyage.volDepart.bean.VolDepart;
import voyage.backinvitationauvoyage.volDepart.service.VolDepartServiceImpl;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class DevisServiceTest {

	@Mock
	private DevisDaoService devisDaoService;
	
	@Mock
	private DestinationServiceImpl destinationServiceImpl;
	
	@Mock
	private HebergementServiceImpl hebergementServiceImpl;
	
	@Mock
	private VolDepartServiceImpl volDepartServiceImpl;
	
	@InjectMocks
	private DevisServiceImpl devisServiceImpl;
	
	static List<Devis> listeDevis;
	
	static Devis devis;
	
	static SejourDTO sejourDTO;
	
	static Destination destination;
	
	static Hebergement hebergement;
	
	static VolDepart volDepart;
	
	static FormulaireDevisDTO formulaireDevisDTO;
	
	@BeforeEach
	public void before() {
		listeDevis = new ArrayList<>();
		
		listeDevis.add(new Devis(1, "Afrique", "Tanzanie", "Kiwi Smith", "kiwi.s@gmail.com", new Date(2021-05-21), true, "Nantes", 14, 4, 2, 0, 500.0f, 4065.0f, 11200.0f, 15765.0f, 1));
		listeDevis.add(new Devis(2, "Europe", "Espagne", "Kiwi Smith", "k.s@gmail.com", new Date(2021-05-21), true, "Lyon", 9, 2, 0, 0, 200.0f, 568.0f, 340.0f, 1500.0f, 16));
		listeDevis.add(new Devis(3, "Océanie", "Polynésie Française", "Kiwi Smith", "kiwi.s@gmail.com", new Date(2021-05-21), true, "Nantes", 9, 4, 0, 0, 200.0f, 5680.0f, 3400.0f, 15000.0f, 20));
		listeDevis.add(new Devis(4, "Amérique", "République Dominicaine", "Kiwi Smith", "kiwi.s@gmail.com", new Date(2021-05-21), true, "Paris", 13, 2, 0, 0, 400.0f, 4680.0f, 3400.0f, 19000.0f, 6));
		listeDevis.add(new Devis(5, "Afrique", "Tanzanie", "Kiwi Smith", "kiwi.s@gmail.com", new Date(2021-05-21), true, "Nantes", 7, 2, 0, 0, 200.0f, 568.0f, 340.0f, 1500.0f, 1));

		devis = new Devis(1, "Afrique", "Tanzanie", "Kiwi Smith", "kiwi.s@gmail.com", new Date(2021-05-21), true, "Nantes", 7, 2, 0, 0, 200.0f, 568.0f, 340.0f, 1500.0f, 1);
		
		sejourDTO = new SejourDTO(1, true, "Paris", 7, 2, 0);
		
		destination = new Destination(1, "Afrique", "Tanzanie", "Zanzibar", 100);
		
		volDepart = new VolDepart(1, "Paris", 625, 1);
		
		hebergement = new Hebergement(1, 160, 1, 1);

		formulaireDevisDTO = new FormulaireDevisDTO(1, "Afrique", "Tanzanie", new Date(2021-05-21), "Kiwi Smith", "kiwi.s@gmail.com", 14, 2, 2, 0, true, "Nantes");
	}
	
	@Test
	void afficherTousLesDevisTest() {
		assertEquals(listeDevis, afficherTousLesDevisTest_casClassique());
	}
	
	private List<Devis> afficherTousLesDevisTest_casClassique(){
		Mockito.when(devisDaoService.findAll()).thenReturn(listeDevis);
		return devisServiceImpl.afficherTousLesDevis();
	}
	
	@Test
	void afficherDevisParIdTest() throws DevisException {
		assertEquals(devis, afficherDevisParIdTest_casClassique());
		
		Throwable parametreZero = assertThrows(DevisException.class, () -> afficherDevisParIdTest_parametreZero());
		assertEquals(MessageDevisException.PARAMETRE_INCORRECT.getMessage(), parametreZero.getMessage());
	}
	
	private Devis afficherDevisParIdTest_casClassique() throws DevisException {
		Mockito.when(devisDaoService.getOne(ArgumentMatchers.anyInt())).thenReturn(devis);
		return devisServiceImpl.afficherDevisParId(1);
	}
	
	private Devis afficherDevisParIdTest_parametreZero() throws DevisException {
		return devisServiceImpl.afficherDevisParId(0);
	}
	
	@Test
	void calculPrixSejourAPartirDeTest() throws DestinationException, HebergementException, DevisException, VolDepartException {
		assertNotNull(calculPrixSejourAPartirDeTest_casClassique());
		
		Throwable sejourDTONull = assertThrows(DevisException.class, () -> calculPrixSejourAPartirDeTest_sejourDTONull());
		assertEquals(MessageDevisException.SEJOURDTO_INCORRECT.getMessage(), sejourDTONull.getMessage());
		
		Throwable idDestinationZero = assertThrows(DevisException.class, () -> calculPrixSejourAPartirDeTest_idDestinationZero());
		assertEquals(MessageDevisException.SEJOURDTO_INCORRECT.getMessage(), idDestinationZero.getMessage());
		
		Throwable transportFalse = assertThrows(DevisException.class, () -> calculPrixSejourAPartirDeTest_transportFalse());
		assertEquals(MessageDevisException.SEJOURDTO_INCORRECT.getMessage(), transportFalse.getMessage());
		
		Throwable nbNuitsZero = assertThrows(DevisException.class, () -> calculPrixSejourAPartirDeTest_nbNuitsZero());
		assertEquals(MessageDevisException.SEJOURDTO_INCORRECT.getMessage(), nbNuitsZero.getMessage());
		
		Throwable nbParticipantsAdulteZero = assertThrows(DevisException.class, () -> calculPrixSejourAPartirDeTest_nbParticipantsAdulteZero());
		assertEquals(MessageDevisException.SEJOURDTO_INCORRECT.getMessage(), nbParticipantsAdulteZero.getMessage());
		
	}
	
	@Test
	void calculPrixSejourAPartirDeTest_calculPrixBase() {
		Throwable destinationNonExistante = assertThrows(DevisException.class, () -> calculPrixSejourAPartirDeTest_destinationNonExistante());
		assertEquals(MessageDevisException.PARAMETRE_METHODE_INTERNE_INCORRECT.getMessage(), destinationNonExistante.getMessage());
	}
	
	private PrixDTO calculPrixSejourAPartirDeTest_casClassique() throws DestinationException, HebergementException, DevisException, VolDepartException {
		Mockito.when(destinationServiceImpl.afficherDestinationParId(ArgumentMatchers.anyInt())).thenReturn(destination);
		Mockito.when(hebergementServiceImpl.afficherHebergementParId(ArgumentMatchers.anyInt())).thenReturn(hebergement);
		Mockito.when(volDepartServiceImpl.afficherVolDepartParIdDestinationEtVille(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString())).thenReturn(volDepart);
		
		return devisServiceImpl.calculPrixSejourAPartirDe(sejourDTO);
	}
	
	private PrixDTO calculPrixSejourAPartirDeTest_sejourDTONull() throws DestinationException, HebergementException, DevisException, VolDepartException {
		return devisServiceImpl.calculPrixSejourAPartirDe(null);
	}
	
	private PrixDTO calculPrixSejourAPartirDeTest_idDestinationZero() throws DestinationException, HebergementException, DevisException, VolDepartException {
		sejourDTO.setIdDestination(0);
		return devisServiceImpl.calculPrixSejourAPartirDe(sejourDTO);	
	}
	
	private PrixDTO calculPrixSejourAPartirDeTest_transportFalse() throws DestinationException, HebergementException, DevisException, VolDepartException {
		sejourDTO.setIdDestination(1);
		sejourDTO.setTransport(false);
		return devisServiceImpl.calculPrixSejourAPartirDe(sejourDTO);
	}
	
	private PrixDTO calculPrixSejourAPartirDeTest_nbNuitsZero() throws DestinationException, HebergementException, DevisException, VolDepartException {
		sejourDTO.setTransport(true);
		sejourDTO.setNbNuits(0);
		return devisServiceImpl.calculPrixSejourAPartirDe(sejourDTO);
	}
	
	private PrixDTO calculPrixSejourAPartirDeTest_nbParticipantsAdulteZero() throws DestinationException, HebergementException, DevisException, VolDepartException {
		sejourDTO.setNbNuits(7);
		sejourDTO.setNbParticipantsAdulte(0);
		return devisServiceImpl.calculPrixSejourAPartirDe(sejourDTO);
	}
	
	private PrixDTO calculPrixSejourAPartirDeTest_destinationNonExistante() throws DestinationException, HebergementException, DevisException, VolDepartException {
		sejourDTO.setIdDestination(56);
		return devisServiceImpl.calculPrixSejourAPartirDe(sejourDTO);
	}
	
	@Test
	void afficherDevisEnregistreTest() throws DevisException {
		assertEquals(listeDevis.get(0).getEmailClient(), afficherDevisEnregistreTest_casClassique().getEmailClient());
		
		assertEquals(listeDevis.get(1).getEmailClient(), afficherDevisEnregistreTest_casClassique2().getEmailClient());
		
		Throwable continentVide = assertThrows(DevisException.class, () -> afficherDevisEnregistreTest_continentVide());
		assertEquals(MessageDevisException.PARAMETRE_INCORRECT.getMessage(), continentVide.getMessage());
	}
	
	private Devis afficherDevisEnregistreTest_casClassique() throws DevisException {
		Mockito.when(devisDaoService.findAll()).thenReturn(listeDevis);
		return devisServiceImpl.afficherDevisEnregistre(formulaireDevisDTO.getContinentDestination(), formulaireDevisDTO.getPaysDestination(), formulaireDevisDTO.getIdDestination(), formulaireDevisDTO.getNomClient());
	}
	
	private Devis afficherDevisEnregistreTest_casClassique2() throws DevisException {
		Mockito.when(devisDaoService.findAll()).thenReturn(listeDevis);
		return devisServiceImpl.afficherDevisEnregistre("Europe", "Espagne", 16, "Kiwi Smith");
	}
	
	private Devis afficherDevisEnregistreTest_continentVide() throws DevisException {
		return devisServiceImpl.afficherDevisEnregistre(" ", formulaireDevisDTO.getPaysDestination(), formulaireDevisDTO.getIdDestination(), formulaireDevisDTO.getNomClient());
	}
}
