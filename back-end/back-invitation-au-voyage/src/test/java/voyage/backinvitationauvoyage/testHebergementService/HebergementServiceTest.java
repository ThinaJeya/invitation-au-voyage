package voyage.backinvitationauvoyage.testHebergementService;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.hebergement.HebergementException.MessageHebergementException;
import voyage.backinvitationauvoyage.hebergement.bean.Hebergement;
import voyage.backinvitationauvoyage.hebergement.dao.HebergementDaoService;
import voyage.backinvitationauvoyage.hebergement.service.HebergementServiceImpl;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class HebergementServiceTest {

	@Mock
	private HebergementDaoService hebergementDaoService;
	
	@InjectMocks
	private HebergementServiceImpl hebergementServiceImpl;
	
	static List<Hebergement> hebergements;
	
	static Hebergement hebergement;
	
	@BeforeEach
	public void before() {
		hebergements = new ArrayList<>();
		
		hebergements.add(new Hebergement(1, 160, 1, 1));
		hebergements.add(new Hebergement(2, 98, 2, 2));
		hebergements.add(new Hebergement(3, 201, 3, 3));
		hebergements.add(new Hebergement(4, 66, 4, 4));
		
		hebergement = new Hebergement(1, 160, 1, 1);
	}
	
	@Test
	void afficherTouslesHebergementsTest() {
		assertNotNull(afficherTouslesHebergementsTest_casClassique());
		assertEquals(hebergements, afficherTouslesHebergementsTest_casClassique());
	} 
	
	private List<Hebergement> afficherTouslesHebergementsTest_casClassique(){
		Mockito.when(hebergementDaoService.findAll()).thenReturn(hebergements);
		return hebergementServiceImpl.afficherTouslesHebergements();
	}
	
	@Test
	void afficherHebergementParIdTest() throws HebergementException {
		
		assertEquals(hebergement, afficherHebergementParIdTest_casClassique());
		
		Throwable parametreZero = assertThrows(HebergementException.class, () -> afficherHebergementParIdTest_parametreIncorrect());
		assertEquals(MessageHebergementException.PARAMETRE_INCORRECT.getMessage(), parametreZero.getMessage());
		
	}
	
	private Hebergement afficherHebergementParIdTest_casClassique() throws HebergementException {
		Mockito.when(hebergementDaoService.getOne(ArgumentMatchers.anyInt())).thenReturn(hebergement);
		
		return hebergementServiceImpl.afficherHebergementParId(1);
	}
	
	private Hebergement afficherHebergementParIdTest_parametreIncorrect() throws HebergementException {
		return hebergementServiceImpl.afficherHebergementParId(0);
	}
	
	@Test
	void afficherHebergementParIdDestinationTest() throws HebergementException {
		assertNotNull(afficherHebergementParIdDestinationTest_casClassique());
		
		Throwable listeHebergementsNull = assertThrows(HebergementException.class, () -> afficherHebergementParIdDestinationTest_listeNull());
		assertEquals(MessageHebergementException.PARAMETRE_INCORRECT.getMessage(), listeHebergementsNull.getMessage());
		
		Throwable parametreZero = assertThrows(HebergementException.class, () -> afficherHebergementParIdDestinationTest_parametreZero());
		assertEquals(MessageHebergementException.PARAMETRE_INCORRECT.getMessage(), parametreZero.getMessage());
	}
	
	private Hebergement afficherHebergementParIdDestinationTest_casClassique() throws HebergementException {
		Mockito.when(hebergementDaoService.findAll()).thenReturn(hebergements);
		
		return hebergementServiceImpl.afficherHebergementParIdDestination(1);
	}
	
	private Hebergement afficherHebergementParIdDestinationTest_listeNull() throws HebergementException {
		Mockito.when(hebergementDaoService.findAll()).thenReturn(null);
		
		return hebergementServiceImpl.afficherHebergementParIdDestination(1);
	}
	
	private Hebergement afficherHebergementParIdDestinationTest_parametreZero() throws HebergementException {
		Mockito.when(hebergementDaoService.findAll()).thenReturn(hebergements);
		
		return hebergementServiceImpl.afficherHebergementParIdDestination(0);
	}
}
