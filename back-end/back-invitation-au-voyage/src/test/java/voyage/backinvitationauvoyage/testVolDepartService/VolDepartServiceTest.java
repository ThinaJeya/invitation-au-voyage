package voyage.backinvitationauvoyage.testVolDepartService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import voyage.backinvitationauvoyage.volDepart.VolDepartException;
import voyage.backinvitationauvoyage.volDepart.VolDepartException.MessageVolDepartException;
import voyage.backinvitationauvoyage.volDepart.bean.VolDepart;
import voyage.backinvitationauvoyage.volDepart.dao.VolDepartDaoService;
import voyage.backinvitationauvoyage.volDepart.service.VolDepartServiceImpl;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class VolDepartServiceTest {

	@Mock
	private VolDepartDaoService volDepartDaoService;
	
	@InjectMocks
	private VolDepartServiceImpl volDepartServiceImpl;
	
	static List<VolDepart> volsDepart;
	
	static VolDepart volDepart;
	
	@BeforeEach
	public void before() {
		volsDepart = new ArrayList<>();
		
		volsDepart.add(new VolDepart(1, "Paris", 625, 1));
		volsDepart.add(new VolDepart(2, "Marseille", 763, 1));
		volsDepart.add(new VolDepart(3, "Lyon", 813, 1));
		volsDepart.add(new VolDepart(4, "Nantes", 975, 1));
		volsDepart.add(new VolDepart(5, "Paris", 1172, 2));
		
		volDepart = new VolDepart(1, "Paris", 625, 1);
	}
	
	@Test
	void afficherVolDepartParIdTest() throws VolDepartException {
		assertEquals(volDepart, afficherVolDepartParIdTest_casClassique());
		
		Throwable parametreZero = assertThrows(VolDepartException.class, () -> afficherVolDepartParIdTest_parametreZero());
		assertEquals(MessageVolDepartException.PARAMETRE_INCORRECT.getMessage(), parametreZero.getMessage());
	}
	
	private VolDepart afficherVolDepartParIdTest_casClassique() throws VolDepartException{
		Mockito.when(volDepartDaoService.getOne(ArgumentMatchers.anyInt())).thenReturn(volDepart);
		return volDepartServiceImpl.afficherVolDepartParId(1);
	}
	
	private VolDepart afficherVolDepartParIdTest_parametreZero() throws VolDepartException {
		return volDepartServiceImpl.afficherVolDepartParId(0);
	}
	
	@Test
	void afficherVolDepartParIdDestinationTest() throws VolDepartException {
		assertFalse(afficherVolDepartParIdDestinationTest_casClassique().isEmpty());
		
		Throwable listeNull = assertThrows(VolDepartException.class, () -> afficherVolDepartParIdDestinationTest_ListeNull());
		assertEquals(MessageVolDepartException.PARAMETRE_INCORRECT.getMessage(), listeNull.getMessage());
		
		Throwable parametreZero = assertThrows(VolDepartException.class, () -> afficherVolDepartParIdDestinationTest_parametreZero());
		assertEquals(MessageVolDepartException.PARAMETRE_INCORRECT.getMessage(), parametreZero.getMessage());
	}
	
	private List<VolDepart> afficherVolDepartParIdDestinationTest_casClassique() throws VolDepartException {
		Mockito.when(volDepartDaoService.findAll()).thenReturn(volsDepart);	
		return volDepartServiceImpl.afficherVolDepartParIdDestination(1);
	}
	
	private List<VolDepart> afficherVolDepartParIdDestinationTest_ListeNull() throws VolDepartException{
		Mockito.when(volDepartDaoService.findAll()).thenReturn(null);
		return volDepartServiceImpl.afficherVolDepartParIdDestination(1);
	}
	
	private List<VolDepart> afficherVolDepartParIdDestinationTest_parametreZero() throws VolDepartException {
		return volDepartServiceImpl.afficherVolDepartParIdDestination(0);
	}
	
	@Test
	void afficherVolDepartParIdDestinationEtVilleTest() throws VolDepartException {
		assertNotNull(afficherVolDepartParIdDestinationEtVilleTest_casClassique());
		
		Throwable listeVide = assertThrows(VolDepartException.class, () -> afficherVolDepartParIdDestinationEtVilleTest_listeVide());
		assertEquals(MessageVolDepartException.PARAMETRE_INTERNE_INCORRECT.getMessage(), listeVide.getMessage());
		
		Throwable villeDepartVide = assertThrows(VolDepartException.class, () -> afficherVolDepartParIdDestinationEtVilleTest_villeDepartVide());
		assertEquals(MessageVolDepartException.PARAMETRE_INCORRECT.getMessage(), villeDepartVide.getMessage());
		
		Throwable idZero = assertThrows(VolDepartException.class, () -> afficherVolDepartParIdDestinationEtVilleTest_idZero());
		assertEquals(MessageVolDepartException.PARAMETRE_INCORRECT.getMessage(), idZero.getMessage());
	}
	
	private VolDepart afficherVolDepartParIdDestinationEtVilleTest_casClassique() throws VolDepartException {
		Mockito.when(volDepartDaoService.findAll()).thenReturn(volsDepart);		
		return volDepartServiceImpl.afficherVolDepartParIdDestinationEtVille(1, "Nantes");
	}
	
	private VolDepart afficherVolDepartParIdDestinationEtVilleTest_listeVide() throws VolDepartException {		
		Mockito.when(volDepartDaoService.findAll()).thenReturn(volsDepart);
		return volDepartServiceImpl.afficherVolDepartParIdDestinationEtVille(6, "Nantes");
	}
	
	private VolDepart afficherVolDepartParIdDestinationEtVilleTest_villeDepartVide() throws VolDepartException {
		return volDepartServiceImpl.afficherVolDepartParIdDestinationEtVille(1, " ");
	}
	
	private VolDepart afficherVolDepartParIdDestinationEtVilleTest_idZero() throws VolDepartException {
		return volDepartServiceImpl.afficherVolDepartParIdDestinationEtVille(0, "Nantes");
	}
}
