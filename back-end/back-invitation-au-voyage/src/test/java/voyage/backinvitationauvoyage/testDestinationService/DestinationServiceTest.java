package voyage.backinvitationauvoyage.testDestinationService;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import voyage.backinvitationauvoyage.destination.DestinationException;
import voyage.backinvitationauvoyage.destination.DestinationException.MessageDestinationException;
import voyage.backinvitationauvoyage.destination.bean.Destination;
import voyage.backinvitationauvoyage.destination.dao.DestinationDaoService;
import voyage.backinvitationauvoyage.destination.service.DestinationServiceImpl;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class DestinationServiceTest {

	@Mock
	private DestinationDaoService destinationDaoService;
	
	@InjectMocks
	private DestinationServiceImpl destinationServiceImpl;
	
	
	static List<Destination> destinations;
	
	static Destination destination;
	
	@BeforeEach
	public void before() {
		
		destinations = new ArrayList<Destination>();
		
		destinations.add(new Destination(1, "Afrique", "Tanzanie", "Zanzibar", 100));
		destinations.add(new Destination(2, "Afrique", "Ile Maurice", "Flic-en-Flac", 100));
		destinations.add(new Destination(3, "Afrique", "Maroc", "Marrakech", 100));
		
		destination = new Destination(3, "Afrique", "Maroc", "Marrakech", 100);
	}
	
	@Test
	void afficherToutesLesDestinationsTest() {
		assertNotNull(afficherToutesLesDestinations_casClassique());
		assertEquals(destinations, afficherToutesLesDestinations_casClassique());
	}
	
	private List<Destination> afficherToutesLesDestinations_casClassique(){
		Mockito.when(destinationDaoService.findAll()).thenReturn(destinations);		
		return destinationServiceImpl.afficherToutesLesDestinations();
	}
	
	@Test
	void afficherDestinationParIdTest() throws DestinationException {
		assertEquals(destination, afficherDestinationParIdTest_casClassique());
		
		Throwable parametreZero = assertThrows(DestinationException.class, () -> afficherDestinationParIdTest_parametreIncorrect());
		assertEquals(MessageDestinationException.PARAMETRE_INCORRECT.getMessage(), parametreZero.getMessage());
	}
	
	private Destination afficherDestinationParIdTest_casClassique() throws DestinationException {
		Mockito.when(destinationDaoService.getOne(ArgumentMatchers.anyInt())).thenReturn(destination);
		return destinationServiceImpl.afficherDestinationParId(1);
	}
	
	private Destination afficherDestinationParIdTest_parametreIncorrect() throws DestinationException {
		return destinationServiceImpl.afficherDestinationParId(0);
	}
}
