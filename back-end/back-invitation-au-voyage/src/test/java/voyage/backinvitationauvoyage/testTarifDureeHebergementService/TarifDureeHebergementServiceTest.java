package voyage.backinvitationauvoyage.testTarifDureeHebergementService;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import voyage.backinvitationauvoyage.tarifDureeHebergement.bean.TarifDureeHebergement;
import voyage.backinvitationauvoyage.tarifDureeHebergement.dao.TarifDureeHebergementDaoService;
import voyage.backinvitationauvoyage.tarifDureeHebergement.service.TarifDureeHebergementServiceImpl;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class TarifDureeHebergementServiceTest {

	@Mock
	private TarifDureeHebergementDaoService tarifDureeHebergementDaoService;
	
	@InjectMocks
	private TarifDureeHebergementServiceImpl tarifDureeHebergementServiceImpl;
	
	static List<TarifDureeHebergement> tarifDureeHebergements;
	
	@BeforeEach
	public void before() {
		tarifDureeHebergements = new ArrayList<>();
		
		tarifDureeHebergements.add(new TarifDureeHebergement(1, 7, 1120, 1));
		tarifDureeHebergements.add(new TarifDureeHebergement(2, 9, 1440, 1));
		tarifDureeHebergements.add(new TarifDureeHebergement(3, 11, 1760, 1));
		tarifDureeHebergements.add(new TarifDureeHebergement(4, 14, 2240, 1));
		tarifDureeHebergements.add(new TarifDureeHebergement(5, 7, 686, 2));
		tarifDureeHebergements.add(new TarifDureeHebergement(6, 9, 882, 2));
		tarifDureeHebergements.add(new TarifDureeHebergement(7, 11, 1078, 2));
		tarifDureeHebergements.add(new TarifDureeHebergement(8, 14, 1372, 2));
	}
	
	@Test
	void afficherTousLesTarifsDureeHebergementsTest() {
		assertNotNull(afficherTousLesTarifsDureeHebergementsTest_casClassique());
	}
	
	private List<TarifDureeHebergement> afficherTousLesTarifsDureeHebergementsTest_casClassique() {
		Mockito.when(tarifDureeHebergementDaoService.findAll()).thenReturn(tarifDureeHebergements);
		return tarifDureeHebergementServiceImpl.afficherTousLesTarifsDureeHebergements();
	}
}
