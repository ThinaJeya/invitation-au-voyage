package voyage.backinvitationauvoyage.tarifDureeHebergement;

public class TarifDureeHebergementException extends Exception {
	
	public enum MessageTarifDureeHebergementException{
		PARAMETRE_INCORRECT("Le paramètre entré est incorrect");
		
		private String message;

		private MessageTarifDureeHebergementException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}
	
	public TarifDureeHebergementException(String messageException) {
		super(messageException);
	}
}
