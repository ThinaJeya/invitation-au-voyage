package voyage.backinvitationauvoyage.tarifDureeHebergement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import voyage.backinvitationauvoyage.tarifDureeHebergement.TarifDureeHebergementException;
import voyage.backinvitationauvoyage.tarifDureeHebergement.bean.TarifDureeHebergement;
import voyage.backinvitationauvoyage.tarifDureeHebergement.service.TarifDureeHebergementService;

/**
 * Controller de TarifDureeHebergement
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/tarifDureeHebergement")
public class TarifDureeHebergementController {

	@Autowired
	private TarifDureeHebergementService tarifDureeHebergementService;
	
	/**
	 * Controller pour afficher tous les tarifs en fonction des durées de séjour
	 * @return liste des tarifs en fonction des durées de séjour
	 */
	@GetMapping(path = "/tousLesTarifsDureeHebergement")
	public List<TarifDureeHebergement> afficherTousLesTarifsDureeHebergements(){
		return tarifDureeHebergementService.afficherTousLesTarifsDureeHebergements();
	}
	
	/**
	 * Controller pour afficher les tarifs en fonction des durées de séjour associé à l'idDestination entré en paramètre
	 * @param idDestination
	 * @return tarifs en fonction des durées de séjour associé à l'idDestination entré en paramètre
	 * @throws TarifDureeHebergementException
	 */
	@GetMapping(path = "/tarifDureeHebergementSelonIdDestination/{idDestination}")
	public List<TarifDureeHebergement> afficherTarifDureeHebergementSelonIdDestination(@PathVariable int idDestination) throws TarifDureeHebergementException {
		return tarifDureeHebergementService.afficherTarifDureeHebergementSelonIdDestination(idDestination);
	}
}
