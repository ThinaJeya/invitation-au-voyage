package voyage.backinvitationauvoyage.tarifDureeHebergement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import voyage.backinvitationauvoyage.tarifDureeHebergement.TarifDureeHebergementException;
import voyage.backinvitationauvoyage.tarifDureeHebergement.TarifDureeHebergementException.MessageTarifDureeHebergementException;
import voyage.backinvitationauvoyage.tarifDureeHebergement.bean.TarifDureeHebergement;
import voyage.backinvitationauvoyage.tarifDureeHebergement.dao.TarifDureeHebergementDaoService;

/**
 * Couche Implémentation de TarifDureeHebergementService
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class TarifDureeHebergementServiceImpl implements TarifDureeHebergementService{

	@Autowired
	private TarifDureeHebergementDaoService tarifDureeHebergementDaoService;
	
	@Override
	public List<TarifDureeHebergement> afficherTousLesTarifsDureeHebergements() {
		return tarifDureeHebergementDaoService.findAll();
	}

	@Override
	public List<TarifDureeHebergement> afficherTarifDureeHebergementSelonIdDestination(int idDestination) throws TarifDureeHebergementException {
		if(idDestination > 0) {
			return tarifDureeHebergementDaoService.afficherTarifDureeHebergementSelonIdDestination(idDestination);			
		} else {
			throw new TarifDureeHebergementException(MessageTarifDureeHebergementException.PARAMETRE_INCORRECT.getMessage());
		}
	}

	

}
