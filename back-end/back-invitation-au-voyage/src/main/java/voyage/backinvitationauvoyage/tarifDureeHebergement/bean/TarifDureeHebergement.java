package voyage.backinvitationauvoyage.tarifDureeHebergement.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe Bean de TarifDureeHebergement
 * @author Pirathina Jeyakumar
 *
 */
@Entity
@Table(name = "Tarif_duree_hebergement")
public class TarifDureeHebergement {

	/**
	 * identifiant de TarifDureeHebergement
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_duree_hebergement")
	private int idDureeHebergement;
	
	/**
	 * Durée du sejour
	 */
	@Column(name = "duree_hebergement")
	private int dureeHebergement;
	
	/**
	 * Prix du séjour pour la durée associée
	 */
	@Column(name = "prix_hebergement")
	private float prixHebergement;
	
	/**
	 * Identifiant de la destination
	 */
	@Column(name = "id_destination")
	private int idDestination;

	/**
	 * Constructeur de TarifDureeHebergement
	 * @param idDureeHebergement
	 * @param dureeHebergement
	 * @param prixHebergement
	 * @param idDestination
	 */
	public TarifDureeHebergement(int idDureeHebergement, int dureeHebergement, float prixHebergement, int idDestination) {
		super();
		this.idDureeHebergement = idDureeHebergement;
		this.dureeHebergement = dureeHebergement;
		this.prixHebergement = prixHebergement;
		this.idDestination = idDestination;
	}

	/**
	 * Constructeur par défaut de TarifDureeHebergement
	 */
	public TarifDureeHebergement() {
		super();
	}

	/**
	 * Permet d'obtenir l'identifiant de TarifDureeHebergement
	 * @return identifiant de TarifDureeHebergement
	 */
	public int getIdDureeHebergement() {
		return idDureeHebergement;
	}

	/**
	 * Permet de modifier l'identifiant de TarifDureeHebergement
	 * @param idDureeHebergement
	 */
	public void setIdDureeHebergement(int idDureeHebergement) {
		this.idDureeHebergement = idDureeHebergement;
	}

	/**
	 * Permet d'obtenir la durée du sejour
	 * @return durée du sejour
	 */
	public int getDureeHebergement() {
		return dureeHebergement;
	}

	/**
	 * Permet de modifier la durée du sejour
	 * @param dureeHebergement
	 */
	public void setDureeHebergement(int dureeHebergement) {
		this.dureeHebergement = dureeHebergement;
	}

	/**
	 * Permet d'obtenir le prix du séjour pour la durée associée
	 * @return prix du séjour pour la durée associée
	 */
	public float getPrixHebergement() {
		return prixHebergement;
	}

	/**
	 * Permet de modifier le prix du séjour pour la durée associée
	 * @param prixHebergement
	 */
	public void setPrixHebergement(float prixHebergement) {
		this.prixHebergement = prixHebergement;
	}

	/**
	 * Permet d'obtenir l'identifiant de la destination
	 * @return identifiant de la destination
	 */
	public int getIdDestination() {
		return idDestination;
	}

	/**
	 * Permet de modifier l'identifiant de la destination
	 * @param idDestination
	 */
	public void setIdDestination(int idDestination) {
		this.idDestination = idDestination;
	}
}
