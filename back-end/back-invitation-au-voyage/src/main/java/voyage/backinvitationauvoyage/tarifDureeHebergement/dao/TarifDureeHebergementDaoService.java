package voyage.backinvitationauvoyage.tarifDureeHebergement.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import voyage.backinvitationauvoyage.tarifDureeHebergement.bean.TarifDureeHebergement;

/**
 * Couche Dao de TarifDureeHebergementService
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface TarifDureeHebergementDaoService extends JpaRepository<TarifDureeHebergement, Integer>{

	@Query(value = "SELECT t FROM TarifDureeHebergement t WHERE t.idDestination =:idDestination")
	public List<TarifDureeHebergement> afficherTarifDureeHebergementSelonIdDestination(int idDestination);
	
}
