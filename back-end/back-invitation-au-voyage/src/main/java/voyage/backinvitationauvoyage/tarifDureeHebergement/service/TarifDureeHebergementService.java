package voyage.backinvitationauvoyage.tarifDureeHebergement.service;

import java.util.List;

import org.springframework.stereotype.Service;

import voyage.backinvitationauvoyage.tarifDureeHebergement.TarifDureeHebergementException;
import voyage.backinvitationauvoyage.tarifDureeHebergement.bean.TarifDureeHebergement;

/**
 * Couche Service de TarifDureeHebergement
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface TarifDureeHebergementService {

	/**
	 * Service pour afficher tous les tarifs en fonction des durées de séjour
	 * @return liste des tarifs en fonction des durées de séjour
	 */
	public List<TarifDureeHebergement> afficherTousLesTarifsDureeHebergements();

	/**
	 * Service pour afficher les tarifs en fonction des durées de séjour associé à l'idDestination entré en paramètre
	 * @param idDestination
	 * @return tarifs en fonction des durées de séjour associé à l'idDestination entré en paramètre
	 * @throws TarifDureeHebergementException
	 */
	public List<TarifDureeHebergement> afficherTarifDureeHebergementSelonIdDestination(int idDestination) throws TarifDureeHebergementException;


}
