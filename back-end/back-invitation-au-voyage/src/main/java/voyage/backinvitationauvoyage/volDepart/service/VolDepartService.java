package voyage.backinvitationauvoyage.volDepart.service;

import java.util.List;

import org.springframework.stereotype.Service;

import voyage.backinvitationauvoyage.volDepart.VolDepartException;
import voyage.backinvitationauvoyage.volDepart.bean.VolDepart;

/**
 * Couche Service de VolDepart
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface VolDepartService {

	/**
	 * Service pour afficher tous les vols
	 * @return liste des vols
	 */
	public List<VolDepart> afficherTousLesVolDeparts();

	/**
	 * Service pour afficher le vol associé à l'idVol entré en paramètre
	 * @param idVolDepart
	 * @return vol associé à l'idVol entré en paramètre
	 * @throws VolDepartException
	 */
	public VolDepart afficherVolDepartParId(int idVolDepart) throws VolDepartException;

	/**
	 * Service pour afficher les vols associés à l'idDestination entré en paramètre
	 * @param idDestination
	 * @return vols associés à l'idDestination entré en paramètre
	 * @throws VolDepartException
	 */
	public List<VolDepart> afficherVolDepartParIdDestination(int idDestination) throws VolDepartException;

	/**
	 * Service pour afficher le vol associé à l'idDestination et à la ville de départ entrés en paramètre
	 * @param idDestination
	 * @param villeDepart
	 * @return vol associé à l'idDestination et à la ville de départ entrés en paramètre
	 * @throws VolDepartException
	 */
	public VolDepart afficherVolDepartParIdDestinationEtVille(int idDestination, String villeDepart) throws VolDepartException;

}
