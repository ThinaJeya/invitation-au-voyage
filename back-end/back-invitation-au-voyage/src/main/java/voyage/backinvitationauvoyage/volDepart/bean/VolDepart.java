package voyage.backinvitationauvoyage.volDepart.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe Bean de VolDepart
 * @author Pirathina Jeyakumar
 *
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "Vol_depart")
public class VolDepart {

	/**
	 * Identifiant du vol
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_vol_depart")
	private int idVolDepart;
	
	/**
	 * Ville de départ du vol
	 */
	@Column(name = "ville_depart")
	private String villeDepart;
	
	/**
	 * Prix du vol par adulte
	 */
	@Column(name = "prix_vol_adulte")
	private float prixVolAdulte;
	
	/**
	 * Identifiant de la destination
	 */
	@Column(name = "id_destination")
	private int idDestination;

	/**
	 * Constructeur par défaut de VolDepart
	 */
	public VolDepart() {
		super();
	}

	/**
	 * Constructeur de VolDepart
	 * @param idVolDepart
	 * @param villeDepart
	 * @param prixVolAdulte
	 * @param idDestination
	 */
	public VolDepart(int idVolDepart, String villeDepart, float prixVolAdulte, int idDestination) {
		super();
		this.idVolDepart = idVolDepart;
		this.villeDepart = villeDepart;
		this.prixVolAdulte = prixVolAdulte;
		this.idDestination = idDestination;
	}

	/**
	 * Permet d'obtenir l'identifiant du vol
	 * @return identifiant du vol
	 */
	public int getIdVolDepart() {
		return idVolDepart;
	}

	/**
	 * Permet de modifier l'identifiant du vol
	 * @param idVolDepart
	 */
	public void setIdVolDepart(int idVolDepart) {
		this.idVolDepart = idVolDepart;
	}

	/**
	 * Permet d'obtenir la ville de départ du vol
	 * @return ville de départ du vol
	 */
	public String getVilleDepart() {
		return villeDepart;
	}

	/**
	 * Permet de modifier la ville de départ du vol
	 * @param villeDepart
	 */
	public void setVilleDepart(String villeDepart) {
		this.villeDepart = villeDepart;
	}

	/**
	 * Permet d'obtenir le prix du vol par adulte
	 * @return prix du vol par adulte
	 */
	public float getPrixVolAdulte() {
		return prixVolAdulte;
	}

	/**
	 * Permet de modifier le prix du vol par adulte
	 * @param prixVolAdulte
	 */
	public void setPrixVolAdulte(float prixVolAdulte) {
		this.prixVolAdulte = prixVolAdulte;
	}

	/**
	 * Permet d'obtenir l'identifiant de la destination
	 * @return identifiant de la destination
	 */
	public int getIdDestination() {
		return idDestination;
	}

	/**
	 * Permet de modifier l'identifiant de la destination
	 * @param idDestination
	 */
	public void setIdDestination(int idDestination) {
		this.idDestination = idDestination;
	}
}
