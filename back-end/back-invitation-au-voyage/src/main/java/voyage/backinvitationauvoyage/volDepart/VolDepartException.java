package voyage.backinvitationauvoyage.volDepart;

public class VolDepartException extends Exception{

	public enum MessageVolDepartException{
		PARAMETRE_INCORRECT("Le paramètre entré est incorrect"),
		PARAMETRE_INTERNE_INCORRECT("Un des paramètres internes est incorrect");
		
		private String message;

		private MessageVolDepartException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}
	
	public VolDepartException(String messageException) {
		super(messageException);
	}
}
