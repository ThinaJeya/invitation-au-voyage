 package voyage.backinvitationauvoyage.volDepart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import voyage.backinvitationauvoyage.volDepart.bean.VolDepart;

/**
 * Couche dao de VolDepartService
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface VolDepartDaoService extends JpaRepository<VolDepart, Integer>{

}
