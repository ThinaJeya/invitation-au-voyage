package voyage.backinvitationauvoyage.volDepart.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import voyage.backinvitationauvoyage.volDepart.VolDepartException;
import voyage.backinvitationauvoyage.volDepart.VolDepartException.MessageVolDepartException;
import voyage.backinvitationauvoyage.volDepart.bean.VolDepart;
import voyage.backinvitationauvoyage.volDepart.dao.VolDepartDaoService;

/**
 * Implémentation de VolDepartService
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class VolDepartServiceImpl implements VolDepartService{

	@Autowired
	private VolDepartDaoService volDepartDaoService;

	@Override
	public List<VolDepart> afficherTousLesVolDeparts() {
		return volDepartDaoService.findAll();
	}

	@Override
	public VolDepart afficherVolDepartParId(int idVolDepart) throws VolDepartException {

		if(idVolDepart > 0) {
			return volDepartDaoService.getOne(idVolDepart);			
		} else {
			throw new VolDepartException(MessageVolDepartException.PARAMETRE_INCORRECT.getMessage());
		}
	}

	@Override
	public List<VolDepart> afficherVolDepartParIdDestination(int idDestination) throws VolDepartException {
		List<VolDepart> volDeparts = afficherTousLesVolDeparts();
		List<VolDepart> reponse = new ArrayList<>();

		if(idDestination > 0 && volDeparts != null) {
			for(VolDepart volDepart: volDeparts) {
				if(volDepart.getIdDestination() == idDestination) {
					reponse.add(volDepart);
				}
			}
			return reponse;

		} else {
			throw new VolDepartException(MessageVolDepartException.PARAMETRE_INCORRECT.getMessage());
		}

	}

	@Override
	public VolDepart afficherVolDepartParIdDestinationEtVille(int idDestination, String villeDepart) throws VolDepartException {
		VolDepart reponse = null;

		if(idDestination > 0 && !StringUtils.isBlank(villeDepart)) {
			List<VolDepart> volDeparts = afficherVolDepartParIdDestination(idDestination);

			if(!volDeparts.isEmpty()) {
				for(VolDepart volDepart: volDeparts) {
					if(volDepart.getVilleDepart().equals(villeDepart)) {
						reponse = volDepart;
					}
				}
				return reponse;

			} else {
				throw new VolDepartException(MessageVolDepartException.PARAMETRE_INTERNE_INCORRECT.getMessage());
			}
			
		}  else {
			throw new VolDepartException(MessageVolDepartException.PARAMETRE_INCORRECT.getMessage());
		}
	}
}
