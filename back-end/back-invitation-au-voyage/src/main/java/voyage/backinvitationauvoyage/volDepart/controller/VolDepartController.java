package voyage.backinvitationauvoyage.volDepart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import voyage.backinvitationauvoyage.volDepart.VolDepartException;
import voyage.backinvitationauvoyage.volDepart.bean.VolDepart;
import voyage.backinvitationauvoyage.volDepart.service.VolDepartService;

/**
 * Controller de VolDepart
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/volDeparts")
public class VolDepartController {

	@Autowired
	private VolDepartService volDepartService;
	
	/**
	 * Controller pour afficher tous les vols
	 * @return liste des vols
	 */
	@GetMapping(path = "/tousLesVolDeparts")
	public List<VolDepart> afficherTousLesVolDeparts(){
		return volDepartService.afficherTousLesVolDeparts();
	}
	
	/**
	 * Controller pour afficher le vol associé à l'idVol entré en paramètre
	 * @param idVolDepart
	 * @return vol associé à l'idVol entré en paramètre
	 * @throws VolDepartException
	 */
	@GetMapping(path = "/volDepartParId/{idVolDepart}")
	public VolDepart afficherVolDepartParId(@PathVariable int idVolDepart) throws VolDepartException{
		return volDepartService.afficherVolDepartParId(idVolDepart);
	}
	
	/**
	 * Controller pour afficher les vols associés à l'idDestination entré en paramètre
	 * @param idDestination
	 * @return vols associés à l'idDestination entré en paramètre
	 * @throws VolDepartException
	 */
	@GetMapping(path = "/volDepartParIdDestination/{idDestination}")
	public List<VolDepart> afficherVolDepartParIdDestination(@PathVariable int idDestination) throws VolDepartException {
		return volDepartService.afficherVolDepartParIdDestination(idDestination);
	}
	
	/**
	 * Controller pour afficher le vol associé à l'idDestination et à la ville de départ entrés en paramètre
	 * @param idDestination
	 * @param villeDepart
	 * @return vol associé à l'idDestination et à la ville de départ entrés en paramètre
	 * @throws VolDepartException
	 */
	@GetMapping(path = "/volDepartParIdDestinationEtVille/{idDestination}/{villeDepart}")
	public VolDepart afficherVolDepartParIdDestinationEtVille(@PathVariable int idDestination, @PathVariable String villeDepart) throws VolDepartException {
		return volDepartService.afficherVolDepartParIdDestinationEtVille(idDestination, villeDepart);
	}
}
