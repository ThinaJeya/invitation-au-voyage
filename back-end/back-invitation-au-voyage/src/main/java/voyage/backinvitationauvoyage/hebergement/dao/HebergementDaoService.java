package voyage.backinvitationauvoyage.hebergement.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import voyage.backinvitationauvoyage.hebergement.bean.Hebergement;

/**
 * Couche Dao de HebergementService
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface HebergementDaoService extends JpaRepository<Hebergement, Integer>{

}
