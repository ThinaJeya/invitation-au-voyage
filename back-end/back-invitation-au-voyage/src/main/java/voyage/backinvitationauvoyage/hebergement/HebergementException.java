package voyage.backinvitationauvoyage.hebergement;

public class HebergementException extends Exception{

	public enum MessageHebergementException{
		PARAMETRE_INCORRECT("Le paramètre entré est incorrect");
		
		private String message;

		private MessageHebergementException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}

	public HebergementException(String messageException) {
		super(messageException);
	}
}
