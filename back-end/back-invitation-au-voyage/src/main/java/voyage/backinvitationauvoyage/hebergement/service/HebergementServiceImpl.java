package voyage.backinvitationauvoyage.hebergement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.hebergement.HebergementException.MessageHebergementException;
import voyage.backinvitationauvoyage.hebergement.bean.Hebergement;
import voyage.backinvitationauvoyage.hebergement.dao.HebergementDaoService;

/**
 * Couche Implémentation de HebergementService
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class HebergementServiceImpl implements HebergementService {

	@Autowired
	private HebergementDaoService hebergementDaoService;

	@Override
	public List<Hebergement> afficherTouslesHebergements() {
		return hebergementDaoService.findAll();
	}

	@Override
	public Hebergement afficherHebergementParId(int idHebergement) throws HebergementException {

		if(idHebergement > 0) {
			return hebergementDaoService.getOne(idHebergement);			
		} else {
			throw new HebergementException(MessageHebergementException.PARAMETRE_INCORRECT.getMessage());
		}
	}

	@Override
	public Hebergement afficherHebergementParIdDestination(int idDestination) throws HebergementException {
		Hebergement reponse = null;
		List<Hebergement> hebergements = afficherTouslesHebergements();

		if(idDestination > 0 && !ObjectUtils.isEmpty(hebergements)) {
			for(Hebergement hebergement: hebergements) {
				if(hebergement.getIdDestination() == idDestination) {
					reponse = hebergement;
				}
			}
		} else {
			throw new HebergementException(MessageHebergementException.PARAMETRE_INCORRECT.getMessage());
		}
		return reponse;
	}
}
