package voyage.backinvitationauvoyage.hebergement.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe Bean de Hebergement
 * @author Pirathina Jeyakumar
 *
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "Hebergement")
public class Hebergement {

	/**
	 * Identifiant (id) de l'hébergement
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_hebergement")
	private int idHebergement;

	/**
	 * Prix d'une nuit d'hébergement par adulte
	 */
	@Column(name = "prix_nuit_adulte")
	private float prixNuitAdulte;

	/**
	 * Identifiant de la destination
	 */
	@Column(name = "id_destination")
	private int idDestination;

	/**
	 * Identifiant de l'hotel
	 */
	@Column(name = "id_hotel")
	private int idHotel;

	/**
	 * Constructeur par défaut de Hebergement
	 */
	public Hebergement() {
		super();
	}

	/**
	 * Constructeur de Hebergement
	 * @param idHebergement
	 * @param prixNuitAdulte
	 * @param idDestination
	 * @param idHotel
	 */
	public Hebergement(int idHebergement, float prixNuitAdulte, int idDestination, int idHotel) {
		super();
		this.idHebergement = idHebergement;
		this.prixNuitAdulte = prixNuitAdulte;
		this.idDestination = idDestination;
		this.idHotel = idHotel;
	}

	/**
	 * Permet d'obtenir l'identifiant de l'hébergement
	 * @return identifiant de l'hébergement
	 */
	public int getIdHebergement() {
		return idHebergement;
	}

	/**
	 * Permet de modifier l'identifiant de l'hébergement
	 * @param idHebergement
	 */
	public void setIdHebergement(int idHebergement) {
		this.idHebergement = idHebergement;
	}

	/**
	 * Permet d'obtenir le prix d'une nuit d'hébergement par adulte
	 * @return prix d'une nuit d'hébergement par adulte
	 */
	public float getPrixNuitAdulte() {
		return prixNuitAdulte;
	}

	/**
	 * Permet de modifier le prix d'une nuit d'hébergement par adulte
	 * @param prixNuitAdulte
	 */
	public void setPrixNuitAdulte(float prixNuitAdulte) {
		this.prixNuitAdulte = prixNuitAdulte;
	}

	/**
	 * Permet d'obtenir l'identifiant de la destination
	 * @return identifiant de la destination
	 */
	public int getIdDestination() {
		return idDestination;
	}

	/**
	 * Permet de modifier l'identifiant de la destination
	 * @param idDestination
	 */
	public void setIdDestination(int idDestination) {
		this.idDestination = idDestination;
	}

	/**
	 * Permet d'obtenir l'identifiant de l'hotel
	 * @return identifiant de l'hotel
	 */
	public int getIdHotel() {
		return idHotel;
	}

	/**
	 * Permet de modifier l'identifiant de l'hotel
	 * @param idHotel
	 */
	public void setIdHotel(int idHotel) {
		this.idHotel = idHotel;
	}
}
