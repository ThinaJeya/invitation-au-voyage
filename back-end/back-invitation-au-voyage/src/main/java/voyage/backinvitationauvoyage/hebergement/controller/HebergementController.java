package voyage.backinvitationauvoyage.hebergement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.hebergement.bean.Hebergement;
import voyage.backinvitationauvoyage.hebergement.service.HebergementService;

/**
 * Controller de Hebergement
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/hebergements")
public class HebergementController {

	@Autowired
	private HebergementService hebergementService;
	
	/**
	 * Controller pour afficher tous les hebergements
	 * @return Liste des hebergements
	 */
	@GetMapping(path = "/tousLesHebergements")
	public List<Hebergement> afficherTouslesHebergements(){
		return hebergementService.afficherTouslesHebergements();
	}
	
	/**
	 * Controller pour afficher l'hebergement associé à l'idHebergement entré en paramètre
	 * @param idHebergement
	 * @return hebergement associé à l'idHebergement entré en paramètre
	 * @throws HebergementException
	 */
	@GetMapping(path = "/hebergementParId/{idHebergement}")
	public Hebergement afficherHebergementParId(@PathVariable int idHebergement) throws HebergementException{
		return hebergementService.afficherHebergementParId(idHebergement);
	}
	
	/**
	 * Controller pour afficher l'hebergement associé à l'idDestination entré en paramètre
	 * @param idDestination
	 * @return hebergement associé à l'idDestination entré en paramètre
	 * @throws HebergementException
	 */
	@GetMapping(path = "/hebergementParIdDestination/{idDestination}")
	public Hebergement afficherHebergementParIdDestination(@PathVariable int idDestination) throws HebergementException{
		return hebergementService.afficherHebergementParIdDestination(idDestination);
	}

}
