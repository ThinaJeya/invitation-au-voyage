package voyage.backinvitationauvoyage.hebergement.service;

import java.util.List;

import org.springframework.stereotype.Service;

import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.hebergement.bean.Hebergement;

/**
 * Couche Service de Hebergement
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface HebergementService {

	/**
	 * Service pour afficher tous les hebergements
	 * @return Liste des hebergements
	 */
	public List<Hebergement> afficherTouslesHebergements();

	/**
	 * Service pour afficher l'hebergement associé à l'idHebergement entré en paramètre
	 * @param idHebergement
	 * @return hebergement associé à l'idHebergement entré en paramètre
	 * @throws HebergementException
	 */
	public Hebergement afficherHebergementParId(int idHebergement) throws HebergementException;

	/**
	 * Service pour afficher l'hebergement associé à l'idDestination entré en paramètre
	 * @param idDestination
	 * @return hebergement associé à l'idDestination entré en paramètre
	 * @throws HebergementException
	 */
	public Hebergement afficherHebergementParIdDestination(int idDestination) throws HebergementException;

}
