package voyage.backinvitationauvoyage.devis;

public class DevisException extends Exception{

	public enum MessageDevisException{
		PARAMETRE_INCORRECT("Le paramètre entré est incorrect"),
		SEJOURDTO_INCORRECT("Un des paramètres entré pour l'objet SejourDTO est incorrect"),
		FORMULAIREDEVISDTO_INCORRECT("Un des paramètres entré pour l'objet FormulaireDevisDTO est incorrect"),
		PARAMETRE_METHODE_INTERNE_INCORRECT("Un des paramètres entré sur une méthode interne est incorrecte");

		private String message;

		private MessageDevisException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}

	public DevisException(String messageException) {
		super(messageException);
	}
}
