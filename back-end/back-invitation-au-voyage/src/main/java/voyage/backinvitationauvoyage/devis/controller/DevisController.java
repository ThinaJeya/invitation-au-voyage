package voyage.backinvitationauvoyage.devis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import voyage.backinvitationauvoyage.destination.DestinationException;
import voyage.backinvitationauvoyage.devis.DevisException;
import voyage.backinvitationauvoyage.devis.bean.Devis;
import voyage.backinvitationauvoyage.devis.service.DevisService;
import voyage.backinvitationauvoyage.dto.FormulaireDevisDTO;
import voyage.backinvitationauvoyage.dto.PrixDTO;
import voyage.backinvitationauvoyage.dto.SejourDTO;
import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.volDepart.VolDepartException;

/**
 * Controller de Devis
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/devis")
public class DevisController {

	@Autowired
	private DevisService devisService;
	
	/**
	 * Controller pour afficher la liste des devis
	 * @return liste des devis
	 */
	@GetMapping(path = "/tousLesDevis")
	public List<Devis> afficherTousLesDevis(){
		return devisService.afficherTousLesDevis();
	}
	
	/**
	 * Controller pour afficher le devis associé à l'idDevis entré en paramètre
	 * @param idDevis
	 * @return Devis
	 * @throws DevisException 
	 */
	@GetMapping(path = "/devisParId/{idDevis}")
	public Devis afficherDevisParId(@PathVariable int idDevis) throws DevisException{
		return devisService.afficherDevisParId(idDevis);
	}
	
	/**
	 * Controller pour afficher l'objet PrixDTO selon le sejourDTO entré en paramètre
	 * @param sejourDTO
	 * @return PrixDTO
	 * @throws DestinationException
	 * @throws HebergementException
	 * @throws DevisException
	 * @throws VolDepartException 
	 */
	@PostMapping(path = "/prixSejourAPartirDe")
	public PrixDTO calculPrixSejourAPartirDe(@RequestBody SejourDTO sejourDTO) throws DestinationException, HebergementException, DevisException, VolDepartException {
		return devisService.calculPrixSejourAPartirDe(sejourDTO);
	}
	
	/**
	 * Controller pour enregistrer le devis selon le FormulaireDevisDTO entré en paramètre
	 * @param formulaireDevisDTO
	 * @throws DestinationException
	 * @throws HebergementException
	 * @throws DevisException
	 * @throws VolDepartException 
	 */
	@PostMapping(path = "/enregistrerDevis")
	public boolean enregistrerDevis(@RequestBody FormulaireDevisDTO formulaireDevisDTO) throws DestinationException, HebergementException, DevisException, VolDepartException {
		return devisService.enregistrerDevis(formulaireDevisDTO);
	}
	
	@GetMapping(path = "/afficherDevisEnregistre/{continentDestination}/{paysDestination}/{idDestination}/{nomClient}")
	public Devis afficherDevisEnregistre(@PathVariable String continentDestination, @PathVariable String paysDestination, 
			@PathVariable int idDestination, @PathVariable String nomClient) throws DevisException {
		return devisService.afficherDevisEnregistre(continentDestination, paysDestination, idDestination, nomClient);
	}
}
