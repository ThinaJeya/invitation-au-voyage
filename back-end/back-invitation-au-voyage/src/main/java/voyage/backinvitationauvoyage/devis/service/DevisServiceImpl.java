package voyage.backinvitationauvoyage.devis.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import voyage.backinvitationauvoyage.destination.DestinationException;
import voyage.backinvitationauvoyage.destination.bean.Destination;
import voyage.backinvitationauvoyage.destination.service.DestinationService;
import voyage.backinvitationauvoyage.devis.DevisException;
import voyage.backinvitationauvoyage.devis.DevisException.MessageDevisException;
import voyage.backinvitationauvoyage.devis.bean.Devis;
import voyage.backinvitationauvoyage.devis.dao.DevisDaoService;
import voyage.backinvitationauvoyage.dto.FormulaireDevisDTO;
import voyage.backinvitationauvoyage.dto.PrixDTO;
import voyage.backinvitationauvoyage.dto.SejourDTO;
import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.hebergement.bean.Hebergement;
import voyage.backinvitationauvoyage.hebergement.service.HebergementService;
import voyage.backinvitationauvoyage.volDepart.VolDepartException;
import voyage.backinvitationauvoyage.volDepart.bean.VolDepart;
import voyage.backinvitationauvoyage.volDepart.service.VolDepartService;

/**
 * Couche Implémentation de DevisService
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class DevisServiceImpl implements DevisService{

	@Autowired
	private DevisDaoService devisDaoService;

	@Autowired
	private DestinationService destinationService;

	@Autowired
	private HebergementService hebergementService;

	@Autowired
	private VolDepartService volDepartService;

	@Override
	public List<Devis> afficherTousLesDevis() {
		return devisDaoService.findAll();
	}

	@Override
	public Devis afficherDevisParId(int idDevis) throws DevisException {
		if(idDevis > 0) {
			return devisDaoService.getOne(idDevis);			
		} else {
			throw new DevisException(MessageDevisException.PARAMETRE_INCORRECT.getMessage());
		}
	}

	@Override
	public PrixDTO calculPrixSejourAPartirDe(SejourDTO sejourDTO) throws DestinationException, HebergementException, DevisException, VolDepartException {		
		PrixDTO prixDTO = new PrixDTO();

		if(sejourDTO != null && sejourDTO.getIdDestination() > 0 && sejourDTO.isTransport() && sejourDTO.getNbNuits() > 0 && sejourDTO.getNbParticipantsAdulte() > 0) {

			float prixBase = calculPrixBase(sejourDTO.getIdDestination(), sejourDTO.getNbParticipantsAdulte(), sejourDTO.getNbParticipantsEnfant());
			float prixHebergement = calculPrixHebergement(sejourDTO.getIdDestination(), sejourDTO.getNbNuits(), sejourDTO.getNbParticipantsAdulte(), sejourDTO.getNbParticipantsEnfant());
			float prixVolParis = calculPrixVol(sejourDTO.getIdDestination(), sejourDTO.getVilleDepart(), sejourDTO.getNbParticipantsAdulte(), sejourDTO.getNbParticipantsEnfant());

			float prixVolMarseille = calculPrixVol(sejourDTO.getIdDestination(), "Marseille", sejourDTO.getNbParticipantsAdulte(), sejourDTO.getNbParticipantsEnfant());
			float prixVolLyon = calculPrixVol(sejourDTO.getIdDestination(), "Lyon", sejourDTO.getNbParticipantsAdulte(), sejourDTO.getNbParticipantsEnfant());
			float prixVolNantes = calculPrixVol(sejourDTO.getIdDestination(), "Nantes", sejourDTO.getNbParticipantsAdulte(), sejourDTO.getNbParticipantsEnfant());

			prixDTO.setPrixAPartirDeSansVol(prixBase + prixHebergement);
			prixDTO.setPrixAPartirDeAvecVol(prixBase + prixHebergement + prixVolParis);
			prixDTO.setPrixVolAuDepartDeParis(prixVolParis);
			prixDTO.setPrixVolAuDepartDeMarseille(prixVolMarseille);
			prixDTO.setPrixVolAuDepartDeLyon(prixVolLyon);
			prixDTO.setPrixVolAuDepartDeNantes(prixVolNantes);

		} else {
			throw new DevisException(MessageDevisException.SEJOURDTO_INCORRECT.getMessage());
		}	

		return prixDTO;			
	}

	/**
	 * Calcul du prix de base en fonction des participants
	 * @param idDestination
	 * @param nbParticipantsAdulte
	 * @param nbParticipantEnfant
	 * @return prix de base en fonction des participants
	 * @throws DestinationException
	 * @throws DevisException
	 */
	private float calculPrixBase(int idDestination, int nbParticipantsAdulte, int nbParticipantEnfant) throws DestinationException, DevisException {
		float prixBaseTotal = 0;

		if(idDestination > 0 && nbParticipantsAdulte > 0) {
			Destination destination = destinationService.afficherDestinationParId(idDestination);

			if(destination != null) {
				float prixBaseAdulte = (destination.getPrixBaseAdulte() * nbParticipantsAdulte);
				float prixBaseEnfant = ((destination.getPrixBaseAdulte() /2) * nbParticipantEnfant);
				
				prixBaseTotal = prixBaseAdulte + prixBaseEnfant;
				
				return prixBaseTotal;				
			} else {
				throw new DevisException(MessageDevisException.PARAMETRE_METHODE_INTERNE_INCORRECT.getMessage());
			}

		} else {
			throw new DevisException(MessageDevisException.PARAMETRE_METHODE_INTERNE_INCORRECT.getMessage());
		}
	}

	/**
	 * Calcul du prix d'hebergement en fonction du nombre de nuits et des participants
	 * @param idDestination
	 * @param nbNuits
	 * @param nbParticipantsAdulte
	 * @param nbParticipantEnfant
	 * @return prix d'hebergement en fonction du nombre de nuits et des participants
	 * @throws HebergementException
	 * @throws DevisException
	 */
	private float calculPrixHebergement(int idDestination, int nbNuits, int nbParticipantsAdulte, int nbParticipantEnfant) throws HebergementException, DevisException {
		float prixHebergementTotal = 0;

		if(idDestination > 0 && nbParticipantsAdulte > 0 && nbNuits > 0) {
			Hebergement hebergement = hebergementService.afficherHebergementParId(idDestination);

			if(hebergement != null) {
				float prixHebergementAdulte = (hebergement.getPrixNuitAdulte() * nbParticipantsAdulte * nbNuits);
				float prixHebergementEnfant = ((hebergement.getPrixNuitAdulte()/2) * nbParticipantEnfant * nbNuits);

				prixHebergementTotal = prixHebergementAdulte + prixHebergementEnfant;

				return prixHebergementTotal;				
			} else {
				throw new DevisException(MessageDevisException.PARAMETRE_METHODE_INTERNE_INCORRECT.getMessage());
			}

		} else {
			throw new DevisException(MessageDevisException.PARAMETRE_METHODE_INTERNE_INCORRECT.getMessage());
		}
	}

	/**
	 * Calcul du prix du vol en fonction de la ville de départ et des participants
	 * @param idDestination
	 * @param villeDepart
	 * @param nbParticipantsAdulte
	 * @param nbParticipantEnfant
	 * @return prix du vol en fonction de la ville de départ et des participants
	 * @throws DevisException
	 * @throws VolDepartException
	 */
	private float calculPrixVol(int idDestination, String villeDepart, int nbParticipantsAdulte, int nbParticipantEnfant) throws DevisException, VolDepartException {
		float prixVolTotal = 0;

		if(idDestination > 0 && nbParticipantsAdulte > 0 && !StringUtils.isBlank(villeDepart)) {
			VolDepart volDepart = volDepartService.afficherVolDepartParIdDestinationEtVille(idDestination, villeDepart);

			if(volDepart != null) {
				float prixVolAdulte = (volDepart.getPrixVolAdulte() * nbParticipantsAdulte);
				float prixVolEnfant = (volDepart.getPrixVolAdulte() / 2) * nbParticipantEnfant;

				prixVolTotal = prixVolAdulte + prixVolEnfant;

				return prixVolTotal;						
			} else {
				throw new DevisException(MessageDevisException.PARAMETRE_METHODE_INTERNE_INCORRECT.getMessage());
			}

		} else {
			throw new DevisException(MessageDevisException.PARAMETRE_METHODE_INTERNE_INCORRECT.getMessage());
		}

	}

	/**
	 * Calcul du prix total TTC (base, vol, hebergement)
	 * @param montantTotalPrixBase
	 * @param montantTotalPrixVol
	 * @param montantTotalPrixHebergement
	 * @return prix total TTC (base, vol, hebergement)
	 */
	private float calculMontantTotalTTC(float montantTotalPrixBase, float montantTotalPrixVol, float montantTotalPrixHebergement) {
		float montantTotalTTC = 0;		
		montantTotalTTC = montantTotalPrixBase + montantTotalPrixVol + montantTotalPrixHebergement;		
		return montantTotalTTC;
	}

	@Override
	public boolean enregistrerDevis(FormulaireDevisDTO formulaireDevisDTO) throws DestinationException, HebergementException, DevisException, VolDepartException {
		boolean estEnregistre = false;

		Devis devis = new Devis();
		float montantTotalBase = 0;
		float montantTotalVol = 0;
		float montantTotalHebergement = 0;
		float montantTotalTTC = 0;

		if(formulaireDevisDTO != null && !StringUtils.isAllBlank(formulaireDevisDTO.getContinentDestination(), formulaireDevisDTO.getEmailClient(), formulaireDevisDTO.getNomClient(),
				formulaireDevisDTO.getPaysDestination(), formulaireDevisDTO.getVilleDepartVol()) 
				&& formulaireDevisDTO.getIdDestination() > 0 && formulaireDevisDTO.getDateDepart() != null
				&& formulaireDevisDTO.getNbNuits() > 1 && formulaireDevisDTO.getNbParticipantsAdulte() > 0) {

			montantTotalBase = calculPrixBase(formulaireDevisDTO.getIdDestination(), formulaireDevisDTO.getNbParticipantsAdulte(), formulaireDevisDTO.getNbParticipantsEnfant());

			if(formulaireDevisDTO.isTransport()) {
				montantTotalVol = calculPrixVol(formulaireDevisDTO.getIdDestination(), formulaireDevisDTO.getVilleDepartVol(), formulaireDevisDTO.getNbParticipantsAdulte(), formulaireDevisDTO.getNbParticipantsEnfant());
			}

			montantTotalHebergement = calculPrixHebergement(formulaireDevisDTO.getIdDestination(), formulaireDevisDTO.getNbNuits(), formulaireDevisDTO.getNbParticipantsAdulte(), formulaireDevisDTO.getNbParticipantsEnfant());

			montantTotalTTC = calculMontantTotalTTC(montantTotalBase, montantTotalVol, montantTotalHebergement);

			devis.setIdDestination(formulaireDevisDTO.getIdDestination());
			devis.setContinentDestination(formulaireDevisDTO.getContinentDestination());
			devis.setPaysDestination(formulaireDevisDTO.getPaysDestination());
			devis.setNomClient(formulaireDevisDTO.getNomClient());
			devis.setEmailClient(formulaireDevisDTO.getEmailClient());
			devis.setDateDepart(formulaireDevisDTO.getDateDepart());
			devis.setTransport(formulaireDevisDTO.isTransport());
			devis.setVilleDepartVol(formulaireDevisDTO.getVilleDepartVol());
			devis.setNbNuits(formulaireDevisDTO.getNbNuits());
			devis.setNbParticipantsAdulte(formulaireDevisDTO.getNbParticipantsAdulte());
			devis.setNbParticipantsEnfant(formulaireDevisDTO.getNbParticipantsEnfant());
			devis.setNbParticipantsBebe(formulaireDevisDTO.getNbParticipantsBebe());
			devis.setMontantTotalBase(montantTotalBase);
			devis.setMontantTotalVol(montantTotalVol);
			devis.setMontantTotalHebergement(montantTotalHebergement);
			devis.setMontantTotalTTC(montantTotalTTC);

			devisDaoService.save(devis);

			estEnregistre = true;

		} else {
			throw new DevisException(MessageDevisException.FORMULAIREDEVISDTO_INCORRECT.getMessage());
		}

		return estEnregistre;
	}

	@Override
	public Devis afficherDevisEnregistre(String continentDestination, String paysDestination, int idDestination,
			String nomClient) throws DevisException {

		List<Devis> possibilitesDevis = new ArrayList<>();
		Devis reponse = null;

		if(!StringUtils.isBlank(continentDestination) && !StringUtils.isBlank(paysDestination) && !StringUtils.isBlank(nomClient) && idDestination > 0) {
			List<Devis> tousLesDevis = afficherTousLesDevis();

			for(Devis devis: tousLesDevis) {
				if(devis.getContinentDestination().equals(continentDestination) && devis.getPaysDestination().equals(paysDestination) &&
						devis.getNomClient().equals(nomClient) && devis.getIdDestination() == idDestination) {
					possibilitesDevis.add(devis);
				}
			}

			if(possibilitesDevis.size() == 1) {
				reponse = possibilitesDevis.get(0);
				return reponse;
			} else if(possibilitesDevis.size() > 1) {
				reponse = possibilitesDevis.get(possibilitesDevis.size() -1);
			}

			return reponse;

		} else {
			throw new DevisException(MessageDevisException.PARAMETRE_INCORRECT.getMessage());
		}

	}
}
