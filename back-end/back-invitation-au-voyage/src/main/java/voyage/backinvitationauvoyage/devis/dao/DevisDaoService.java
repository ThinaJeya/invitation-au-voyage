package voyage.backinvitationauvoyage.devis.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import voyage.backinvitationauvoyage.devis.bean.Devis;

/**
 * Couche dao de DevisService
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface DevisDaoService extends JpaRepository<Devis, Integer>{
}
