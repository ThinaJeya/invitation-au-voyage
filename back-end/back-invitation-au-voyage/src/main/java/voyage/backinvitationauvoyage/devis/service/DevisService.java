package voyage.backinvitationauvoyage.devis.service;

import java.util.List;

import org.springframework.stereotype.Service;

import voyage.backinvitationauvoyage.destination.DestinationException;
import voyage.backinvitationauvoyage.devis.DevisException;
import voyage.backinvitationauvoyage.devis.bean.Devis;
import voyage.backinvitationauvoyage.dto.FormulaireDevisDTO;
import voyage.backinvitationauvoyage.dto.PrixDTO;
import voyage.backinvitationauvoyage.dto.SejourDTO;
import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.volDepart.VolDepartException;

/**
 * Service de Devis
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface DevisService {

	/**
	 * Service pour afficher la liste des devis
	 * @return liste des devis
	 */
	public List<Devis> afficherTousLesDevis();
	
	/**
	 * Service pour afficher le devis associé à l'idDevis entré en paramètre
	 * @param idDevis
	 * @return Devis
	 * @throws DevisException 
	 */
	public Devis afficherDevisParId(int idDevis) throws DevisException;

	/**
	 * Service pour afficher l'objet PrixDTO selon le sejourDTO entré en paramètre
	 * @param sejourDTO
	 * @return PrixDTO
	 * @throws DestinationException
	 * @throws HebergementException
	 * @throws DevisException
	 * @throws VolDepartException 
	 */
	public PrixDTO calculPrixSejourAPartirDe(SejourDTO sejourDTO) throws DestinationException, HebergementException, DevisException, VolDepartException;
	
	/**
	 * Service pour enregistrer le devis selon le FormulaireDevisDTO entré en paramètre
	 * @param formulaireDevisDTO
	 * @throws DestinationException
	 * @throws HebergementException
	 * @throws DevisException
	 * @throws VolDepartException 
	 */
	public boolean enregistrerDevis(FormulaireDevisDTO formulaireDevisDTO) throws DestinationException, HebergementException, DevisException, VolDepartException;

	public Devis afficherDevisEnregistre(String continentDestination, String paysDestination, int idDestination,
			String nomClient) throws DevisException;

}
