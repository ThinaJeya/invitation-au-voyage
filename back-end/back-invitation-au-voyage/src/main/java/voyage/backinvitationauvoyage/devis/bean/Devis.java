package voyage.backinvitationauvoyage.devis.bean;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Bean de Devis
 * @author Pirathina Jeyakumar
 *
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "Devis")
public class Devis {

	/**
	 * Identifiant (id) de devis
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_devis")
	private int idDevis;
	
	/**
	 * Nom du continent de la destination
	 */
	@Column(name = "continent_destination")
	private String continentDestination;

	/**
	 * Nom du pays de la destination
	 */
	@Column(name = "pays_destination")
	private String paysDestination;

	/**
	 * Nom du client
	 */
	@Column(name = "nom_client")
	private String nomClient;

	/**
	 * Adresse email du client
	 */
	@Column(name = "email_client")
	private String emailClient;

	/**
	 * Date de départ souhaitée
	 */
	@Column(name = "date_depart_souhaitee")
	private Date dateDepart;
	
	/**
	 * Renseigne si le vol est inclu 
	 */
	@Column(name = "transport_inclu")
	private boolean transport;
	
	/**
	 * Nom de la ville de départ du vol
	 */
	@Column(name = "ville_depart")
	private String villeDepartVol;
	
	/**
	 * Nombre de nuits souhaitées
	 */
	@Column(name = "nb_nuits_souhaitees")
	private int nbNuits;
	
	/**
	 * Nombre de participants adulte
	 */
	@Column(name = "nb_participants_adulte")
	private int nbParticipantsAdulte;

	/**
	 * Nombre de participants enfant
	 */
	@Column(name = "nb_participants_enfant")
	private int nbParticipantsEnfant;

	/**
	 * Nombre de participants bébé
	 */
	@Column(name = "nb_participants_bebe")
	private int nbParticipantsBebe;

	/**
	 * Montant total de base du séjour
	 */
	@Column(name = "montant_total_base")
	private float montantTotalBase;

	/**
	 * Montant total du vol du séjour
	 */
	@Column(name = "montant_total_vol")
	private float montantTotalVol;

	/**
	 * Montant total de l'hébergement du séjour
	 */
	@Column(name = "montant_total_hebergement")
	private float montantTotalHebergement;

	/**
	 * Montant total TTC du séjour
	 */
	@Column(name = "montant_total_TTC")
	private float montantTotalTTC;

	/**
	 * identifiant de la destination
	 */
	@Column(name = "id_destination")
	private int idDestination;

	/**
	 * Constructeur par défaut de Devis
	 */
	public Devis() {
		super();
	}

	/**
	 * Constructeur de Devis
	 * @param idDevis
	 * @param continentDestination
	 * @param paysDestination
	 * @param nomClient
	 * @param emailClient
	 * @param dateDepart
	 * @param transport
	 * @param villeDepartVol
	 * @param nbNuits
	 * @param nbParticipantsAdulte
	 * @param nbParticipantsEnfant
	 * @param nbParticipantsBebe
	 * @param montantTotalBase
	 * @param montantTotalVol
	 * @param montantTotalHebergement
	 * @param montantTotalTTC
	 * @param idDestination
	 */
	public Devis(int idDevis, String continentDestination, String paysDestination, String nomClient, String emailClient,
			Date dateDepart, boolean transport, String villeDepartVol, int nbNuits, int nbParticipantsAdulte,
			int nbParticipantsEnfant, int nbParticipantsBebe, float montantTotalBase, float montantTotalVol,
			float montantTotalHebergement, float montantTotalTTC, int idDestination) {
		super();
		this.idDevis = idDevis;
		this.continentDestination = continentDestination;
		this.paysDestination = paysDestination;
		this.nomClient = nomClient;
		this.emailClient = emailClient;
		this.dateDepart = dateDepart;
		this.transport = transport;
		this.villeDepartVol = villeDepartVol;
		this.nbNuits = nbNuits;
		this.nbParticipantsAdulte = nbParticipantsAdulte;
		this.nbParticipantsEnfant = nbParticipantsEnfant;
		this.nbParticipantsBebe = nbParticipantsBebe;
		this.montantTotalBase = montantTotalBase;
		this.montantTotalVol = montantTotalVol;
		this.montantTotalHebergement = montantTotalHebergement;
		this.montantTotalTTC = montantTotalTTC;
		this.idDestination = idDestination;
	}


	/**
	 * Permet d'obtenir l'identifiant du devis
	 * @return identifiant du devis
	 */
	public int getIdDevis() {
		return idDevis;
	}

	/**
	 * Permet de modifier l'identifiant du devis
	 * @param idDevis
	 */
	public void setIdDevis(int idDevis) {
		this.idDevis = idDevis;
	}

	/**
	 * Permet d'obtenir le nom du continent de la destination
	 * @return nom du continent de la destination
	 */
	public String getContinentDestination() {
		return continentDestination;
	}

	/**
	 * Permet de modifier le nom du continent de la destination
	 * @param continentDestination
	 */
	public void setContinentDestination(String continentDestination) {
		this.continentDestination = continentDestination;
	}

	/**
	 * Permet d'obtenir le nom du pays de la destination
	 * @return nom du pays de la destination
	 */
	public String getPaysDestination() {
		return paysDestination;
	}

	/**
	 * Permet de modifier le nom du pays de la destination
	 * @param paysDestination
	 */
	public void setPaysDestination(String paysDestination) {
		this.paysDestination = paysDestination;
	}

	/**
	 * Permet d'obtenir le nom du client
	 * @return nom du client
	 */
	public String getNomClient() {
		return nomClient;
	}

	/**
	 * Permet de modifier le nom du client
	 * @param nomClient
	 */
	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	/**
	 * Permet d'obtenir l'adresse email du client
	 * @return adresse email du client
	 */
	public String getEmailClient() {
		return emailClient;
	}

	/**
	 * Permet de modifier l'adresse email du client
	 * @param emailClient
	 */
	public void setEmailClient(String emailClient) {
		this.emailClient = emailClient;
	}

	/**
	 * Permet d'obtenir la date de départ souhaitée
	 * @return date de départ souhaitée
	 */
	public Date getDateDepart() {
		return dateDepart;
	}

	/**
	 * Permet de modifier la date de départ souhaitée
	 * @param dateDepart
	 */
	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}

	/**
	 * Renseigne si le vol est inclu
	 * @return vrai si le vol est inclu
	 */
	public boolean isTransport() {
		return transport;
	}

	/**
	 * Permet de modifier si le vol est inclu 
	 * @param transport
	 */
	public void setTransport(boolean transport) {
		this.transport = transport;
	}

	/**
	 * Permet d'obtenir la ville de départ du vol
	 * @return ville de départ du vol
	 */
	public String getVilleDepartVol() {
		return villeDepartVol;
	}

	/**
	 * Permet de modifier la ville de départ du vol
	 * @param villeDepartVol
	 */
	public void setVilleDepartVol(String villeDepartVol) {
		this.villeDepartVol = villeDepartVol;
	}

	/**
	 * Permet d'obtenir le nombre de nuits souhaitées
	 * @return nombre de nuits souhaitées
	 */
	public int getNbNuits() {
		return nbNuits;
	}

	/**
	 * Permet de modifier le nombre de nuits souhaitées
	 * @param nbNuits
	 */
	public void setNbNuits(int nbNuits) {
		this.nbNuits = nbNuits;
	}

	/**
	 * Permet d'obtenir le nombre de participants adulte
	 * @return le nombre de participants adulte
	 */
	public int getNbParticipantsAdulte() {
		return nbParticipantsAdulte;
	}

	/**
	 * Permet de modifier le nombre de participants adulte
	 * @param nbParticipantsAdulte
	 */
	public void setNbParticipantsAdulte(int nbParticipantsAdulte) {
		this.nbParticipantsAdulte = nbParticipantsAdulte;
	}

	/**
	 * Permet d'obtenir le nombre de participants enfant
	 * @return le nombre de participants enfant
	 */
	public int getNbParticipantsEnfant() {
		return nbParticipantsEnfant;
	}

	/**
	 * Permet de modifier le nombre de participants enfant
	 * @param nbParticipantsEnfant
	 */
	public void setNbParticipantsEnfant(int nbParticipantsEnfant) {
		this.nbParticipantsEnfant = nbParticipantsEnfant;
	}

	/**
	 * Permet d'obtenir le nombre de participants bébé
	 * @return le nombre de participants bébé
	 */
	public int getNbParticipantsBebe() {
		return nbParticipantsBebe;
	}

	/**
	 * Permet de modifier le nombre de participants bébé
	 * @param nbParticipantsBebe
	 */
	public void setNbParticipantsBebe(int nbParticipantsBebe) {
		this.nbParticipantsBebe = nbParticipantsBebe;
	}

	/**
	 * Permet d'obtenir le montant total du vol du séjour
	 * @return montant total du vol du séjour
	 */
	public float getMontantTotalBase() {
		return montantTotalBase;
	}

	/**
	 * Permet de modifier le montant total du vol du séjour
	 * @param montantTotalBase
	 */
	public void setMontantTotalBase(float montantTotalBase) {
		this.montantTotalBase = montantTotalBase;
	}

	/**
	 * Permet d'obtenir le montant total du vol du séjour
	 * @return montant total du vol du séjour
	 */
	public float getMontantTotalVol() {
		return montantTotalVol;
	}

	/**
	 * Permet de modifier le montant total du vol du séjour
	 * @param montantTotalVol
	 */
	public void setMontantTotalVol(float montantTotalVol) {
		this.montantTotalVol = montantTotalVol;
	}

	/**
	 * Permet d'obtenir le montant total de l'hébergement du séjour
	 * @return montant total de l'hébergement du séjour
	 */
	public float getMontantTotalHebergement() {
		return montantTotalHebergement;
	}

	/**
	 * Permet de modifier le montant total de l'hébergement du séjour
	 * @param montantTotalHebergement
	 */
	public void setMontantTotalHebergement(float montantTotalHebergement) {
		this.montantTotalHebergement = montantTotalHebergement;
	}

	/**
	 * Permet d'obtenir le montant total TTC du séjour
	 * @return montant total TTC du séjour
	 */
	public float getMontantTotalTTC() {
		return montantTotalTTC;
	}

	/**
	 * Permet de modifier le montant total TTC du séjour
	 * @param montantTotalTTC
	 */
	public void setMontantTotalTTC(float montantTotalTTC) {
		this.montantTotalTTC = montantTotalTTC;
	}

	/**
	 * Permet d'obtenir l'identifiant de la destination
	 * @return identifiant de la destination
	 */
	public int getIdDestination() {
		return idDestination;
	}

	/**
	 * Permet de modifier l'identifiant de la destination
	 * @param idDestination
	 */
	public void setIdDestination(int idDestination) {
		this.idDestination = idDestination;
	}
}
