package voyage.backinvitationauvoyage.hotel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.hebergement.bean.Hebergement;
import voyage.backinvitationauvoyage.hebergement.service.HebergementService;
import voyage.backinvitationauvoyage.hotel.HotelException;
import voyage.backinvitationauvoyage.hotel.HotelException.MessageHotelException;
import voyage.backinvitationauvoyage.hotel.bean.Hotel;
import voyage.backinvitationauvoyage.hotel.dao.HotelDaoService;

/**
 * Couche Implémentation de HotelService
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class HotelServiceImpl implements HotelService{

	@Autowired
	private HotelDaoService hotelDaoService;

	@Autowired
	private HebergementService hebergementService;

	@Override
	public List<Hotel> afficherTousLesHotels() {
		return hotelDaoService.findAll();
	}

	@Override
	public Hotel afficherHotelParId(int idHotel) throws HotelException {
		if(idHotel > 0) {
			return hotelDaoService.getOne(idHotel);			
		} else {
			throw new HotelException(MessageHotelException.PARAMETRE_INCORRECT.getMessage());
		}
	}

	@Override
	public Hotel afficherHotelParIdDestination(int idDestination) throws HebergementException, HotelException {
		if(idDestination > 0) {
			Hebergement hebergement = hebergementService.afficherHebergementParIdDestination(idDestination);

			if(hebergement != null) {
				return afficherHotelParId(hebergement.getIdHotel());
			} else {
				throw new HotelException(MessageHotelException.PARAMETRE_INTERNE_NULL.getMessage());
			}			
		} else {
			throw new HotelException(MessageHotelException.PARAMETRE_INCORRECT.getMessage());
		}	
	}
}
