package voyage.backinvitationauvoyage.hotel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.hotel.HotelException;
import voyage.backinvitationauvoyage.hotel.bean.Hotel;
import voyage.backinvitationauvoyage.hotel.service.HotelService;

/**
 * Controller de Hotel
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/hotels")
public class HotelController {

	@Autowired
	private HotelService hotelService;
	
	/**
	 * Controller pour afficher tous les hotels
	 * @return Liste des hotels
	 */
	@GetMapping(path = "/tousLesHotels")
	public List<Hotel> afficherTousLesHotels(){
		return hotelService.afficherTousLesHotels();
	}
	
	/**
	 * Controller pour afficher l'hotel associé à l'idHotel entré en paramètre
	 * @param idHotel
	 * @return hotel associé à l'idHotel entré en paramètre
	 * @throws HotelException
	 */
	@GetMapping(path = "/hotelParId/{idHotel}")
	public Hotel afficherHotelParId(@PathVariable int idHotel) throws HotelException{
		return hotelService.afficherHotelParId(idHotel);
	}
	
	/**
	 * Controller pour afficher l'hotel associé à l'idDestination entré en paramètre
	 * @param idDestination
	 * @return hotel associé à l'idDestination entré en paramètre
	 * @throws HebergementException
	 * @throws HotelException
	 */
	@GetMapping(path = "/hotelParIdDestination/{idDestination}")
	public Hotel afficherHotelParIdDestination(@PathVariable int idDestination) throws HebergementException, HotelException {
		return hotelService.afficherHotelParIdDestination(idDestination);
	}
}
