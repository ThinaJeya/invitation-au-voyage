package voyage.backinvitationauvoyage.hotel;

public class HotelException extends Exception {
	
	public enum MessageHotelException{
		PARAMETRE_INCORRECT("Le paramètre entré est incorrect"),
		PARAMETRE_INTERNE_NULL("Un des paramètres interne vaut null");
		
		private String message;

		private MessageHotelException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}
	
	public HotelException(String messageException) {
		super(messageException);
	}
}
