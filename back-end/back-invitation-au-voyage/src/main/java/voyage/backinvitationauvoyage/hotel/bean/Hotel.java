package voyage.backinvitationauvoyage.hotel.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe Bean de Hotel
 * @author Pirathina Jeyakumar
 *
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "Hotel")
public class Hotel {

	/**
	 * Identifiant de l'hotel
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_hotel")
	private int idHotel;
	
	/**
	 * Nom de l'hotel
	 */
	@Column(name = "nom_hotel")
	private String nomHotel;
	
	/**
	 * Nom de la chambre
	 */
	@Column(name = "nom_chambre")
	private String nomChambre;
	
	/**
	 * Constructeur par défaut de Hotel
	 */
	public Hotel() {
		super();
	}

	/**
	 * Constructeur de Hotel
	 * @param idHotel
	 * @param nomHotel
	 * @param nomChambre
	 */
	public Hotel(int idHotel, String nomHotel, String nomChambre) {
		super();
		this.idHotel = idHotel;
		this.nomHotel = nomHotel;
		this.nomChambre = nomChambre;
	}

	/**
	 * Permet d'obtenir l'identifiant de l'hotel
	 * @return identifiant de l'hotel
	 */
	public int getIdHotel() {
		return idHotel;
	}

	/**
	 * Permet de modifier l'identifiant de l'hotel
	 * @param idHotel
	 */
	public void setIdHotel(int idHotel) {
		this.idHotel = idHotel;
	}

	/**
	 * Permet d'obtenir le nom de l'hotel
	 * @return nom de l'hotel
	 */
	public String getNomHotel() {
		return nomHotel;
	}

	/**
	 * Permet de modifier le nom de l'hotel
	 * @param nomHotel
	 */
	public void setNomHotel(String nomHotel) {
		this.nomHotel = nomHotel;
	}

	/**
	 * Permet d'obtenir le nom de la chambre
	 * @return nom de la chambre
	 */
	public String getNomChambre() {
		return nomChambre;
	}

	/**
	 * Permet de modifier le nom de la chambre
	 * @param nomChambre
	 */
	public void setNomChambre(String nomChambre) {
		this.nomChambre = nomChambre;
	}
}
