package voyage.backinvitationauvoyage.hotel.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import voyage.backinvitationauvoyage.hotel.bean.Hotel;

/**
 * Couche Dao de HotelService
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface HotelDaoService extends JpaRepository<Hotel, Integer>{
}
