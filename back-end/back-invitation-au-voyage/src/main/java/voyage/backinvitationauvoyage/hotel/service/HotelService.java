package voyage.backinvitationauvoyage.hotel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import voyage.backinvitationauvoyage.hebergement.HebergementException;
import voyage.backinvitationauvoyage.hotel.HotelException;
import voyage.backinvitationauvoyage.hotel.bean.Hotel;

/**
 * Couche Service de Hotel
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface HotelService {

	/**
	 * Service pour afficher tous les hotels
	 * @return Liste des hotels
	 */
	public List<Hotel> afficherTousLesHotels();

	/**
	 * Service pour afficher l'hotel associé à l'idHotel entré en paramètre
	 * @param idHotel
	 * @return hotel associé à l'idHotel entré en paramètre
	 * @throws HotelException
	 */
	public Hotel afficherHotelParId(int idHotel) throws HotelException;

	/**
	 * Service pour afficher l'hotel associé à l'idDestination entré en paramètre
	 * @param idDestination
	 * @return hotel associé à l'idDestination entré en paramètre
	 * @throws HebergementException
	 * @throws HotelException
	 */
	public Hotel afficherHotelParIdDestination(int idDestination) throws HebergementException, HotelException;
}
