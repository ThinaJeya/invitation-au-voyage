package voyage.backinvitationauvoyage.dto;

import java.sql.Date;

/**
 * Classe Bean de FormulaireDevisDTO
 * @author Pirathina Jeyakumar
 *
 */
public class FormulaireDevisDTO {
	
	/**
	 * Identifiant (id) de la destination
	 */
	private int idDestination;
	
	/**
	 * Nom du continent de la destination
	 */
	private String continentDestination;
	
	/**
	 * Nom du pays de la destination
	 */
	private String paysDestination;
	
	/**
	 * Date de départ souhaitée
	 */
	private Date dateDepart;
	
	/**
	 * Nom du client
	 */
	private String nomClient;
	
	/**
	 * Adresse email du client
	 */
	private String emailClient;
	
	/**
	 * Nombre de nuits souhaitées
	 */
	private int nbNuits;
	
	/**
	 * Nombre de participants adulte
	 */
	private int nbParticipantsAdulte;
	
	/**
	 * Nombre de participants enfant
	 */
	private int nbParticipantsEnfant;
	
	/**
	 * Nombre de participants bébé
	 */
	private int nbParticipantsBebe;
	
	/**
	 * Renseigne si le vol est inclu 
	 */
	private boolean transport;
	
	/**
	 * Nom de la ville de départ du vol
	 */
	private String villeDepartVol;

	/**
	 * Constructeur de FormulaireDTO
	 * @param idDestination
	 * @param continentDestination
	 * @param paysDestination
	 * @param dateDepart
	 * @param nomClient
	 * @param emailClient
	 * @param nbNuits
	 * @param nbParticipantsAdulte
	 * @param nbParticipantsEnfant
	 * @param nbParticipantsBebe
	 * @param transport
	 * @param villeDepartVol
	 */
	public FormulaireDevisDTO(int idDestination, String continentDestination, String paysDestination, Date dateDepart,
			String nomClient, String emailClient, int nbNuits, int nbParticipantsAdulte, int nbParticipantsEnfant,
			int nbParticipantsBebe, boolean transport, String villeDepartVol) {
		super();
		this.idDestination = idDestination;
		this.continentDestination = continentDestination;
		this.paysDestination = paysDestination;
		this.dateDepart = dateDepart;
		this.nomClient = nomClient;
		this.emailClient = emailClient;
		this.nbNuits = nbNuits;
		this.nbParticipantsAdulte = nbParticipantsAdulte;
		this.nbParticipantsEnfant = nbParticipantsEnfant;
		this.nbParticipantsBebe = nbParticipantsBebe;
		this.transport = transport;
		this.villeDepartVol = villeDepartVol;
	}

	/**
	 * Constructeur par défaut de FormulaireDTO
	 */
	public FormulaireDevisDTO() {
		super();
	}

	/**
	 * Permet d'obtenir l'identifiant de la destination
	 * @return identifiant de la destination
	 */
	public int getIdDestination() {
		return idDestination;
	}

	/**
	 * Permet de modifier l'identifiant de la destination
	 * @param idDestination
	 */
	public void setIdDestination(int idDestination) {
		this.idDestination = idDestination;
	}

	/**
	 * Permet d'obtenir le nom du continent de la destination
	 * @return nom du continent de la destination
	 */
	public String getContinentDestination() {
		return continentDestination;
	}

	/**
	 * Permet de modifier le nom du continent de la destination
	 * @param continentDestination
	 */
	public void setContinentDestination(String continentDestination) {
		this.continentDestination = continentDestination;
	}

	/**
	 * Permet d'obtenir le nom du pays de la destination
	 * @return nom du pays de la destination
	 */
	public String getPaysDestination() {
		return paysDestination;
	}

	/**
	 * Permet de modifier le nom du pays de la destination
	 * @param paysDestination
	 */
	public void setPaysDestination(String paysDestination) {
		this.paysDestination = paysDestination;
	}

	/**
	 * Permet d'obtenir la date de départ souhaitée
	 * @return la date de départ souhaitée
	 */
	public Date getDateDepart() {
		return dateDepart;
	}

	/**
	 * Permet de modifier la date de départ souhaitée
	 * @param dateDepart
	 */
	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}

	/**
	 * Permet d'obtenir le nom du client
	 * @return le nom du client
	 */
	public String getNomClient() {
		return nomClient;
	}

	/**
	 * Permet de modifier le nom du client
	 * @param nomClient
	 */
	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	/**
	 * Permet d'obtenir l'adresse email du client
	 * @return l'adresse email du client
	 */
	public String getEmailClient() {
		return emailClient;
	}

	/**
	 * Permet de modifier l'adresse email du client
	 * @param emailClient
	 */
	public void setEmailClient(String emailClient) {
		this.emailClient = emailClient;
	}

	/**
	 * Permet d'obtenir le nombre de nuits souhaitées
	 * @return le nombre de nuits souhaitées
	 */
	public int getNbNuits() {
		return nbNuits;
	}

	/**
	 * Permet de modifier le nombre de nuits souhaitées
	 * @param nbNuits
	 */
	public void setNbNuits(int nbNuits) {
		this.nbNuits = nbNuits;
	}

	/**
	 * Permet d'obtenir le nombre de participants adulte
	 * @return le nombre de participants adulte
	 */
	public int getNbParticipantsAdulte() {
		return nbParticipantsAdulte;
	}

	/**
	 * Permet de modifier le nombre de participants adulte
	 * @param nbParticipantsAdulte
	 */
	public void setNbParticipantsAdulte(int nbParticipantsAdulte) {
		this.nbParticipantsAdulte = nbParticipantsAdulte;
	}

	/**
	 * Permet d'obtenir le nombre de participants enfant
	 * @return le nombre de participants enfant
	 */
	public int getNbParticipantsEnfant() {
		return nbParticipantsEnfant;
	}

	/**
	 * Permet de modifier le nombre de participants enfant
	 * @param nbParticipantsEnfant
	 */
	public void setNbParticipantsEnfant(int nbParticipantsEnfant) {
		this.nbParticipantsEnfant = nbParticipantsEnfant;
	}

	/**
	 * Permet d'obtenir le nombre de participants bébé
	 * @return le nombre de participants bébé
	 */
	public int getNbParticipantsBebe() {
		return nbParticipantsBebe;
	}

	/**
	 * Permet de modifier le nombre de participants bébé
	 * @param nbParticipantsBebe
	 */
	public void setNbParticipantsBebe(int nbParticipantsBebe) {
		this.nbParticipantsBebe = nbParticipantsBebe;
	}

	/**
	 * Renseigne si le vol est inclu
	 * @return vrai si le vol est inclu
	 */
	public boolean isTransport() {
		return transport;
	}

	/**
	 * Permet de modifier si le vol est inclu 
	 * @param transport
	 */
	public void setTransport(boolean transport) {
		this.transport = transport;
	}

	/**
	 * Permet d'obtenir la ville de départ du vol
	 * @return la ville de départ du vol
	 */
	public String getVilleDepartVol() {
		return villeDepartVol;
	}

	/**
	 * Permet de modifier la ville de départ du vol
	 * @param villeDepartVol
	 */
	public void setVilleDepartVol(String villeDepartVol) {
		this.villeDepartVol = villeDepartVol;
	}
}
