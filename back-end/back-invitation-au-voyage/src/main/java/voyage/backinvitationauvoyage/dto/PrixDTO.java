package voyage.backinvitationauvoyage.dto;

/**
 * Classe Bean de PrixDTO
 * @author Pirathina Jeyakumar
 *
 */
public class PrixDTO {

	/**
	 * Prix de départ du séjour sans vol
	 */
	private float prixAPartirDeSansVol;
	
	/**
	 * Prix de départ du séjour vol inclu (depuis Paris)
	 */
	private float prixAPartirDeAvecVol;
	
	/**
	 * Prix du vol au départ de Paris
	 */
	private float prixVolAuDepartDeParis;
	
	/**
	 * Prix du vol au départ de Marseille
	 */
	private float prixVolAuDepartDeMarseille;
	
	/**
	 * Prix du vol au départ de Lyon
	 */
	private float prixVolAuDepartDeLyon;
	
	/**
	 * Prix du vol au départ de Nantes
	 */
	private float prixVolAuDepartDeNantes;
	
	/**
	 * Constructeur de PrixDTO
	 * @param prixAPartirDeSansVol
	 * @param prixAPartirDeAvecVol
	 * @param prixVolAuDepartDeParis
	 * @param prixVolAuDepartDeMarseille
	 * @param prixVolAuDepartDeLyon
	 * @param prixVolAuDepartDeNantes
	 */
	public PrixDTO(float prixAPartirDeSansVol, float prixAPartirDeAvecVol, float prixVolAuDepartDeParis,
			float prixVolAuDepartDeMarseille, float prixVolAuDepartDeLyon, float prixVolAuDepartDeNantes) {
		super();
		this.prixAPartirDeSansVol = prixAPartirDeSansVol;
		this.prixAPartirDeAvecVol = prixAPartirDeAvecVol;
		this.prixVolAuDepartDeParis = prixVolAuDepartDeParis;
		this.prixVolAuDepartDeMarseille = prixVolAuDepartDeMarseille;
		this.prixVolAuDepartDeLyon = prixVolAuDepartDeLyon;
		this.prixVolAuDepartDeNantes = prixVolAuDepartDeNantes;
	}

	/**
	 * Constructeur par défaut de PrixDTO
	 */
	public PrixDTO() {
		super();
	}
	
	/**
	 * Permet d'obtenir le prix de départ du séjour sans vol
	 * @return prix de départ du séjour sans vol
	 */
	public float getPrixAPartirDeSansVol() {
		return prixAPartirDeSansVol;
	}

	/**
	 * Permet de modifier le prix de départ du séjour sans vol
	 * @param prixAPartirDeSansVol
	 */
	public void setPrixAPartirDeSansVol(float prixAPartirDeSansVol) {
		this.prixAPartirDeSansVol = prixAPartirDeSansVol;
	}

	/**
	 * Permet d'obtenir le prix de départ du séjour vol inclu (depuis Paris)
	 * @return prix de départ du séjour vol inclu (depuis Paris)
	 */
	public float getPrixAPartirDeAvecVol() {
		return prixAPartirDeAvecVol;
	}

	/**
	 * Permet de modifier le prix de départ du séjour vol inclu (depuis Paris)
	 * @param prixAPartirDeAvecVol
	 */
	public void setPrixAPartirDeAvecVol(float prixAPartirDeAvecVol) {
		this.prixAPartirDeAvecVol = prixAPartirDeAvecVol;
	}
	
	/**
	 * Permet d'obtenir le prix du vol au départ de Paris
	 * @return prix du vol au départ de Paris
	 */
	public float getPrixVolAuDepartDeParis() {
		return prixVolAuDepartDeParis;
	}
	
	/**
	 * Permet de modifier le prix du vol au départ de Paris
	 * @param prixVolAuDepartDeParis
	 */
	public void setPrixVolAuDepartDeParis(float prixVolAuDepartDeParis) {
		this.prixVolAuDepartDeParis = prixVolAuDepartDeParis;
	}
	
	/**
	 * Permet d'obtenir le prix du vol au départ de Marseille
	 * @return prix du vol au départ de Marseille
	 */
	public float getPrixVolAuDepartDeMarseille() {
		return prixVolAuDepartDeMarseille;
	}
	
	/**
	 * Permet de modifier le prix du vol au départ de Marseille
	 * @param prixVolAuDepartDeMarseille
	 */
	public void setPrixVolAuDepartDeMarseille(float prixVolAuDepartDeMarseille) {
		this.prixVolAuDepartDeMarseille = prixVolAuDepartDeMarseille;
	}
	
	/**
	 * Permet d'obtenir le prix du vol au départ de Lyon
	 * @return prix du vol au départ de Lyon
	 */
	public float getPrixVolAuDepartDeLyon() {
		return prixVolAuDepartDeLyon;
	}
	
	/**
	 * Permet de modifier le prix du vol au départ de Lyon
	 * @param prixVolAuDepartDeLyon
	 */
	public void setPrixVolAuDepartDeLyon(float prixVolAuDepartDeLyon) {
		this.prixVolAuDepartDeLyon = prixVolAuDepartDeLyon;
	}
	
	/**
	 * Permet d'obtenir le prix du vol au départ de Nantes
	 * @return prix du vol au départ de Nantes
	 */
	public float getPrixVolAuDepartDeNantes() {
		return prixVolAuDepartDeNantes;
	}
	
	/**
	 * Permet de modifier le prix du vol au départ de Nantes
	 * @param prixVolAuDepartDeNantes
	 */
	public void setPrixVolAuDepartDeNantes(float prixVolAuDepartDeNantes) {
		this.prixVolAuDepartDeNantes = prixVolAuDepartDeNantes;
	}
}
