package voyage.backinvitationauvoyage.dto;

/**
 * Classe Bean de SejourDTO
 * @author Pirathina Jeyakumar
 *
 */
public class SejourDTO {

	/**
	 * Identifiant (id) de la destination
	 */
	private int idDestination;

	/**
	 * Renseigne si le vol est inclu 
	 */
	private boolean transport;

	/**
	 * Nom de la ville de départ du vol
	 */
	private String villeDepart;

	/**
	 * Nombre de nuits souhaitées
	 */
	private int nbNuits;

	/**
	 * Nombre de participants adulte
	 */
	private int nbParticipantsAdulte;

	/**
	 * Nombre de participants enfant
	 */
	private int nbParticipantsEnfant;

	/**
	 * Constructeur par défaut de SejourDTO
	 */
	public SejourDTO() {
		super();
	}

	/**
	 * Constructeur de SejourDTO
	 * @param idDestination
	 * @param transport
	 * @param villeDepart
	 * @param nbNuits
	 * @param nbParticipantsAdulte
	 * @param nbParticipantsEnfant
	 */
	public SejourDTO(int idDestination, boolean transport, String villeDepart, int nbNuits, int nbParticipantsAdulte,
			int nbParticipantsEnfant) {
		super();
		this.idDestination = idDestination;
		this.transport = transport;
		this.villeDepart = villeDepart;
		this.nbNuits = nbNuits;
		this.nbParticipantsAdulte = nbParticipantsAdulte;
		this.nbParticipantsEnfant = nbParticipantsEnfant;
	}

	/**
	 * Permet d'obtenir l'identifiant de la destination
	 * @return identifiant de la destination
	 */
	public int getIdDestination() {
		return idDestination;
	}

	/**
	 * Permet de modifier l'identifiant de la destination
	 * @param idDestination
	 */
	public void setIdDestination(int idDestination) {
		this.idDestination = idDestination;
	}

	/**
	 * Renseigne si le vol est inclu
	 * @return vrai si le vol est inclu
	 */
	public boolean isTransport() {
		return transport;
	}

	/**
	 * Permet de modifier si le vol est inclu 
	 * @param transport
	 */
	public void setTransport(boolean transport) {
		this.transport = transport;
	}

	/**
	 * Permet d'obtenir la ville de départ du vol
	 * @return la ville de départ du vol
	 */
	public String getVilleDepart() {
		return villeDepart;
	}

	/**
	 * Permet de modifier la ville de départ du vol
	 * @param villeDepart
	 */
	public void setVilleDepart(String villeDepart) {
		this.villeDepart = villeDepart;
	}

	/**
	 * Permet d'obtenir le nombre de nuits souhaitées
	 * @return le nombre de nuits souhaitées
	 */
	public int getNbNuits() {
		return nbNuits;
	}

	/**
	 * Permet de modifier le nombre de nuits souhaitées
	 * @param nbNuits
	 */
	public void setNbNuits(int nbNuits) {
		this.nbNuits = nbNuits;
	}

	/**
	 * Permet d'obtenir le nombre de participants adulte
	 * @return le nombre de participants adulte
	 */
	public int getNbParticipantsAdulte() {
		return nbParticipantsAdulte;
	}

	/**
	 * Permet de modifier le nombre de participants adulte
	 * @param nbParticipantsAdulte
	 */
	public void setNbParticipantsAdulte(int nbParticipantsAdulte) {
		this.nbParticipantsAdulte = nbParticipantsAdulte;
	}

	/**
	 * Permet d'obtenir le nombre de participants enfant
	 * @return le nombre de participants enfant
	 */
	public int getNbParticipantsEnfant() {
		return nbParticipantsEnfant;
	}

	/**
	 * Permet de modifier le nombre de participants enfant
	 * @param nbParticipantsEnfant
	 */
	public void setNbParticipantsEnfant(int nbParticipantsEnfant) {
		this.nbParticipantsEnfant = nbParticipantsEnfant;
	}
}
