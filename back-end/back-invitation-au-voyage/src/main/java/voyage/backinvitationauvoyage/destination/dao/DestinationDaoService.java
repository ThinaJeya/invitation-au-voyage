package voyage.backinvitationauvoyage.destination.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import voyage.backinvitationauvoyage.destination.bean.Destination;

/**
 * Interface Dao de Destination Service
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface DestinationDaoService extends JpaRepository<Destination, Integer>{

}
