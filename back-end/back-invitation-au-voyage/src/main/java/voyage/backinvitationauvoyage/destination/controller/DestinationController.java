package voyage.backinvitationauvoyage.destination.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import voyage.backinvitationauvoyage.destination.DestinationException;
import voyage.backinvitationauvoyage.destination.bean.Destination;
import voyage.backinvitationauvoyage.destination.service.DestinationService;

/**
 * Classe controller de Destination
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/destinations")
public class DestinationController {

	@Autowired
	private DestinationService destinationService;
	
	/**
	 * Controller pour afficher la liste des destinations
	 * @return liste des destinations
	 */
	@GetMapping(path = "/toutesLesDestinations")
	public List<Destination> afficherToutesLesDestinations(){
		return destinationService.afficherToutesLesDestinations();
	}
	
	/**
	 * Controller pour afficher la destination associée à l'id destination entré en paramètre
	 * @param idDestination
	 * @return destination associée à l'id destination entré en paramètre
	 * @throws DestinationException
	 */
	@GetMapping(path = "/destinationParId/{idDestination}")
	public Destination afficherDestinationParId(@PathVariable int idDestination) throws DestinationException {
		return destinationService.afficherDestinationParId(idDestination);
	}
}
