package voyage.backinvitationauvoyage.destination;

/**
 * Classe Exception de Destination
 * @author Pirathina Jeyakumar
 *
 */
public class DestinationException extends Exception{

	public enum MessageDestinationException{
		PARAMETRE_INCORRECT("Le paramètre entré est incorrect");
		
		private String message;

		private MessageDestinationException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}
	
	public DestinationException(String messageException) {
		super(messageException);
	}
}
