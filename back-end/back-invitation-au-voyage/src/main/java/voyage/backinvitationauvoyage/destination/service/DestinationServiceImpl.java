package voyage.backinvitationauvoyage.destination.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import voyage.backinvitationauvoyage.destination.DestinationException;
import voyage.backinvitationauvoyage.destination.DestinationException.MessageDestinationException;
import voyage.backinvitationauvoyage.destination.bean.Destination;
import voyage.backinvitationauvoyage.destination.dao.DestinationDaoService;

/**
 * Implémentation de Service Destination
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class DestinationServiceImpl implements DestinationService {

	@Autowired
	private DestinationDaoService destinationDaoService;

	@Override
	public List<Destination> afficherToutesLesDestinations() {
		return destinationDaoService.findAll();
	}

	@Override
	public Destination afficherDestinationParId(int idDestination) throws DestinationException {
		
		if(idDestination > 0) {
			return destinationDaoService.getOne(idDestination);			
		} else {
			throw new DestinationException(MessageDestinationException.PARAMETRE_INCORRECT.getMessage());
		}
	}
}
