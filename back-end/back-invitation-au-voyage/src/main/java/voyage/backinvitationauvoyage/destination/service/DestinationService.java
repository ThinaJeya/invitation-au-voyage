package voyage.backinvitationauvoyage.destination.service;

import java.util.List;

import org.springframework.stereotype.Service;

import voyage.backinvitationauvoyage.destination.DestinationException;
import voyage.backinvitationauvoyage.destination.bean.Destination;

/**
 * Service de Destination
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface DestinationService {

	/**
	 * Service pour afficher la liste des destinations
	 * @return liste des destinations
	 */
	public List<Destination> afficherToutesLesDestinations();

	/**
	 * Service pour afficher la destination associée à l'id destination entré en paramètre
	 * @param idDestination
	 * @return destination associée à l'id destination entré en paramètre
	 * @throws DestinationException
	 */
	public Destination afficherDestinationParId(int idDestination) throws DestinationException;

}
