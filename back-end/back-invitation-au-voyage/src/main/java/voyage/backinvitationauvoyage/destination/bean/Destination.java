package voyage.backinvitationauvoyage.destination.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe Bean Destination
 * @author Pirathina Jeyakumar
 *
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "Destination")
public class Destination {

	/**
	 * Identifiant (id) de la destination
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_destination")
	private int idDestination;

	/**
	 * Nom du continent de la destination
	 */
	@Column(name = "continent_destination")
	private String continentDestination;

	/**
	 * Nom du pays de la destination
	 */
	@Column(name = "pays_destination")
	private String paysDestination;

	/**
	 * Nom de la ville de la destination
	 */
	@Column(name = "ville_destination")
	private String villeDestination;

	/**
	 * Montant du prix de base associé à la destination
	 */
	@Column(name = "prix_base_adulte")
	private float prixBaseAdulte;

	/**
	 * Constructeur par défaut de Destination
	 */
	public Destination() {
		super();
	}

	/**
	 * Constructeur de Destination
	 * @param idDestination
	 * @param continentDestination
	 * @param paysDestination
	 * @param villeDestination
	 * @param prixBaseAdulte
	 */
	public Destination(int idDestination, String continentDestination, String paysDestination, String villeDestination,
			float prixBaseAdulte) {
		super();
		this.idDestination = idDestination;
		this.continentDestination = continentDestination;
		this.paysDestination = paysDestination;
		this.villeDestination = villeDestination;
		this.prixBaseAdulte = prixBaseAdulte;
	}

	/**
	 * Permet d'obtenir l'identifiant de la destination
	 * @return identifiant de la destination
	 */
	public int getIdDestination() {
		return idDestination;
	}

	/**
	 * Permet de modifier l'identifiant de la destination
	 * @param idDestination
	 */
	public void setIdDestination(int idDestination) {
		this.idDestination = idDestination;
	}

	/**
	 * Permet d'obtenir le nom du continent de la destination
	 * @return nom du continent de la destination
	 */
	public String getContinentDestination() {
		return continentDestination;
	}

	/**
	 * Permet de modifier le nom du continent de la destination
	 * @param continentDestination
	 */
	public void setContinentDestination(String continentDestination) {
		this.continentDestination = continentDestination;
	}

	/**
	 * Permet d'obtenir le nom du pays de la destination
	 * @return nom du pays de la destination
	 */
	public String getPaysDestination() {
		return paysDestination;
	}

	/**
	 * Permet de modifier le nom du pays de la destination
	 * @param paysDestination
	 */
	public void setPaysDestination(String paysDestination) {
		this.paysDestination = paysDestination;
	}

	/**
	 * Permet d'obtenir le nom de la ville de la destination
	 * @return nom de la ville de la destination
	 */
	public String getVilleDestination() {
		return villeDestination;
	}

	/**
	 * Permet de modifier le nom de la ville de la destination
	 * @param villeDestination
	 */
	public void setVilleDestination(String villeDestination) {
		this.villeDestination = villeDestination;
	}

	/**
	 * Permet d'obtenir le montant du prix de base associé à la destination
	 * @return montant du prix de base associé à la destination
	 */
	public float getPrixBaseAdulte() {
		return prixBaseAdulte;
	}

	/**
	 * Permet de modifier le montant du prix de base associé à la destination
	 * @param prixBaseAdulte
	 */
	public void setPrixBaseAdulte(float prixBaseAdulte) {
		this.prixBaseAdulte = prixBaseAdulte;
	}
}
