import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ContinentComponent } from './continent/continent.component';
import { PropositionDestinationComponent } from './proposition-destination/proposition-destination.component';
import { DetailsVoyageComponent } from './details-voyage/details-voyage.component';
import { DevisFinaliseComponent } from './devis-finalise/devis-finalise.component';
import { MentionsLegalesComponent } from './mentions-legales/mentions-legales.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { ContinentsDestinationsService } from './services/continents-destinations.service';
import { DatePipe } from '@angular/common';
import { DevisService } from './services/devis.service';
import { ReactiveFormsModule } from '@angular/forms';

const appRoutes: Routes = [
  { path: 'accueil/continents', component: ContinentComponent },
  { path: 'accueil/continents/:nom-continent/destinations', component: PropositionDestinationComponent },
  { path: 'accueil/continents/:nom-continent/destinations/:pays-destination/:id', component: DetailsVoyageComponent },
  { path: 'accueil/continents/:nom-continent/destinations/:pays-destination/:id/devis/:nom-client', component: DevisFinaliseComponent },
  { path: 'accueil/mentions-legales', component: MentionsLegalesComponent },
  { path: '', component: ContinentComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: '**', redirectTo: 'not-found' }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ContinentComponent,
    PropositionDestinationComponent,
    DetailsVoyageComponent,
    DevisFinaliseComponent,
    MentionsLegalesComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ContinentsDestinationsService, DatePipe, DevisService],
  bootstrap: [AppComponent]
})
export class AppModule { }
