import { Injectable } from '@angular/core';
import { CONTINENTS } from '../bean/continent';
import { DESTINATIONSAFRIQUE, DESTINATIONSAMERIQUE, DESTINATIONSASIE, DESTINATIONSEUROPE, DESTINATIONSOCEANIE } from '../bean/destination';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class ContinentsDestinationsService {

  arrayDestinations: any[] = [];
  destination: any;
  plusPetiteDureeSejour: number = 0;

  AFRIQUE = 'Afrique';
  AMERIQUE = 'Amérique';
  ASIE = 'Asie';
  EUROPE = 'Europe';
  OCEANIE = 'Océanie';

  ARRAYASSURANCE_MULTIRISQUEPREMIUM = [
    'Annulation "toutes causes" jusitifiées',
    'Départ manqué ou retard d\'avion',
    'Assurance baggages',
    'Départ ou retour impossible',
    'Asistance médicale 24h/24 et 7j/7',
    'Assistance rapatriement',
    'Garantie COVID-19'
  ];

  ARRAYASSURANCE_ANNULATIONPREMIUM = [
    'Annulation "toutes causes" jusitifiées (Evènements soudains et exterieurs, Imprévus justifiés, Interruption de séjour)',
    'Assistance rapratriement',
    'Garantie COVID-19'
  ];


  constructor(private httpClient: HttpClient, private spinner: NgxSpinnerService) { }

  chargementSpinner(){
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 2000);
  }

  /**
   * Service pour récupérer l'array des noms des continents
   * @returns l'array des noms des continents
   */
  getContinents() {
    return CONTINENTS;
  }

  /**
   * Service pour récupérer l'array des détails des destinations en fonction du nom du continent entré en paramètre
   * @param nomContinent 
   * @returns l'array des détails des destinations en fonction du nom du continent entré en paramètre
   */
  getDestinationsSelonContinent(nomContinent: string){
    switch (nomContinent) {
      case this.AFRIQUE:
        return DESTINATIONSAFRIQUE;
      case this.AMERIQUE:
        return DESTINATIONSAMERIQUE;
      case this.ASIE:
        return DESTINATIONSASIE;
      case this.EUROPE:
        return DESTINATIONSEUROPE;
      case this.OCEANIE:
        return DESTINATIONSOCEANIE;
    }
  }

  /**
   * Service pour récupérer les informations de l'hôtel côté serveur associées à l'id destination entré en paramètre
   * @param idDestination 
   * @returns les informations de l'hôtel associées à l'id destination entré en paramètre
   */
  getInformationsHotel(idDestination: number){
    return this.httpClient.get<any>('http://localhost:8081/hotels/hotelParIdDestination/' + idDestination);
  }

  /**
   * Service pour recupérer les informations de l'assurance multirisque premium
   * @returns array contenant les informations de l'assurance multirisque premium
   */
  getInformationsAssuranceMultirisquePremium(){
    return this.ARRAYASSURANCE_MULTIRISQUEPREMIUM;
  }

  /**
   * Service pour recupérer les informations de l'assurance annulation premium
   * @returns array contenant les informations de l'assurance annulation premium
   */
  getInformationsAssuranceAnnulationPremium(){
    return this.ARRAYASSURANCE_ANNULATIONPREMIUM;
  }

}
