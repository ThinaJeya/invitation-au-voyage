import { Injectable } from '@angular/core';
import { Devis } from '../bean/devis';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DevisService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    })
  };

  constructor(private httpClient: HttpClient) { }

  /**
   * Service pour récupérer les tarifs de départ pour le séjour depuis le serveur
   * @param data 
   * @returns les tarifs de départ pour le séjour
   */
  getPrixSejourAPartirDe(data: any) {
    return this.httpClient.post('http://localhost:8081/devis/prixSejourAPartirDe', data, this.httpOptions);
  }

  /**
   * Service pour récupérer les tarifs des sejours selon leurs durées depuis le serveur
   * @param idDestination 
   * @returns les tarifs des sejours selon leurs durées
   */
  getPrixHebergementSelonDureesDestination(idDestination: number) {
    return this.httpClient.get<any>('http://localhost:8081/tarifDureeHebergement/tarifDureeHebergementSelonIdDestination/' + idDestination);
  }

  /**
   * Service pour enregistrer un devis côté serveur
   * @param devis 
   * @returns boolean true si le devis est enregistré côté serveur
   */
  enregistrerDevis(devis: Devis){
    return this.httpClient.post('http://localhost:8081/devis/enregistrerDevis', devis, this.httpOptions);
  }

  /**
   * Service pour récupérer le devis enregistré en fonction des paramètres entrés depuis le serveur
   * @param continentDestination 
   * @param paysDestination 
   * @param idDestination 
   * @param nomClient 
   * @returns devis enregistré en fonction des paramètres entrés
   */
  getDevisFinalise(continentDestination: string, paysDestination: string, idDestination: number, nomClient: string){
    return this.httpClient.get<any>('http://localhost:8081/devis/afficherDevisEnregistre/' + continentDestination + "/" + paysDestination + "/" + idDestination + "/" + nomClient);
  }
}

