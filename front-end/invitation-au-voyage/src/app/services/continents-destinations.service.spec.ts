import { TestBed } from '@angular/core/testing';

import { ContinentsDestinationsService } from './continents-destinations.service';

describe('ContinentsDestinationsService', () => {
  let service: ContinentsDestinationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContinentsDestinationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
