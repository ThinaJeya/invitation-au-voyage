import { Component, OnInit } from '@angular/core';
import { ContinentsDestinationsService } from '../services/continents-destinations.service';

@Component({
  selector: 'app-continent',
  templateUrl: './continent.component.html',
  styleUrls: ['./continent.component.scss']
})
export class ContinentComponent implements OnInit {

  continents: any[];

  constructor(private continentsDestinationsService: ContinentsDestinationsService) { }

  ngOnInit(): void {

    this.continentsDestinationsService.chargementSpinner();
    
    this.continents = this.continentsDestinationsService.getContinents();
   
  }


}
