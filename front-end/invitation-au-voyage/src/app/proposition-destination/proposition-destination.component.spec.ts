import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropositionDestinationComponent } from './proposition-destination.component';

describe('PropositionDestinationComponent', () => {
  let component: PropositionDestinationComponent;
  let fixture: ComponentFixture<PropositionDestinationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropositionDestinationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropositionDestinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
