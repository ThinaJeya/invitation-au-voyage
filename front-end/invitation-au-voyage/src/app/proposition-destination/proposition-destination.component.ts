import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContinentsDestinationsService } from '../services/continents-destinations.service';


@Component({
  selector: 'app-proposition-destination',
  templateUrl: './proposition-destination.component.html',
  styleUrls: ['./proposition-destination.component.scss']
})
export class PropositionDestinationComponent implements OnInit {

  /**
   * Nom du continent inscrit dans l'url
   */
  continentParams: string;
  
  /**
   * Tableau contenant les détails d'informations des destinations de l'Afrique
   */
  destinationsAfrique: any[];

    /**
   * Tableau contenant les détails d'informations des destinations de l'Amérique
   */
  destinationsAmerique: any[];

    /**
   * Tableau contenant les détails d'informations des destinations de l'Asie
   */
  destinationsAsie: any[];

    /**
   * Tableau contenant les détails d'informations des destinations de l'Europe
   */
  destinationsEurope: any[];

    /**
   * Tableau contenant les détails d'informations des destinations de l'Océanie
   */
  destinationsOceanie: any[];

  tableauVide: any[];

  constructor(private activatedRoute: ActivatedRoute, private continentsDestinationsService: ContinentsDestinationsService) { }

  ngOnInit(): void {

    this.continentsDestinationsService.chargementSpinner();

    this.activatedRoute.paramMap.subscribe(params => {
      this.continentParams = params.get('nom-continent');
    });

    this.recupererTableauxDestinations();
  }

  /**
   * Permet de récupérer les arrays de destinations de tous les continents
   */
  recupererTableauxDestinations() {
    this.destinationsAfrique = this.continentsDestinationsService.getDestinationsSelonContinent('Afrique');
    this.destinationsAmerique = this.continentsDestinationsService.getDestinationsSelonContinent('Amérique');
    this.destinationsAsie = this.continentsDestinationsService.getDestinationsSelonContinent('Asie');
    this.destinationsEurope = this.continentsDestinationsService.getDestinationsSelonContinent('Europe');
    this.destinationsOceanie = this.continentsDestinationsService.getDestinationsSelonContinent('Océanie');
  }

}
