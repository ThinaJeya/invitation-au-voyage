import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevisFinaliseComponent } from './devis-finalise.component';

describe('DevisFinaliseComponent', () => {
  let component: DevisFinaliseComponent;
  let fixture: ComponentFixture<DevisFinaliseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DevisFinaliseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DevisFinaliseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
