import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DevisFinalise } from '../bean/devis-finalise';
import { ContinentsDestinationsService } from '../services/continents-destinations.service';
import { DevisService } from '../services/devis.service';

@Component({
  selector: 'app-devis-finalise',
  templateUrl: './devis-finalise.component.html',
  styleUrls: ['./devis-finalise.component.scss']
})
export class DevisFinaliseComponent implements OnInit {

  /**
   * Nom du Continent inscrit dans l'url
   */
  continentParams: string;

  /**
   * Id de la destination inscrit dans l'url
   */
  idDestinationParams: number;

  /**
   * Nom du Pays inscrit dans l'url
   */
  paysDestinationsParams: string;

  /**
   * Nom du client inscrit dans l'url
   */
  nomClientParams: string;

  devisFinaliseSubscription: Subscription;
  devisFinalise: DevisFinalise = new DevisFinalise(0, "", "", "", "", null, null, "", 0, 0, 0, 0, 0, 0, 0, 0, 0);

  arrayDetailsVoyage: any[] = [];
  arrayAssurancePremium: any[] = [];
  nomHotel: string;
  nomChambre: string;
  tarifVolAffaire: number = 0;


  constructor(private activatedRoute: ActivatedRoute, private devisService: DevisService, private continentsDestinationsService: ContinentsDestinationsService, private datePipe: DatePipe) { }

  ngOnInit(): void {

    this.continentsDestinationsService.chargementSpinner();

    this.recupererParametresUrl();

    this.recupererDevisEnregistre(this.devisFinalise, this.continentParams, this.paysDestinationsParams, this.idDestinationParams, this.nomClientParams);

    this.recupererInfosHotel(this.idDestinationParams);

    this.recupererDetailsVoyage(this.idDestinationParams, this.continentParams, this.arrayDetailsVoyage);

    this.tarifVolAffaire = Math.round(this.afficherTarifVolClasseAffaire());
  }

  /**
   * Permet de récupérer les paramètres d'url (nom du continent, id destination, pays de la destination, nom client)
   */
  recupererParametresUrl() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.continentParams = params.get('nom-continent');
      this.idDestinationParams = +params.get('id');
      this.paysDestinationsParams = params.get('pays-destination');
      this.nomClientParams = params.get('nom-client');

    });
  }

  /**
   * Permet de récupérer le devis finalisé, enregistré en fonction des paramètres entrés depuis le serveur
   * @param devisFinalise 
   * @param continentParams 
   * @param paysDestinationsParams 
   * @param idDestinationParams 
   * @param nomClientParams 
   * @returns devisFinalise
   */
  recupererDevisEnregistre(devisFinalise: DevisFinalise, continentParams: string, paysDestinationsParams: string, idDestinationParams: number, nomClientParams: string) {
    return this.devisService.getDevisFinalise(continentParams, paysDestinationsParams, idDestinationParams, nomClientParams)
      .subscribe(
        (response) => {
          devisFinalise.idDevis = response['idDevis'];
          devisFinalise.continentDestination = response['continentDestination'];
          devisFinalise.paysDestination = response['paysDestination'];
          devisFinalise.nomClient = response['nomClient'];
          devisFinalise.emailClient = response['emailClient'];
          devisFinalise.dateDepart = response['dateDepart'];
          devisFinalise.transportInclu = response['transport'];
          devisFinalise.villeDepart = response['villeDepartVol'];
          devisFinalise.nbNuits = response['nbNuits'];
          devisFinalise.nbParticipantsAdulte = response['nbParticipantsAdulte'];
          devisFinalise.nbParticipantsEnfant = response['nbParticipantsEnfant'];
          devisFinalise.nbParticipantsBebe = response['nbParticipantsBebe'];
          devisFinalise.montantTotalBase = response['montantTotalBase'];
          devisFinalise.montantTotalVol = response['montantTotalVol'];
          devisFinalise.montantTotalHebergement = response['montantTotalHebergement'];
          devisFinalise.montantTotalTTC = response['montantTotalTTC'];
          devisFinalise.idDestination = response['idDestination'];

        },
        (error) => {
          console.log(error);
        }
      )
  }

  /**
   * Permet de récupérer les détails du voyage selon l'id de la destination
   * @param idDestination 
   * @param continentParams 
   * @param arrayDetailsVoyage 
   */
  recupererDetailsVoyage(idDestination: number, continentParams: string, arrayDetailsVoyage: any[]) {
    var arrayDestinations = this.continentsDestinationsService.getDestinationsSelonContinent(continentParams);

    for (let destination of arrayDestinations) {
      if (destination.idDestination === idDestination) {
        arrayDetailsVoyage.push(destination);
      }
    }
  }

  /**
   * Permet de récupérer les information de l'hotel
   * @param idDestination 
   */
  recupererInfosHotel(idDestination: number) {
    this.continentsDestinationsService.getInformationsHotel(idDestination)
      .subscribe(
        (response) => {
          this.nomHotel = response['nomHotel'];
          this.nomChambre = response['nomChambre'];
        }
      )
  }

  /**
   * Permet de mettre à jour le texte en fonction du type de participants
   * @param nbParticipantsAdulte 
   * @param nbParticipantsEnfant 
   * @param nbParticipantsBebe 
   * @returns string type et nombre de participants
   */
  modifierTexteParticipants(nbParticipantsAdulte: number, nbParticipantsEnfant: number, nbParticipantsBebe: number) {
    var texteParticipants = "";

    if (nbParticipantsAdulte > 0 && nbParticipantsEnfant === 0 && nbParticipantsBebe === 0) {
      texteParticipants = nbParticipantsAdulte + " Adulte(s)"
    } else if (nbParticipantsAdulte > 0 && nbParticipantsEnfant > 0 && nbParticipantsBebe === 0) {
      texteParticipants = nbParticipantsAdulte + " Adulte(s), " + nbParticipantsEnfant + " Enfant(s)"
    } else if (nbParticipantsAdulte > 0 && nbParticipantsEnfant > 0 && nbParticipantsBebe > 0) {
      texteParticipants = nbParticipantsAdulte + " Adulte(s), " + nbParticipantsEnfant + " Enfant(s) et " + nbParticipantsBebe + " Bebe(s)"
    }
    return texteParticipants;
  }

  /**
   * Permet de mettre à jour le texte en fonction de si le vol est inclu ou non
   * @param transport 
   * @returns string vol inclu ou non
   */
  modifierTexteVol(transport: boolean) {
    var texteVol = "";

    if (transport) {
      texteVol = "Vol inclu";
    } else {
      texteVol = "Vol non inclu";
    }
    return texteVol;
  }

  /**
   * Permet de mettre à jour le texte pour le vol au départ de
   * @param transport 
   * @param villeDepart 
   * @returns string vol au départ de
   */
  modifierVilleDepart(transport: boolean, villeDepart: string) {
    var texteVilleDepart = "";

    if (transport) {
      texteVilleDepart = villeDepart;
    } else {
      texteVilleDepart = "En fonction de votre vol";
    }
    return texteVilleDepart;
  }

  afficherTarifVolClasseAffaire(){
    var tarif = 0;

    if(this.continentParams === 'Afrique' || this.continentParams === 'Asie'){
      tarif = (Math.random() * 5500) + 4700;
    } else if(this.continentParams === 'Amérique'){
      tarif = (Math.random() * 4500) + 4000;
    } else if(this.continentParams === 'Europe'){
      tarif = (Math.random() * 2500) + 2300;
    } else {
      tarif = (Math.random() * 8900) + 7900;
    }

    return tarif;
  }

  /**
   * Permet de mettre à jour les infos de transfert collectif
   * @param arrayDetailsVoyage 
   * @returns 
   */
  modifierTransfertCollectifInclu(arrayDetailsVoyage: any[]) {
    var reponse = 'non inclus';

    if (arrayDetailsVoyage[0].transfertCollectif === true) {
      reponse = 'inclus';
    }
    return reponse;
  }

  /**
   * Permet d'afficher les infos de l'assurance multirisque premium
   * @returns array contenant les infos de l'assurance multirisque premium
   */
  recupererinfosAssuranceMultirisquePremium() {
    return this.continentsDestinationsService.getInformationsAssuranceMultirisquePremium();
  }

  /**
   * Permet d'afficher les infos de l'assurance annulation premium
   * @returns array contenant les infos de l'assurance annulation premium
   */
  recupererinfosAssuranceAnnulationPremium() {
    return this.continentsDestinationsService.getInformationsAssuranceAnnulationPremium();
  }

  /**
   * Permet de calculer le montant de l'option Flex en fonction du nombre de participants
   * @param nbParticipantsAdulte 
   * @param nbParticipantsEnfant 
   * @param prixOptionFex 
   * @returns montant de l'option Flex en fonction du nombre de participants
   */
  calculTarifOptionFlex(nbParticipantsAdulte: number, nbParticipantsEnfant: number, prixOptionFex: number) {
    return (prixOptionFex * nbParticipantsAdulte) + ((prixOptionFex / 2) * nbParticipantsEnfant);
  }

  /**
   * Permet de calculer le montant de l'assurance multirisque premium en fonction du nombre de participants
   * @param nbParticipantsAdulte 
   * @param nbParticipantsEnfant 
   * @param prixAssuranceMultirisquePremium 
   * @returns montant de l'assurance multirisque premium en fonction du nombre de participants
   */
  calculTarifAssuranceMultirisquePremium(nbParticipantsAdulte: number, nbParticipantsEnfant: number, prixAssuranceMultirisquePremium: number) {
    return (prixAssuranceMultirisquePremium * nbParticipantsAdulte) + ((prixAssuranceMultirisquePremium / 2) * nbParticipantsEnfant);
  }

  /**
   * Permet de calculer le montant de l'assurance annulation premium en fonction du nombre de participants
   * @param nbParticipantsAdulte 
   * @param nbParticipantsEnfant 
   * @param prixAssuranceAnnulationPremium 
   * @returns montant de l'assurance annulation premium en fonction du nombre de participants
   */
  calculTarifAssuranceAnnulationPremium(nbParticipantsAdulte: number, nbParticipantsEnfant: number, prixAssuranceAnnulationPremium: number) {
    return (prixAssuranceAnnulationPremium * nbParticipantsAdulte) + ((prixAssuranceAnnulationPremium / 2) * nbParticipantsEnfant);
  }

}
