export class Devis {
    public continentDestination: string;
    public paysDestination: string;
    public idDestination: string;
    public transport: boolean;
    public villeDepartVol: string;
    public nbNuits: string;
    public dateDepart: Date;
    public nbParticipantsAdulte: number;
    public nbParticipantsEnfant: number;
    public nbParticipantsBebe: number;
    public nomClient: string;
    public emailClient: string;

    constructor(continentDestination: string, paysDestination: string, idDestination: string, transport: boolean, villeDepartVol: string, nbNuits: string,
        dateDepart: Date, nbParticipantsAdulte: number, nbParticipantsEnfant: number, nbParticipantsBebe: number, nomClient: string, emailClient: string) {
        this.continentDestination = continentDestination;
        this.paysDestination = paysDestination;
        this.idDestination = idDestination;
        this.transport = transport;
        this.villeDepartVol = villeDepartVol;
        this.nbNuits = nbNuits;
        this.dateDepart = dateDepart;
        this.nbParticipantsAdulte = nbParticipantsAdulte;
        this.nbParticipantsEnfant = nbParticipantsEnfant;
        this.nbParticipantsBebe = nbParticipantsBebe;
        this.nomClient = nomClient;
        this.emailClient = emailClient;
    }
}
 