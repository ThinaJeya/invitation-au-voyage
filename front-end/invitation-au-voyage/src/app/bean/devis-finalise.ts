
export class DevisFinalise {

    public idDevis: number;
    public continentDestination: string;
    public paysDestination: string;
    public nomClient: string;
    public emailClient: string;
    public dateDepart: Date;
    public transportInclu: boolean;
    public villeDepart: string;
    public nbNuits: number;
    public nbParticipantsAdulte: number;
    public nbParticipantsEnfant: number;
    public nbParticipantsBebe: number;
    public montantTotalBase: number;
    public montantTotalVol: number;
    public montantTotalHebergement: number;
    public montantTotalTTC: number;
    public idDestination: number;

    constructor(idDevis: number, continentDestination: string, paysDestination: string, nomClient: string, emailClient: string, dateDepart: Date, transportInclu: boolean,
        villeDepart: string, nbNuits: number, nbParticipantsAdulte: number, nbParticipantsEnfant: number, nbParticipantsBebe: number, montantTotalBase: number, 
        montantTotalVol: number, montantTotalHebergement: number, montantTotalTTC: number, idDestination: number){
        
            this.idDevis = idDevis;
            this.continentDestination = continentDestination;
            this.paysDestination = paysDestination;
            this.nomClient = nomClient;
            this.emailClient = emailClient;
            this.dateDepart = dateDepart;
            this.transportInclu = transportInclu;
            this.villeDepart = villeDepart;
            this.nbNuits = nbNuits;
            this.nbParticipantsAdulte = nbParticipantsAdulte;
            this.nbParticipantsEnfant = nbParticipantsEnfant;
            this.nbParticipantsBebe = nbParticipantsBebe;
            this.montantTotalBase = montantTotalBase;
            this.montantTotalVol = montantTotalVol;
            this.montantTotalHebergement = montantTotalHebergement;
            this.montantTotalTTC = montantTotalTTC;
            this.idDestination = idDestination;
    }

}
