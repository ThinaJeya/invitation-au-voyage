export class Continent {

    public nom: string;

    constructor(nom: string) { 
        this.nom = nom;
    }  
}

export const CONTINENTS: Continent[] = [
    {
        nom: "Afrique"
    },
    {
        nom: "Amérique"
    },
    {
        nom: "Asie"
    },
    {
        nom: "Europe"
    },
    {
        nom: "Océanie"
    },
];