export class Destination {

    public idDestination: number;
    public continentDestination: string;
    public paysDestination: string;
    public villeDestination: string;
    public titreSejour: string;
    public nomHotel: string;
    public urlImage1: string;
    public urlImage2: string;
    public urlImage3: string;
    public urlImage4: string;
    public urlImage5: string;
    public descriptionImage1: string;
    public descriptionImage2: string;
    public descriptionImage3: string;
    public descriptionImage4: string;
    public descriptionImage5: string;
    public pointsPositifs: any[];
    public descriptionHotel: string;
    public descriptionSaveurs: string;
    public descriptionLoisirs: string;
    public modalitesVoyage: any[];
    public rdvA: string;
    public formulePensionComplete: boolean;
    public formulePetitDejeuner: boolean;
    public formuleSansPension: boolean;
    public transfertCollectif: boolean;
    public prixTransfertPrivatif: number;
    public prixOptionFex: number;
    public prixAssuranceMultirisquePremium: number;
    public prixAssuranceAnnulationPremium: number;


    constructor(idDestination: number, continentDestination: string, paysDestination: string, villeDestination: string, titreSejour: string, nomHotel: string,
        urlImage1: string, urlImage2: string, urlImage3: string, urlImage4: string, urlImage5: string, descriptionImage1: string, descriptionImage2: string,
        descriptionImage3: string, descriptionImage4: string, descriptionImage5: string, pointsPositifs: any[], descriptionHotel: string,
        descriptionSaveurs: string, descriptionLoisirs: string, modalitesVoyage: any[], rdvA: string, formulePensionComplete: boolean, 
        formulePetitDejeuner: boolean, formuleSansPension: boolean, transfertCollectif: boolean, prixTransfertPrivatif: number, prixOptionFex: number,
        prixAssuranceMultirisquePremium: number, prixAssuranceAnnulationPremium: number) {

        this.idDestination = idDestination;
        this.continentDestination = continentDestination;
        this.paysDestination = paysDestination;
        this.villeDestination = villeDestination;
        this.titreSejour = titreSejour;
        this.nomHotel = nomHotel;
        this.urlImage1 = urlImage1;
        this.urlImage2 = urlImage2;
        this.urlImage3 = urlImage3;
        this.urlImage4 = urlImage4;
        this.urlImage5 = urlImage5;
        this.descriptionImage1 = descriptionImage1;
        this.descriptionImage2 = descriptionImage2;
        this.descriptionImage3 = descriptionImage3;
        this.descriptionImage4 = descriptionImage4;
        this.descriptionImage5 = descriptionImage5;
        this.pointsPositifs = pointsPositifs;
        this.descriptionHotel = descriptionHotel;
        this.descriptionSaveurs = descriptionSaveurs;
        this.descriptionLoisirs = descriptionLoisirs;
        this.modalitesVoyage = modalitesVoyage;
        this.rdvA = rdvA;
        this.formulePensionComplete = formulePensionComplete;
        this.formulePetitDejeuner = formulePetitDejeuner;
        this.formuleSansPension = formuleSansPension;
        this.transfertCollectif = transfertCollectif;
        this.prixTransfertPrivatif = prixTransfertPrivatif;
        this.prixOptionFex = prixOptionFex;
        this.prixAssuranceMultirisquePremium = prixAssuranceMultirisquePremium;
        this.prixAssuranceAnnulationPremium = prixAssuranceAnnulationPremium;
    }
}

export const DESTINATIONSAFRIQUE: Destination[] = [
    {
        idDestination: 1,
        continentDestination: "Afrique",
        paysDestination: "Tanzanie",
        villeDestination: "Zanzibar",
        titreSejour: "Détente en demi-pension sur l'île aux épices",
        nomHotel: "Hôtel Dhow Inn 4*",
        urlImage1: "../../assets/Destinations/Afrique/Tanzanie_Zanzibar/pexels-hendrik-cornelissen-2862070.jpg",
        urlImage2: "../../assets/Destinations/Afrique/Tanzanie_Zanzibar/photo7jpg.jpg",
        urlImage3: "../../assets/Destinations/Afrique/Tanzanie_Zanzibar/zanzibar_saveurs.jpg",
        urlImage4: "../../assets/Destinations/Afrique/Tanzanie_Zanzibar/photo0jpg.jpg",
        urlImage5: "../../assets/Destinations/Afrique/Tanzanie_Zanzibar/pexels-humphrey-muleba-3361818.jpg",
        descriptionImage1: "Photo de Hendrik Cornelissen provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Zanzibar",
        descriptionImage3: "Photo du restaurant de l'hotel de Zanzibar",
        descriptionImage4: "Photo de plage de Zanzibar",
        descriptionImage5: "Photo de Humphrey Muleba provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Découvrir le village de pêcheurs de la Paje, bordé par l'une des plus belles plages de sable blanc de l'île"
            },
            {
                pointPositif: "S'installer dans une chambre spacieuse à la décoration lumineuse et épurée pour un sentiment d'évasion et de détente"
            },
            {
                pointPositif: "Admirer le coucher de soleil depuis la plage"
            },
            {
                pointPositif: ""
            }
        ],
        descriptionHotel: "C'est sur la côte sud est de Zanzibar que nous vous donnons rendez-vous, dans un établissement plein de charme, le Dhow Inn. Niché à Paje, un village de pêcheurs bordé par l'une des plus belles plages de sable blanc de l'île, ce petit boutique hôtel vous accueille dans un cadre épuré et élégant. C'est une escapade des plus dépaysantes qui vous attend dans ce 4* face à l'océan Indien...",
        descriptionSaveurs: 'Tout au long de votre séjour vous profiterez de la formule demi-pension (hors boissons), idéale pour profiter de vos vacances et découvrir de nouvelles saveurs au petit-déjeuner et dîner ! En option avec supplément optez pour la formule pension complète(hors boissons)! C\'est dans un cadre reposant, face à la mer, que le restaurant principal vous accueille autour d\'une carte composée de produits frais et typiques. Installez-vous les pieds dans le sable au restaurant The Grill, pour déguster viandes ou fruits de mer fraîchement pêchés et cuits au barbecue, dans une ambiance décontractée... Une petite soif ou un petit creux ? Rendez-vous au The Bar & Lounge pour sirotez des cocktails ou grignoter un encas. Vous aurez le choix entre l\'atmosphère intime de l\'intérieure le cadre relaxant du patio extérieur, avec vue sur l\'océan...',
        descriptionLoisirs: "Que ce soit pour parfaire votre bronzage sur la plage ou pour vous prélasser au bord de la piscine, votre séjour sera placé sous le signe de la détente... Sur les plages de rêve de l’île, vous saurez apprécier les joies de la baignade et des loisirs aquatiques en tout genre : plongée, snorkeling ou encore kitesurf vous attendent ! Durant votre séjour, ne manquez pas de faire une halte dans le village de pêcheurs de Paje afin d’apprécier l’hospitalité locale et de goûter à des mets exquis à base de fruits de mer !",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous aurez le choix entre la classe économique (inclus) ou la classe premium économique (en option avec supplément) ou la classe affaires (en option et avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales. - si vous choisissez une arrivée tardive, vous effectuerez l'enregistrement de votre chambre et la durée de votre séjour commencera à partir de cette soirée. - si vous choisissez une nuit en vol, vous ne pourrez récupérer votre chambre à l'hôtel qu'à partir de 15h le jour de votre arrivée. La durée de votre séjour commencera à partir de ce jour. Lors de votre choix de vol retour : - si votre vol est prévu avant 3h00, votre départ de la chambre aura lieu à 12h00, vous ne pourrez pas conserver votre chambre. Selon les conditions de votre hôtel, vous pourrez laisser vos bagages à la réception. - si votre vol est prévu après 3h00, vous pourrez conserver votre chambre jusqu’à votre départ de l’hôtel"
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Vous saurez si le tarif du billet d'avion inclut un bagage lors du choix de la compagnie et des horaires. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaître le tarif exact et acheter votre bagage supplémentaire. Les tarifs étant susceptibles d’évoluer, à titre d’exemple, ce bagage peut s’élever sur un Paris/New-York à 68€ pour Air France ou 100€ pour Air Europa par aller simple, soit 136€ et 200€ pour l’aller-retour. Si vous choisissez une compagnie lowcost, vous pourrez ajouter votre bagage lors du processus de réservation. Pour les vols internes, les compagnies aériennes ne proposent pas toujours la possibilité de réserver un bagage, il sera donc à votre charge et à régler au comptoir de l’aéroport."
            },
            {
                infosTransferts: "Sur certaines compagnies le trajet Province/Paris ou Bruxelles/Paris peut être assuré en train. Cette information vous est mentionnée lorsque vous cliquez sur « Détails » lors de votre sélection. Si vous commencez votre trajet par le TGV, veuillez-vous rendre au plus tôt 24h et jusqu’à 20 minutes avant le départ de votre train au comptoir TGV AIR de votre gare de départ afin de récupérer votre billet TGV. Si vous commencez votre trajet par l’aérien, après avoir récupéré vos bagages, rendez-vous au comptoir TGV AIR pour la remise de votre billet TGV."
            }
        ],
        rdvA: "Véritable joyau exotique de la Tanzanie, Zanzibar est un archipel où les plages de sable blanc et les lagons sont protégés par une magnifique barrière de corail. C’est dans cet environnement exceptionnel que vous prendrez du temps pour vous ressourcer mais également pour partir à la découverte de ses trésors. Stone Town, la capitale classée au patrimoine mondial de l’humanité par l’UNESCO, vous offre un aperçu des nombreuses influences qui font la richesse de l’île aux épices.",
        formulePensionComplete: true,
        formulePetitDejeuner: false,
        formuleSansPension: false,
        transfertCollectif: false,
        prixTransfertPrivatif: 12,
        prixOptionFex: 37,
        prixAssuranceMultirisquePremium: 86,
        prixAssuranceAnnulationPremium: 75
    },
    {
        idDestination: 2,
        continentDestination: "Afrique",
        paysDestination: "Ile Maurice",
        villeDestination: "Flic-en-Flac",
        titreSejour: "Havre de paix face au lagon",
        nomHotel: "Hôtel Hilton Mauritius Resort & Spa 5*",
        urlImage1: "../../assets/Destinations/Afrique/IleMaurice_FlicEnFlac/beach-v18333181.jpg",
        urlImage2: "../../assets/Destinations/Afrique/IleMaurice_FlicEnFlac/guest-room.jpg",
        urlImage3: "../../assets/Destinations/Afrique/IleMaurice_FlicEnFlac/ginger-thai.jpg",
        urlImage4: "../../assets/Destinations/Afrique/IleMaurice_FlicEnFlac/landscaped-pool.jpg",
        urlImage5: "../../assets/Destinations/Afrique/IleMaurice_FlicEnFlac/pexels-michal-marek-3703465.jpg",
        descriptionImage1: "Photo de plage de l'Ile Maurice",
        descriptionImage2: "Photo de l'hotel de Flic-en-Flac",
        descriptionImage3: "Photo du restaurant de l'hotel de Flic-en-Flac",
        descriptionImage4: "Photo de piscine de l'hotel de Flic-en-Flac",
        descriptionImage5: "Photo de Michal Marek provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Poser ses valises sur une île de rêve et dans un établissement de choix somptueux et élégant"
            },
            {
                pointPositif: "S'installer dans une chambre spacieuse et lumineuse où vous aimerez vous reposer"
            },
            {
                pointPositif: "S'accorder une pause bien-être au Spa et se laisser aller à des mains expertes lors d'un massage"
            },
            {
                pointPositif: "Résider à Flic en Flac, une station balnéaire de charme et un site réputé pour la plongée sous-marine"
            }
        ],
        descriptionHotel: "Le confort de cette chaîne hôtelière n’est pas une légende et vous apprécierez aussi bien le standing de son mobilier que les soins tout particulier apportés à l'esthétique des lieux. Le cadre enchanteur et raffiné du Hilton Mauritius Resort & Spa 5* ainsi que le personnel avenant et affable créent une atmosphère sereine qui permet à chacun de se retrouver en couple, bercé par le bruit de l'eau qui s'entend partout dans l'établissement. Bienvenue dans le lieu de séjour absolu pour vivre un rêve mauricien !",
        descriptionSaveurs: "Vous séjournerez en formule pension complète (hors boissons) pour des vacances en toute sérénité. Profitez de votre petit-déjeuner au restaurant principal La Pomme d’Amour où vous découvrirez une cuisine locale et internationale sous forme de buffets. Pour le dîner, vous aurez la possibilité de choisir vos repas au restaurant La Pomme d'Amour ou aux restaurants à la carte Ginger Thai, établissement gastronomique orchestré par un chef thaïlandais, ou encore Les Coquillages, situé sur la plage.",
        descriptionLoisirs: "L'immense piscine lagon paysagée sera au centre de vos instants de détente entre cascades artificielles, bains à remous et végétation luxuriante. Vous pourrez aussi profiter d'activités sportives ou de remise en forme pendant que les enfants s'amuseront au mini-club ! Depuis votre hôtel, vous accéderez directement à la belle plage de Flic en Flac, ombragée et parfaitement équipée ! Sur place, vous découvrirez une sélection de sports nautiques motorisés ou non pour égayer vos journées. Profitez-en pour vous essayer au ski nautique ou encore partez en excursion pour voir les dauphins de plus près. Pour découvrir le monde marin qui vous entoure, ne manquez pas de faire appel à l'hôtel qui, avec supplément, organisera vos sorties plongée sous-marine, vos balades en mer en kayak ou en bateau...",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible. Sur certaines compagnies le trajet Province/Paris ou Bruxelles/Paris peut être assuré en train. Cette information vous est mentionnée lorsque vous cliquez sur « Détails » lors de votre sélection.  Si vous commencez votre trajet par le TGV, veuillez-vous rendre au plus tôt 24h et jusqu’à 20 minutes avant le départ de votre train au comptoir TGV AIR de votre gare de départ afin de récupérer votre billet TGV. Si vous commencez votre trajet par l’aérien, après avoir récupéré vos bagages, rendez-vous au comptoir TGV AIR pour la remise de votre billet TGV."
            },
            {
                infosTransferts: "Les transferts collectifs aller-retour entre l’aéroport et l’hôtel sont compris dans nos offres avec vol (bagages de golf non inclus, disponibles uniquement avec les transferts privés)."
            }
        ],
        rdvA: "Véritable petit bout de paradis, l’île Maurice est la destination rêvée ! L’île présente des panoramas paradisiaques mais aussi contrastés. Sur les côtes vous trouverez des plages de sable blanc bordées par une mer translucide, l’idéal pour déconnecter et se laisser bercer par le clapotis de l’eau à l’ombre d’un palmier. En regagnant l’intérieur de l’île, des paysages plus sauvages et luxuriants s’offrent à vous. Laissez-vous porter par le rythme créole, découvrez la gentillesse des mauriciens qui vous inviteront certainement à partager un cari... Ici, vos rêves deviennent réalité.",
        formulePensionComplete: true,
        formulePetitDejeuner: false,
        formuleSansPension: false,
        transfertCollectif: true,
        prixTransfertPrivatif: 11,
        prixOptionFex: 42,
        prixAssuranceMultirisquePremium: 90,
        prixAssuranceAnnulationPremium: 79
    },
    {
        idDestination: 3,
        continentDestination: "Afrique",
        paysDestination: "Maroc",
        villeDestination: "Marrakech",
        titreSejour: "Adresse de légende dans l'écrin chic de l'Hivernage",
        nomHotel: "Es Saadi Marrakech Resort Hotel 5*",
        urlImage1: "../../assets/Destinations/Afrique/Maroc_Marrakech/pexels-tomáš-malík-1703312.jpg",
        urlImage2: "../../assets/Destinations/Afrique/Maroc_Marrakech/es-saadi-marrakech-resort.jpg",
        urlImage3: "../../assets/Destinations/Afrique/Maroc_Marrakech/saveurs_marrakech.jpg",
        urlImage4: "../../assets/Destinations/Afrique/Maroc_Marrakech/pexels-meliani-idriss-2945595.jpg",
        urlImage5: "../../assets/Destinations/Afrique/Maroc_Marrakech/pexels-gabriel-garcia-2404046.jpg",

        descriptionImage1: "Photo de Tomas Malik provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Marrakech",
        descriptionImage3: "Photo du restaurant de l'hotel de Marrakech",
        descriptionImage4: "Photo de Meliani Idriss provenant de Pexels",
        descriptionImage5: "Photo de Gabriel Garcia provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Entrer dans la légende d'une adresse réputée de Marrakech, au cœur du quartier chic de l'Hivernage, face à l'Atlas et proche de la médina"
            },
            {
                pointPositif: "Remonter le temps dans un décor élégant aux lignes années 50, entouré d'un parc luxuriant de 8 hectares et chouchouté par un personnel dévoué"
            },
            {
                pointPositif: "Se ressourcer au bord de la piscine, s'initier à la culture locale au Spa Oriental puis au restaurant gastronomique de l'hôtel"
            },
            {
                pointPositif: "Percer à jour les nombreux secrets de Marrakech, pour des souvenirs indélébiles"
            }
        ],
        descriptionHotel: "L'Hôtel Es Saadi 5* vous accueille dans la quiétude de son grand parc fleuri de huit hectares avec pour toile de fond les cimes de l'Atlas. Laissez-vous enivrer par les senteurs et les couleurs des orangers, des bougainvilliers, des roses et des palmiers... Dans cette oasis au cœur du quartier chic de l'Hivernage, vous serez transporté dans le temps, à travers l'histoire de ce resort légendaire devenu une véritable référence pour les voyageurs comme pour les marocains. Marchez dans les pas des Rolling Stones ou de Joséphine Baker, imprégnez-vous de l'atmosphère unique de ce refuge au luxe discret où se mêlent avec raffinement style années 50, artisanat marocain et tons chauds...",
        descriptionSaveurs: "Pour vous laisser libre dans votre découverte de la gastronomie marocaine, nous avons sélectionné pour vous un séjour en formule petit-déjeuner. Commencez la journée tout en douceur au Jardin d'Hiver en dégustant un savoureux petit-déjeuner dans le parc. Après une matinée passée à vous chouchouter au Spa, quoi de plus logique qu'un déjeuner léger avant de partir visiter la ville ? Le restaurant Autour de la Piscine, vous propose des en-cas et des repas légers. À l'heure du dîner, place à la cuisine française au restaurant gastronomique Le Saadi servie à l'intérieur ou en terrasse. Profitez du dîner-spectacle les mercredis et vendredis soir et goûtez une cuisine généreuse au rythme des danseurs et des musiciens. En soirée, profitez de l'atmosphère rétro chic du Piano Bar pour partager vos découvertes autour d'un verre.Vous savourerez une huile d'olive vierge produite par l'hôtel, des fruits et des légumes provenant des potagers du resort et traités en culture biologique dans sa ferme située dans la vallée de l'Ourika !",
        descriptionLoisirs: "Après avoir parcouru les ruelles animées de la Médina, vous apprécierez vous délasser dans la superbe piscine extérieure ou vous faire chouchouter à l'Oriental Spa de l'hôtel empreint de traditions. Vous en voulez plus ? Offrez-vous une parenthèse bien-être ultime au Spa Dior de la partie Palace, une expérience unique (sur réservation en supplément) ! Le temps d'une nuit inoubliable, laissez-vous embarquer dans le cadre mythique de la discothèque Le Theatro, l'un des meilleurs clubs du Maroc.",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts aller-retour entre l’aéroport et l’hôtel sont compris dans nos offres avec vol. "
            }
        ],
        rdvA: "'Il y a peu de villes, au monde, qui vous ensorcellent avec la même force et la même magie que Marrakech'…  Comme Pierre Bergé, nombre d’artistes sont tombés amoureux de cette ville captivante. Médina animée, artisanat typique et riads à l’architecture exceptionnelle, Marrakech ne laisse pas indifférent. La « perle du sud » est une ville colorée dans laquelle Yves Saint-Laurent puisa son inspiration pour créer ses plus belles collections. Gage de dépaysement, la « ville rouge », promet des instants uniques à chaque coin de rue, de la place Jemma El Fna, aux Jardins Majorelle, en passant par la mosquée Koutoubia. Vous serez charmé à votre tour !",
        formulePensionComplete: false,
        formulePetitDejeuner: true,
        formuleSansPension: false,
        transfertCollectif: true,
        prixTransfertPrivatif: 8,
        prixOptionFex: 34,
        prixAssuranceMultirisquePremium: 77,
        prixAssuranceAnnulationPremium: 70
    },
    {
        idDestination: 4,
        continentDestination: "Afrique",
        paysDestination: "Egypte",
        villeDestination: "Sharm El Sheikh",
        titreSejour: "En tout inclus face à la mer Rouge",
        nomHotel: "Coral Sea Holiday Resort & Aqua Park 5*",
        urlImage1: "../../assets/Destinations/Afrique/Egypte_SharmElSheikh/pexels-murat-şahin-3522880.jpg",
        urlImage2: "../../assets/Destinations/Afrique/Egypte_SharmElSheikh/pexels-maria-orlova-4946767.jpg",
        urlImage3: "../../assets/Destinations/Afrique/Egypte_SharmElSheikh/sharm_restaurant.jpg",
        urlImage4: "../../assets/Destinations/Afrique/Egypte_SharmElSheikh/sharm_loisirs.jpg",
        urlImage5: "../../assets/Destinations/Afrique/Egypte_SharmElSheikh/pexels-julia-volk-5273214.jpg",
        descriptionImage1: "Photo de Murat Şahin provenant de Pexels",
        descriptionImage2: "Photo de Maria Orlova provenant de Pexels",
        descriptionImage3: "Photo du restaurant de l'hotel de Sharm El Sheikh",
        descriptionImage4: "Photo de de Sharm El Sheikh",
        descriptionImage5: "Photo de Julia Volk provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Poser ses valises dans un resort offrant de nombreux divertissements"
            },
            {
                pointPositif: "Découvrir de nouvelles saveurs grâce à la formule tout inclus"
            },
            {
                pointPositif: "S'extasier face à la beauté du paysage et le panorama unique de la Mer Rouge"
            },
            {
                pointPositif: "S'initier à la plongée au parc national Ras Muhammad très prisé par les plongeurs"
            }
        ],
        descriptionHotel: "L'hôtel Coral Sea Holiday Resort & Aqua Park 5 *, situé dans le quartier branché de Nabq Bay, est majestueusement placé en face de l'île de Tiran. L'hôtel est unique par son élégance architecturale, embrassant la splendeur de la nature du Sinaï dans un authentique complexe. De conception contemporaine, le complexe dispose de 484 chambres luxueuses offrant une diversité de types de chambres et une vue différente qui varie entre vue panoramique, vue sur la piscine et vue sur le jardin intérieur.",
        descriptionSaveurs: "Pour un voyage en toute tranquillité, nous avons négocié pour vous la formule tout inclus que vous pourrez découvrir dans plus de 10 lieux différents. Au Piazza, vous pourrez profiter d'une expérience culinaire avec vue sur le lac. Vous désirez un petit-déjeuner en famille ? Rendez-vous au Dine Around pour un buffet au petit-déjeuner, déjeuner ou dîner. Si vous désirez déguster des saveurs typiques de la Méditerranée alors Mediterraneo est fait pour vous ! Cet endroit sera idéal pour un dîner romantique ou un moment en famille. Etes-vous amateurs de viande ? Filez la plage au Village Diner et savourez une grillade. Si vous désirez vous désaltérer durant la journée ou le soir, plusieurs bars s'offriront à vous. Profitez-en par exemple pour vous retrouver au coucher du soleil face à la mer au Beach Bar...",
        descriptionLoisirs: "Profitez des nombreuses activités au Coral Sea Holiday Resort & Aqua Park 5* ! Pour un plaisir en famille, direction le parc aquatique. Cette expérience inclus une piscine à vagues, plusieurs toboggans et une rivière artificielle. Offrez-vous également un moment de détente en profitant du Spa de l'hôtel. Prenez un bain délicieux, accordez-vous une séance de massage ou purifiez votre corps dans un bain vapeur... vous en ressortirez comblé ! L'hôtel propose également un vaste programme de divertissements comme des spectacles amusants pour les enfants.",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible.Sur certaines compagnies le trajet Province/Paris ou Bruxelles/Paris peut être assuré en train. Cette information vous est mentionnée lorsque vous cliquez sur « Détails » lors de votre sélection. "
            },
            {
                infosTransferts: "Les transferts collectifs aller-retour entre l’aéroport et l’hôtel sont compris dans nos offres avec vol. "
            }
        ],
        rdvA: "Nous vous emmenons à Sharm El Sheikh sur les bords de la mer Rouge, dans l'une des régions phares du tourisme en Égypte. Avec ses oasis, ses plages et ses fonds sous-marins parmi les plus beaux du monde, vous effectuerez un véritable voyage à travers le temps, où vous profiterez d'eaux chaudes toute l'année. Partez à la découverte du désert de Sinaï, et allez explorer les trésors de l'Égypte... N'hésitez plus !",
        formulePensionComplete: true,
        formulePetitDejeuner: false,
        formuleSansPension: false,
        transfertCollectif: true,
        prixTransfertPrivatif: 7,
        prixOptionFex: 35,
        prixAssuranceMultirisquePremium: 72,
        prixAssuranceAnnulationPremium: 68
    },
    {
        idDestination: 5,
        continentDestination: "Afrique",
        paysDestination: "Kenya",
        villeDestination: "Diani",
        titreSejour: "En tout inclus face à la mer Rouge",
        nomHotel: "Leopard Beach Resort 4*",
        urlImage1: "../../assets/Destinations/Afrique/Kenya_Diani/pexels-vishva-patel-4595868.jpg",
        urlImage2: "../../assets/Destinations/Afrique/Kenya_Diani/residence-villa.jpg",
        urlImage3: "../../assets/Destinations/Afrique/Kenya_Diani/diani_restaurant.jpg",
        urlImage4: "../../assets/Destinations/Afrique/Kenya_Diani/leopard-beach-resort.jpg",
        urlImage5: "../../assets/Destinations/Afrique/Kenya_Diani/pexels-git-stephen-gitau-1687193.jpg",
        descriptionImage1: "Photo de Vishva Patel provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Diani",
        descriptionImage3: "Photo du restaurant de l'hotel de Diani",
        descriptionImage4: "Photo de piscine de l'hotel de Diani",
        descriptionImage5: "Photo de Git Stephen Gitau provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Se prélasser dans l'atmosphère tropicale d'un 4* situé les pieds dans l'eau"
            },
            {
                pointPositif: "S'adonner aux activités nautiques sur la plage de Diani"
            },
            {
                pointPositif: "Découvrir toute la magie du Kenya grâce aux safaris"
            },
            {
                pointPositif: ""
            }
        ],
        descriptionHotel: "Le Leopard Beach Resort vous promet un séjour inoubliable... Entrez dans ce petit paradis 4* et découvrez un site préservé où l'architecture des bâtiments s'intègre parfaitement à la végétation luxuriante. Ici, vous profiterez du meilleur des prestations, alliant confort, service irréprochable et proposant un grand nombre d'activités et d'excursions. Des moments privilégiés à partager avec vos proches !",
        descriptionSaveurs: "Pour que vos vacances soient des plus sereines, vous séjournerez en formule demi pension (hors boissons). En option et avec supplément, laissez-vous tenter par la pension complète (hors boissons) ou la formule tout inclus. Le restaurant Horizon vous accueille pour le petit-déjeuner servi sous forme de buffet, le déjeuner et le dîner avec pour toile de fond, l'océan Indien... L'hôtel dispose aussi de 4 restaurants à la carte (avec supplément) pour varier les plaisirs : le Pool Bistrot, snack en bord de piscine pour les en-cas légers, le Coco Beach, le Pasta 'N' Pizza Tornati, pizzeria située en bord de plage, et le Chui Grill, restaurant membre de la Chaîne des Rôtisseurs, proposant des spécialités de fruits de mer pour un dîner raffiné dans un cadre magnifique.",
        descriptionLoisirs: "Véritable décor de carte postale, la plage de sable blanc de Diani au sud de Mombasa borde votre hôtel et vous invite à l'évasion... Installez-vous et oubliez le temps qui passe pendant que les amateurs de sports nautiques s'adonnent à leur activité favorite : plongée, planche à voile, kayak, pêche... Ne manquez pas de terminer la journée par une touche de douceur à l'Uzuri Spa, un endroit unique situé au cœur d'une végétation tropicale.",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous aurez le choix entre la classe économique(inclus) ou la classe affaires (en option et avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Vous saurez si le tarif du billet d'avion inclut un bagage lors du choix de la compagnie et des horaires.  Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaître le tarif exact et acheter votre bagage supplémentaire. Les tarifs étant susceptibles d’évoluer, à titre d’exemple, ce bagage peut s’élever sur un Paris/New-York à 68€ pour Air France ou 100€ pour Air Europa par aller simple, soit 136€ et 200€ pour l’aller-retour.  Si vous choisissez une compagnie lowcost, vous pourrez ajouter votre bagage lors du processus de réservation. "
            },
            {
                infosTransferts: "Les transferts aller-retour entre l'aéroport et l'hôtel sont compris dans nos offres avec vol uniquement."
            }
        ],
        rdvA: "Envie d'un décor tropical idyllique pour vos prochaines vacances ? Embarquement immédiat pour le Kenya. Pays d'Afrique de l'Est, baigné par les eaux turquoise de l'océan Indien, il compte près de 500 kilomètres de côtes aux plages toutes plus belles les unes que les autres. Un cadre de rêve pour se prélasser au soleil, loin de votre quotidien. Vous poserez vos valises sur la côte sud du Kenya, le long de la plage de sable blanc de Diani. Et pour compléter votre séjour, nous vous proposons de découvrir la faune sauvage et les paysages grandioses de ses célèbres parcs nationaux. Prêt à vivre une expérience unique ?",
        formulePensionComplete: true,
        formulePetitDejeuner: false,
        formuleSansPension: false,
        transfertCollectif: true,
        prixTransfertPrivatif: 9,
        prixOptionFex: 41,
        prixAssuranceMultirisquePremium: 79,
        prixAssuranceAnnulationPremium: 75
    }
]

export const DESTINATIONSAMERIQUE: Destination[] = [
    {
        idDestination: 6,
        continentDestination: "Amérique",
        paysDestination: "République Dominicaine",
        villeDestination: "Punta Cana",
        titreSejour: "Echappée inoubliable en bord de mer en tout inclus",
        nomHotel: "Dreams Royal Beach 5*",
        urlImage1: "../../assets/Destinations/Amerique/RepDominicaine_PuntaCana/now-larimar-punta-cana.jpg",
        urlImage2: "../../assets/Destinations/Amerique/RepDominicaine_PuntaCana/now-larimar-punta-cana (1).jpg",
        urlImage3: "../../assets/Destinations/Amerique/RepDominicaine_PuntaCana/puntaCana_restaurant.jpg",
        urlImage4: "../../assets/Destinations/Amerique/RepDominicaine_PuntaCana/now-larimar-punta-cana (2).jpg",
        urlImage5: "../../assets/Destinations/Amerique/RepDominicaine_PuntaCana/pexels-michal-marek-3703511.jpg",
        descriptionImage1: "Photo du resort de Punta Cana",
        descriptionImage2: "Photo de l'hotel dde Punta Cana",
        descriptionImage3: "Photo du restaurant de l'hotel de Punta Canac",
        descriptionImage4: "Photo de piscine de l'hotel de Punta Cana",
        descriptionImage5: "Photo de Michal Marek provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Découvrir un établissement luxueux, totalement rénové, situé à deux pas d'une des plus belles plages de Punta Cana"
            },
            {
                pointPositif: "Bénéficier d'un surclassement dans une chambre confortable et parfaitement équipée"
            },
            {
                pointPositif: "Vivre un séjour sans contrainte grâce à une formule tout inclus, donnant accès à une multitude de restaurants et bars"
            },
            {
                pointPositif: "Découvrir les merveilleux paysages de la région, entre plages de sable blanc et trésors naturels époustouflants"
            }
        ],
        descriptionHotel: "Face à l'océan se trouve le lieu de vos prochaines vacances... Le Dreams Royal Beach est un établissement 5* qui vous accueille dans un cadre unique ! Laissez-vous emporter par l'ambiance chaleureuse des lieux : plage à deux pas, cocotiers, jardins luxuriants et piscines rafraîchissantes, tous les ingrédients sont réunis pour quelques jours agréables sous le soleil. Avec ses services luxueux, sa large palette de restaurants et bars ainsi que sa multitude d'activités, cet hôtel sera un véritable petit havre de paix pour des vacances en famille ou entre amis.",
        descriptionSaveurs: "C'est un séjour gourmand qui vous attend : nous avons sélectionné pour vous la formule tout inclus. Le Dreams Royal Beach vous offrira tout au long de votre séjour un large panel gastronomique. En tout, ce n'est pas moins de 7 restaurants et 10 bars qui vous attendent ! Selon vos envies, dégustez une délicieuse cuisine asiatique au restaurant Fusion, un succulent poisson grillé à l'Oceana ou encore une pizza croustillante au Portofino. Vous apprécierez sinon les délices du Seaside Grill, situé en extérieur, de copieux burgers au Tides Pool Side Grill ou bien une sélection de mets internationaux au World Café.",
        descriptionLoisirs: "Faites place à l'évasion ! L'hôtel vous propose pléthore d'activités, qui satisferont les envies de toute la famille ! Délassez-vous dans l'une des 3 piscines extérieures afin de vivre de doux moments sous le soleil caribéen... Pendant que les plus jeunes sont au mini-club et s'amusent avec leurs nouveaux copains, accordez-vous une parenthèse de bien-être inoubliable dans le Secrets Spa by Pevonia. Massages, soins face à l'océan, circuit d'hydrothérapie ou bien hammam, vous y trouverez forcément votre bonheur ! De leur côté, les plus sportifs seront ravis : cours de danse, aérobic, courts de tennis, sports nautiques, basketball et encore plein d'autres activités vous seront proposées. Une chose est sûre, vous ne voudrez plus partir de ce complexe !",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: " Les transferts collectifs aller-retour entre l’aéroport et l’hôtel sont compris dans nos offres avec vol.  En option et avec supplément, vous pourrez également réserver des transferts privatifs aller-retour entre l'aéroport et l'hôtel, uniquement proposés sur les offres avec vol."
            }
        ],
        rdvA: "Besoin de vous déconnecter sous les tropiques ? Envolez-vous pour Punta Cana, la destination incontournable de la République Dominicaine. Réputée pour ses magnifiques plages, Punta Cana vous invite à la détente et au repos dans un cadre enchanteur. Que vous soyez plutôt noctambule en quête d’animations, amateur de repos et farniente, avide de découvertes culturelles et nature ou passionné de sports aquatiques ; vous allez tomber sous le charme de cette destination aux multiples atouts.  Le lieu à ne pas manquer ? Playa Bavaro, une interminable plage de sable blanc à la beauté unique, bordée par une eau turquoise. Véritable paradis terrestre !",
        formulePensionComplete: true,
        formulePetitDejeuner: false,
        formuleSansPension: false,
        transfertCollectif: true,
        prixTransfertPrivatif: 15,
        prixOptionFex: 42,
        prixAssuranceMultirisquePremium: 90,
        prixAssuranceAnnulationPremium: 88
    },
    {
        idDestination: 7,
        continentDestination: "Amérique",
        paysDestination: "Mexique",
        villeDestination: "Puerto Morelos",
        titreSejour: "Playa et slpendeurs Mayas, avec surclassement vue océan",
        nomHotel: "Hôtel Now Jade 5*",
        urlImage1: "../../assets/Destinations/Amerique/Mexique_PuertoMorelos/mix-bar.jpg",
        urlImage2: "../../assets/Destinations/Amerique/Mexique_PuertoMorelos/preferred-club-suite.jpg",
        urlImage3: "../../assets/Destinations/Amerique/Mexique_PuertoMorelos/pexels-hana-brannigan-3642718.jpg",
        urlImage4: "../../assets/Destinations/Amerique/Mexique_PuertoMorelos/pexels-ivon-gorgonio-3720115.jpg",
        urlImage5: "../../assets/Destinations/Amerique/Mexique_PuertoMorelos/pexels-efrain-alonso-3314864.jpg",
        descriptionImage1: "Photo de bar de plage de Puerto Morelos",
        descriptionImage2: "Photo de l'hotel de Puerto Morelos",
        descriptionImage3: "Photo de Hana Brannigan provenant de Pexels",
        descriptionImage4: "Photo de Ivon Gorgonio provenant de Pexels",
        descriptionImage5: "Photo de Efrain Alonso provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Se détendre en couple ou en famille sur la belle plage qui borde le Now Jade"
            },
            {
                pointPositif: "S'amuser et se dépenser grâce aux nombreuses installations à disposition"
            },
            {
                pointPositif: "Oublier les contraintes et profiter de la formule tout inclus"
            },
            {
                pointPositif: "Approfondir la découverte de la région en réservant les excursions en option"
            }
        ],
        descriptionHotel: "L'hôtel Now Jade Riviera Cancun 5* est situé à Puerto Morelos, entre Cancun et Playa del Carmen, le long de la célèbre Riviera Maya, juste à côté de la marina. Vous allez craquer d'emblée pour la plage de sable blanc qui s'étend sur plus de 2 kilomètres d'ouest en est, offrant un panorama exceptionnel et de sublimes couchers de soleil.  Parfait pour les couples comme pour les familles et groupes d'amis, l'hôtel vous proposera de nombreuses infrastructures qui rendront votre séjour exquis.",
        descriptionSaveurs: "Pour un séjour sous le signe de la gourmandise et de la tranquillité, vous séjournerez en formule tout inclus. Vous aurez le choix entre le restaurant de buffet international, avec vue imprenable sur la mer et 6 restaurants pour vos dîners à la carte : mexicain, italien, asiatique, français, méditerranéen et gourmet.  Un snack bar comblera les petits creux et plusieurs bars seront à votre disposition pour partager des moments de convivialité autour d'un verre.",
        descriptionLoisirs: "Profitez des nombreux aménagements de l'hôtel pour faire le plein d'activités. Chacun y trouvera une source de plaisir intarissable, qui comblera toutes les envies, à chaque instant.Des piscines à la salle de sports, du terrain de beach-volley au court de tennis, ou encore du Spa aux divers sports nautiques proposés, vous trouverez votre bonheur.  Un club pour les enfants permettra à vos chérubins de se divertir à la hauteur de leurs envies. Entièrement supervisé par des animateurs, avec pour programme arts et artisanat, jeux, aventure hors du camp, grand écran et films sur la plage, vos enfants s'y régaleront ! Pour les personnes qui recherchent un sanctuaire pour les sens, le Now Spa offre une atmosphère sublime de pur plaisir. Alliant hydrothérapie et soins, vous allez vivre une expérience rafraîchissante, tonique et paisible.",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts collectifs aller-retour entre l’aéroport et l’hôtel sont compris dans nos offres avec vol.  En option et avec supplément, vous pourrez également réserver des transferts privatifs aller-retour entre l'aéroport et l'hôtel, uniquement proposés sur les offres avec vol."
            }
        ],
        rdvA: "Vous recherchez une destination mêlant culture et farniente au bord de la mer des Caraïbes ?  C'est possible ! Sur la Riviera Maya, un fabuleux ruban de sable bordé d'eaux turquoise, et de végétation luxuriante n'attend que vous. Cette région vous offre la chance exceptionnelle de visiter des sites précolombiens à couper le souffle, tout en profitant d'un environnement paradisiaque ! Posez vos valises à l'hôtel Secrets Akumal Riviera Maya, entre Tulum et Playa del Carmen et à Akumal, qui abrite l'une des plus grandes populations de tortues marines vertes. Laissez-vous tenter par les installations et la tranquillité de cet établissement, lové au bord d'une divine plage. Vous serez idéalement situé pour profiter des animations tout en vous laissant aller à la rêverie dans ce décor sublime... ",
        formulePensionComplete: true,
        formulePetitDejeuner: false,
        formuleSansPension: false,
        transfertCollectif: true,
        prixTransfertPrivatif: 14,
        prixOptionFex: 37,
        prixAssuranceMultirisquePremium: 86,
        prixAssuranceAnnulationPremium: 75
    },
    {
        idDestination: 8,
        continentDestination: "Amérique",
        paysDestination: "Etats-Unis",
        villeDestination: "New York",
        titreSejour: "Chic new-yorkais au coeur de la ville",
        nomHotel: "Hôtel Manhattan Club 4*",
        urlImage1: "../../assets/Destinations/Amerique/USA_NewYork/pexels-carlos-oliva-3586966.jpg",
        urlImage2: "../../assets/Destinations/Amerique/USA_NewYork/the-manhattan-club.jpg",
        urlImage3: "../../assets/Destinations/Amerique/USA_NewYork/lounge.jpg",
        urlImage4: "../../assets/Destinations/Amerique/USA_NewYork/pexels-luis-dalvan-1770775.jpg",
        urlImage5: "../../assets/Destinations/Amerique/USA_NewYork/pexels-pixabay-290386.jpg",
        descriptionImage1: "Photo de Carlos Oliva provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de New York",
        descriptionImage3: "Photo du restaurant de l'hotel de New York",
        descriptionImage4: "Photo de Luis Dalvan provenant de Pexels",
        descriptionImage5: "Photo de Pixabay provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Pouvoir s'offrir un city-break en famille en posant ses valises dans une suite spacieuse"
            },
            {
                pointPositif: "Bénéficier de l'emplacement exceptionnel à seulement 3 minutes de Central Park"
            },
            {
                pointPositif: "Découvrir le quartier de Lower Manhattan le temps d'une visite guidée complète et authentique"
            },
            {
                pointPositif: "Profiter du luxe d'un véritable 4* au cœur d'un quartier dynamique et à proximité du célèbre Carnegie Hall"
            }
        ],
        descriptionHotel: "C'est au cœur de Manhattan que vous attend votre hôtel, le Manhattan Club 4*. Cet hôtel vous offrira une situation privilégiée dans les rues de New York, afin de pouvoir profiter pleinement de la ville. À seulement 3 rues de Central Park, partez découvrir le carré de verdure le plus célèbre de la côte Est.  Dans un style élégant et disposant d'installations modernes, votre hôtel vous permettra de vivre l'immersion dans la vie américaine à 100%.",
        descriptionSaveurs: "Lors de votre séjour, sans pension, profitez de votre liberté pour partir goûter aux spécialités locales. Au retour de belles journées, rejoignez le bar de l'établissement pour un moment de détente et de convivialité. Vous appréciez le décor feutré et l'ambiance chaleureuse des lieux. Il ne vous reste plus qu'à trinquer aux vacances !",
        descriptionLoisirs: "Si vous désirez entretenir votre corps, direction la salle de remise en forme. Celle-ci dispose de machines et d'équipements hauts de gamme qui feront votre bonheur. Le reste du temps, partez à la découverte de l'effervescence de cette ville qui ne dort jamais... Vous risquez d'adorer !",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous aurez le choix entre la classe économique (inclus) ou la classe affaires (en option et avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Vous saurez si le tarif du billet d'avion inclut un bagage lors du choix de la compagnie et des horaires.  Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaître le tarif exact et acheter votre bagage supplémentaire. Les tarifs étant susceptibles d’évoluer, à titre d’exemple, ce bagage peut s’élever sur un Paris/New-York à 68€ pour Air France ou 100€ pour Air Europa par aller simple, soit 136€ et 200€ pour l’aller-retour.  Si vous choisissez une compagnie lowcost, vous pourrez ajouter votre bagage lors du processus de réservation. Pour les vols internes au départ des Etats-Unis, les compagnies aériennes n'incluent pas de bagages. Ils seront à votre charge et à régler à l'aéroport le jour de votre vol. "
            },
            {
                infosTransferts: "Les transferts aller-retour entre l’aéroport et l’hôtel ne sont pas compris dans nos offres mais sont disponibles en option avec supplément pour les offres avec vol."
            }
        ],
        rdvA: "Des grandes avenues de Manhattan aux cafés animés de Brooklyn, laissez-vous entraîner par la frénésie de New York ! 'La ville qui ne dort jamais' porte bien son nom : entre musées à la renommée internationale comme le Metropolitan Museum ou le MoMa, buildings iconiques comme le Rockefeller Center, bars et restaurants où l'on sert une cuisine venue du monde entier et shows immanquables sur Broadway, l'ennui n'a pas sa place. New York séduit par la diversité de ses quartiers, chacun offrant une atmosphère unique, mais tous mettant en avant le melting pot qui fait leur richesse. Pour un week-end romantique ou une aventure en famille ou entre amis, la Grosse Pomme offre tout un panel d'activités. Flânez dans Central Park, laissez-vous éblouir par les lumières de Times Square et ne manquez pas de goûter aux fameux hot-dogs… mais surtout, préparez-vous à tomber amoureux d'une ville emblématique qui n'a pas son pareil !",
        formulePensionComplete: false,
        formulePetitDejeuner: false,
        formuleSansPension: true,
        transfertCollectif: false,
        prixTransfertPrivatif: 35,
        prixOptionFex: 44,
        prixAssuranceMultirisquePremium: 88,
        prixAssuranceAnnulationPremium: 82
    },
    {
        idDestination: 9,
        continentDestination: "Amérique",
        paysDestination: "Cuba",
        villeDestination: "La Havane",
        titreSejour: "Echappée authentique et plaisirs balnéaires",
        nomHotel: "Hôtel Nacional 5*",
        urlImage1: "../../assets/Destinations/Amerique/Cuba_LaHavane/pexels-matthias-oben-3687922.jpg",
        urlImage2: "../../assets/Destinations/Amerique/Cuba_LaHavane/el-hotel-nacional-de.jpg",
        urlImage3: "../../assets/Destinations/Amerique/Cuba_LaHavane/laHavane_restaurant.jpg",
        urlImage4: "../../assets/Destinations/Amerique/Cuba_LaHavane/hotel-nacional-de-cuba.jpg",
        urlImage5: "../../assets/Destinations/Amerique/Cuba_LaHavane/pexels-ivan-bertolazzi-6485227.jpg",
        descriptionImage1: "Photo de Matthias Oben provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Flic-en-Flac",
        descriptionImage3: "Photo du restaurant de l'hotel de La Havane",
        descriptionImage4: "Photo de piscine de l'hotel de La Havane",
        descriptionImage5: "Photo de Ivan Bertolazzi provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Trouver l'alchimie parfaite entre découvertes culturelles à La Havane et détente balnéaire à Varadero"
            },
            {
                pointPositif: "Plonger dans l'ambiance unique de la vieille ville de La Havane en flânant dans ses ruelles et en rencontrant les Cubains"
            },
            {
                pointPositif: "Prolonger son séjour à Varadero, petit paradis du farniente sur les plus belles plages de Cuba"
            },
            {
                pointPositif: "Profiter de l'excellent service, du confort et de l'emplacement de Melia Internacional 5 *, un petit bijou de la modernité qui a ouvert ses portes en février 2019"
            }
        ],
        descriptionHotel: "En apercevant au loin la belle façade monumentale de l'hôtel Nacional de Cuba 5*, puis en pénétrant dans le lobby, vous aurez comme le sentiment d'entrer dans la légende. Celle d'un hôtel mythique devenu l'un des symboles de l'hôtellerie cubaine. Depuis sa création en 1933, il a vu défiler les célébrités (Churchill, Hemingway, Sinatra) et les tournages de film (le deuxième volet du Parrain). ",
        descriptionSaveurs: "Pour un séjour entre confort et liberté à La Havane, vous séjournerez en formule petit déjeuner ! C'est un véritable voyage culinaire qui vous attend à travers les cartes des trois restaurants de l'hôtel. Éveillez vos papilles avec la cuisine créole de La Barraca, craquez pour le buffet du restaurant La Veranda ou offrez-vous un moment inoubliable au Comedor de Aguiar, l'un des lieux les plus réputés de la ville...  Surtout ne manquez pas la vue superbe offerte depuis le jardin, lieu idéal pour siroter un cocktail !",
        descriptionLoisirs: "Dès le matin, après votre réveil, rendez-vous à la piscine de l'établissement, avant de partir à la conquête de la ville. En soirée, laissez vous envahir par les notes de musique d'un magnifique spectacles au Salon 1930 où se produisent les successeurs du Buena Vista Social Club, pour une expérience unique !",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous aurez le choix entre la classe économique (inclus) ou la classe affaires (en option et avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Vous saurez si le tarif du billet d'avion inclut un bagage lors du choix de la compagnie et des horaires.  Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Les tarifs étant susceptibles d’évoluer, à titre d’exemple, ce bagage peut s’élever sur un Paris/New-York à 68€ pour Air France ou 100€ pour Air Europa par aller simple, soit 136€ et 200€ pour l’aller-retour.  Si vous choisissez une compagnie lowcost, vous pourrez ajouter votre bagage lors du processus de réservation. "
            },
            {
                infosTransferts: "Les transferts sont compris dans nos offres de la façon suivante : - Transferts privatifs entre l'aéroport de La Havane et l'hôtel à La Havane inclus sur les offres avec vol - Transferts collectifs inter-hôtels sur toutes les offres - Transferts collectifs entre l'hôtel à Varadero et l'aéroport de La Havane inclus sur les offres avec vol"
            }
        ],
        rdvA: "Envie de changer d'air ? Préparez-vous à tomber sous le charme de Cuba ! Lors de ce séjour unique, nous vous emmenons à la découverte de la trépidante capitale La Havane, pour une virée authentique, à la découverte d'un patrimoine unique. Vous aurez également le plaisir de vous octroyer un moment rien qu'à vous sur Playa Azul à Varadero... Ne rêvez plus, l'évasion est à portée de clics ! Commencez par la beauté intemporelle de La Havane... Ville coloniale historique inscrite au patrimoine mondial de l’Humanité, elle vous séduira par ses ruelles typiques, son architecture coloniale, son ambiance festive, unique, mêlant de superbes bâtisses historiques, d'autres abandonnées au temps ou certaines rénovées... et toujours en fil conducteur la chaleur cubaine !",
        formulePensionComplete: false,
        formulePetitDejeuner: true,
        formuleSansPension: false,
        transfertCollectif: true,
        prixTransfertPrivatif: 15,
        prixOptionFex: 49,
        prixAssuranceMultirisquePremium: 74,
        prixAssuranceAnnulationPremium: 70
    }

]

export const DESTINATIONSASIE: Destination[] = [
    {
        idDestination: 10,
        continentDestination: "Asie",
        paysDestination: "Maldives",
        villeDestination: "Atoll Malé Nord",
        titreSejour: "Merveilles tropicales avec villa privée et accès direct à la plage",
        nomHotel: "Hôtel Angsana Ihuru 5*",
        urlImage1: "../../assets/Destinations/Asie/Maldives_MaleNord/pexels-asad-photo-maldives-3601425.jpg",
        urlImage2: "../../assets/Destinations/Asie/Maldives_MaleNord/angsana-ihuru.jpg",
        urlImage3: "../../assets/Destinations/Asie/Maldives_MaleNord/male_restaurant.jpg",
        urlImage4: "../../assets/Destinations/Asie/Maldives_MaleNord/pexels-asad-photo-maldives-1450372.jpg",
        urlImage5: "../../assets/Destinations/Asie/Maldives_MaleNord/pexels-asad-photo-maldives-1430677.jpg",
        descriptionImage1: "Photo de Asad Photo Maldives provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Malé",
        descriptionImage3: "Photo du restaurant de l'hotel de Malé",
        descriptionImage4: "Photo de Asad Photo Maldives provenant de Pexels",
        descriptionImage5: "Photo de Asad Photo Maldives provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Poser ses valises dans un véritable havre de paix tropical pour des vacances reposantes"
            },
            {
                pointPositif: "Découvrir une villa posée sur la plage et se réveiller tous les matins avec une vue imprenable sur l'océan"
            },
            {
                pointPositif: "S'accorder une pause agréable à l'Angsana Spa et se laisser aller à des mains expertes"
            },
            {
                pointPositif: "Faire son baptême de plongée aux Maldives grâce au centre située tout près de l'hôtel"
            }
        ],
        descriptionHotel: "L'Hôtel Angsana Ihuru 5* est un véritable havre de paix posé sur une île paradisiaque. Entouré par du sable blanc immaculé et une mer translucide cet établissement a tout pour vous plaire. A vous les moments de détente sur la plage, le snorkelling avec les poissons colorés et les instants bien-être au Angsana Spa.",
        descriptionSaveurs: "Vous passerez des vacances en totale sérénité grâce à votre formule Tout inclus ! Le restaurant Riveli vous propose une cuisine variée, allant des saveurs asiatiques aux plats méditerranéens. Installez-vous en extérieur face à la mer pour apprécier votre repas. Pour déguster un cocktail vous pourrez franchir les portes du Velaavani Bar.",
        descriptionLoisirs: "Débutez votre journée par un plongeon dans l'océan Indien. Partez avec votre masque et votre tuba pour nager avec les poissons multicolores qui peuplent la mer. Continuez votre journée par une séance au Angsana Spa, qui met en avant les essences de fleurs et de fruits. Vous en ressortirez totalement relaxé. Pour profiter aux maximum de l'océan, un centre de plongée et un centre de sports nautiques sont à votre disposition. Ainsi vous pourrez partir à la découverte d'une épave de bateau située à proximité de l'île pour vivre une expérience unique aux Maldives. ",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts en bateau rapide aller-retour entre l’aéroport et l’hôtel sont compris dans nos offres."
            }
        ],
        rdvA: "Fermez les yeux et laissez-vous emporter par un doux rêve...  Avec ses 26 atolls et ses 1000 îles coralliennes posées au cœur de l’océan Indien, les Maldives sont certainement la destination paradisiaque par excellence. Ses lagons turquoise, ses plages de sable blanc bordées de cocotiers et ses fonds marins multicolores en font un véritable un décor de carte postale.  Voyagez d’atoll en atoll et profitez de vacances où tous vos désirs seront réalité. Dans cet environnement aussi intime qu’exclusif, vous pourrez profiter de séances de farniente, bains de soleil ou vous adonner aux joies du snorkeling...",
        formulePensionComplete: true,
        formulePetitDejeuner: false,
        formuleSansPension: false,
        transfertCollectif: true,
        prixTransfertPrivatif: 9,
        prixOptionFex: 56,
        prixAssuranceMultirisquePremium: 88,
        prixAssuranceAnnulationPremium: 79
    },
    {
        idDestination: 11,
        continentDestination: "Asie",
        paysDestination: "Thaïlande",
        villeDestination: "Khao Lak",
        titreSejour: "Echappée inoubliable avec dîner et surclassement inclus",
        nomHotel: "Pullman Khao Lak Resort",
        urlImage1: "../../assets/Destinations/Asie/Thailande_KhaoLak/pexels-alexandr-podvalny-1007657.jpg",
        urlImage2: "../../assets/Destinations/Asie/Thailande_KhaoLak/guest-room (1).jpg",
        urlImage3: "../../assets/Destinations/Asie/Thailande_KhaoLak/khaoLak_restaurant.jpg",
        urlImage4: "../../assets/Destinations/Asie/Thailande_KhaoLak/pullman-khao-lak-resort.jpg",
        urlImage5: "../../assets/Destinations/Asie/Thailande_KhaoLak/pexels-tirachard-kumtanom-472309.jpg",
        descriptionImage1: "Photo de Alexandr Podvalny provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Flic-en-Flac",
        descriptionImage3: "Photo du restaurant de l'hotel de Khao Lak",
        descriptionImage4: "Photo de piscine de l'hotel de Khao Lak",
        descriptionImage5: "Photo de Tirachard Kumtanom provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Séjourner dans un établissement 5* de choix, en bord de mer et niché dans un cadre naturel"
            },
            {
                pointPositif: "Poser ses valises dans une agréable chambre Deluxe Vue Mer, confortable et élégante"
            },
            {
                pointPositif: "Faire voyager ses papilles en dégustant des mets locaux, pour un véritable plaisir gustatif"
            },
            {
                pointPositif: "S'octroyer de doux instants farniente au Spa ou sur la plage, bordée par la mer d'Andaman"
            }
        ],
        descriptionHotel: "Vous avez toujours rêvé d'explorer la majestueuse Khao Lak ? Pour cela, nous avons ce qu'il vous faut ! Le Pullman Khao Lak 5* vous accueille dans un environnement de choix où vous pourrez vous relaxer en toute sérénité avec un cadre à couper le souffle, au bord de la mer d'Andaman. Laissez-vous séduire par un établissement luxueux, où tout a été pensé dans les moindres détails pour votre plus grand confort. Vous y trouverez une décoration raffinée et élégante ainsi que des points de vue sublimes pour admirer de magnifiques coucher de soleil. Une chose est sûre, votre séjour en Thaïlande s'annonce radieux !",
        descriptionSaveurs: "Prenez des forces dès le matin en vous délectant de votre formule petit-déjeuner. En option et avec supplément, vous pourrez également opter pour la formule demi-pension (hors boissons) avec petit-déjeuner et déjeuner ou petit-déjeuner et dîner. Faites voyager vos papilles durant votre séjour en Thaïlande ! Pour cela, offrez-vous un véritable voyage gustatif en vous attablant au restaurant Plai où vous aurez le plaisir de déguster des mets locaux ou internationaux, le tout dans un cadre de rêve. Le Kram Beach Club vous proposera des fruits de mer ou le soir venu, des plats méditerranéens face à une vue imprenable sur la mer d'Andaman.",
        descriptionLoisirs: "Côté loisirs, votre établissement a pensé à tout pour satisfaire la moindre de vos envies. Cette oasis de sérénité vous permettra de vous adonner à une session farniente au bord de la piscine extérieure où vous pourrez peaufiner votre teint sous le soleil radieux de Khao Lak. Les plus sportifs d'entre vous se dirigeront naturellement vers le centre de fitness où ils pourront profiter d'appareils modernes afin de prendre soin de leur corps. ",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts aller-retour entre l’aéroport et l’hôtel ne sont pas compris dans nos offres mais sont disponibles en option avec supplément pour les offres avec vol."
            }
        ],
        rdvA: "Envie de Paradis ? À seulement une heure au nord de l'île de Phuket, dans la province de Phang Nga, Khao Lak est une belle région aux longues plages de sable bordées par les montagnes des parcs nationaux environnants. Encore tout récemment sauvage, l'environnement offre aux amoureux de la nature un paysage préservé. Plages, montagnes couvertes de jungle, vallées boisées, mangroves... Rassurez-vous, la nature a gardé sa place grâce à des constructions se fondant dans le paysage. Alors, exploration ou farniente ? Choisissez !",
        formulePensionComplete: false,
        formulePetitDejeuner: true,
        formuleSansPension: false,
        transfertCollectif: false,
        prixTransfertPrivatif: 9,
        prixOptionFex: 36,
        prixAssuranceMultirisquePremium: 75,
        prixAssuranceAnnulationPremium: 66
    },
    {
        idDestination: 12,
        continentDestination: "Asie",
        paysDestination: "Ile de la Réunion",
        villeDestination: "Saint Gilles Les Bains",
        titreSejour: "Décor de carte postale pour paranthèse de rêve",
        nomHotel: "Hôtel La Résidence Archipel",
        urlImage1: "../../assets/Destinations/Asie/Reunion_SaintGillesLesBains/pexels-artem-beliaikin-853199 (1).jpg",
        urlImage2: "../../assets/Destinations/Asie/Reunion_SaintGillesLesBains/residence-l-archipel.jpg",
        urlImage3: "../../assets/Destinations/Asie/Reunion_SaintGillesLesBains/reunion_restaurant.jpg",
        urlImage4: "../../assets/Destinations/Asie/Reunion_SaintGillesLesBains/residence-l-archipel (1).jpg",
        urlImage5: "../../assets/Destinations/Asie/Reunion_SaintGillesLesBains/reunion-1779297_1920.jpg",
        descriptionImage1: "Photo de Artem Beliaikin provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Saint Gilles Les Bains",
        descriptionImage3: "Photo du restaurant de l'hotel de Saint Gilles Les Bains",
        descriptionImage4: "Photo de piscine de l'hotel de Saint Gilles Les Bains",
        descriptionImage5: "Image par marion beraudias de Pixabay",
        pointsPositifs: [
            {
                pointPositif: "Profiter d'une résidence pleine de charme à l'emplacement idéal"
            },
            {
                pointPositif: "Démarrer la journée avec votre formule petit-déjeuner et plonger dans la piscine lagon"
            },
            {
                pointPositif: "S'offrir un dîner au restaurant et découvrir de délicieuses saveurs créoles"
            },
            {
                pointPositif: "Opter pour une excursion en option comme le survol en hélicoptère"
            }
        ],
        descriptionHotel: "Bienvenue à l'hôtel Résidence Archipel situé entre Saint-Gilles-les-Bains et Boucan Canot. Composée de 52 studios équipés avec kitchenette, vous pourrez apprécier le magnifique jardin tropical entourant ces derniers. Votre résidence sera un pied à terre idéal pour visiter les alentours comme une randonnée dans les cirques de Mafate ou encore les belles plages de l'ouest comme Saint-Gilles ou La Saline. Profitez des différentes activités proposées dans la Résidence comme la piscine, le terrain de tennis ou la salle de Fitness ! Vous nous suivez ?",
        descriptionSaveurs: "Afin de démarrer la journée du bon pied, vous séjournerez en formule petit-déjeuner. Il sera proposé sous forme de buffet de 6h30 à 10h00 du lundi au vendredi et également de 7h à 10h30 le weekend et les jours fériés. Vous pourrez y déguster des confitures locales, fruits frais de saison, thé, café ou encore un choix de charcuteries et fromages... Offrez-vous un dîner au Restaurant l'Instant Gourmand ouvert le midi de 12h à 14h et proposant des plats originaux aux saveurs créoles. Envie de vous désaltérer ? Direction le bar ouvert toute la journée de 7h à 22h !",
        descriptionLoisirs: "Profitez pleinement de vos vacances à l’hôtel Résidence Archipel. Démarrez votre journée par un plongeon dans la piscine lagon de 350 m² puis direction le terrain de tennis en gazon synthétique pour une séance sportive. La Réunion sera également l'endroit idéal pour vous initier aux activités en mer comme la plongée ou le jet ski ! Vous pourrez trouver ces activités (en supplément) au port de Saint-Gilles-les-Bains. Envie de sensations fortes ? Offrez-vous en option et en supplément un survol en hélicoptère pour découvrir les paysages de La Réunion autrement !",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts aller-retour entre l’aéroport et l’hôtel ne sont pas compris dans nos offres mais sont disponibles en option avec supplément pour les offres avec vol."
            }
        ],
        rdvA: "Prêt à vivre intensément ? Terre de métissage, spectaculaire et envoûtante, La Réunion vous accueille dans l’archipel des Mascareignes, au sud-ouest de l’océan Indien, pour une escapade inoubliable. Intensément exotique, grandiose, vibrante, ressourçante et préservée, celle île vous émerveillera autant par ses paysages majestueux, que par sa culture authentique, son vivre ensemble unique et sa cuisine aux mille saveurs. Découvrez une île unique au monde d'où vous pourrez partir à la rencontre des baleines, vous ressourcer au coeur du patrimoine mondial de l’UNESCO, vous laisser hypnotiser par son volcan ou découvrir la culture créole et la richesse du vivre ensemble. L’eau, la terre, l’air, le feu... à la Réunion, les éléments se mêlent pour vous inviter à une épopée unique au coeur de l’océan Indien !",
        formulePensionComplete: false,
        formulePetitDejeuner: true,
        formuleSansPension: false,
        transfertCollectif: false,
        prixTransfertPrivatif: 18,
        prixOptionFex: 45,
        prixAssuranceMultirisquePremium: 78,
        prixAssuranceAnnulationPremium: 72
    },
    {
        idDestination: 13,
        continentDestination: "Asie",
        paysDestination: "Indonésie",
        villeDestination: "Bali",
        titreSejour: "Voyage en Indonésie entre plages et rizières",
        nomHotel: "Best Western Premier Agung 4*",
        urlImage1: "../../assets/Destinations/Asie/Indonesie_Bali/pexels-skitterphoto-654.jpg",
        urlImage2: "../../assets/Destinations/Asie/Indonesie_Bali/our-deluxe-green-view.jpg",
        urlImage3: "../../assets/Destinations/Asie/Indonesie_Bali/upper-and-lower-swimming.jpg",
        urlImage4: "../../assets/Destinations/Asie/Indonesie_Bali/kaamala-resort-ubud.jpg",
        urlImage5: "../../assets/Destinations/Asie/Indonesie_Bali/pexels-dimitri-dim-1802183.jpg",
        descriptionImage1: "Photo de Skitter Photo provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Bali",
        descriptionImage3: "Photo du lounge de l'hotel de Bali",
        descriptionImage4: "Photo du resort de Bali",
        descriptionImage5: "Photo de Dimitri Dim provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Découvrir trois facettes de Bali, entre plages, temples, nature sauvage et rizières en un seul voyage"
            },
            {
                pointPositif: "S'installer dans des hôtels charmants, disposant de tout le confort nécessaire à un séjour inoubliable"
            },
            {
                pointPositif: "Être émerveillé face aux paysages époustouflants de l'île des Dieux"
            },
            {
                pointPositif: "Se laisser tenter par les spécialités locales et faire le plein de saveurs tout au long de son séjour"
            }
        ],
        descriptionHotel: "Bienvenue au Best Western Premier Agung Resort Ubud, un établissement à taille humaine, où vous goûterez à l'hospitalité balinaise dans un décor authentique. Entouré d'une végétation luxuriante, entre rizières et rivière, vous pourrez vous relaxer mais également rejoindre facilement la vie culturelle d'Ubud et les temples qui l'entourent.",
        descriptionSaveurs: "Libre d'organiser vos journées comme bon vous semble, vous profiterez de la formule petit-déjeuner pour 2 personnes. Vous commencerez la journée en douceur par un petit-déjeuner buffet servi au restaurant Taman Lotus. Son style traditionnel et son ouverture sur l'extérieur vous charmeront. Au déjeuner et au dîner, vous pourrez également vous laisser tenter par des spécialités asiatiques et internationales. Envie d'une petite douceur ? Direction le Tamian Sari Coffee Shop pour siroter un verre accompagné d'une glace !",
        descriptionLoisirs: "Entre chacune de vos visites, gardez la forme au centre de fitness, ou laissez-vous tenter par un massage au Spa qui offre un grand choix de soins. Puis filez vous relaxer au bord de la piscine en vous imprégnant de l'atmosphère tranquille des lieux.",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: " Les transferts aller-retour entre l’aéroport et l’hôtel sont compris dans nos offres avec vol. Les transferts inter-hôtels sont inclus dans toutes nos offres et s'effectueront en bateau rapide et en véhicule partagé. "
            }
        ],
        rdvA: "Vous rêvez de vacances sur une île exotique et chaleureuse ? Cap sur Bali, 'l’île des Dieux'. Entre rizières, montagnes, plages de sable fin et forêts, vous serez enchanté par la beauté et la diversité des paysages de chaque région que vous allez visiter. La convivialité, la culture authentique et les traditions encore très encrées vous promettent un dépaysement des plus agréables.",
        formulePensionComplete: false,
        formulePetitDejeuner: true,
        formuleSansPension: false,
        transfertCollectif: true,
        prixTransfertPrivatif: 11,
        prixOptionFex: 37,
        prixAssuranceMultirisquePremium:  75,
        prixAssuranceAnnulationPremium: 60
    }
]

export const DESTINATIONSEUROPE: Destination[] = [
    {
        idDestination: 14,
        continentDestination: "Europe",
        paysDestination: "Italie",
        villeDestination: "Venise",
        titreSejour: "Ecrin intime au design contemporain",
        nomHotel: "Hôtel Aquarius Venice 4*",
        urlImage1: "../../assets/Destinations/Europe/Italie/Venise/pexels-pixabay-161907.jpg",
        urlImage2: "../../assets/Destinations/Europe/Italie/Venise/suite.jpg",
        urlImage3: "../../assets/Destinations/Europe/Italie/Venise/photo3jpg.jpg",
        urlImage4: "../../assets/Destinations/Europe/Italie/Venise/pexels-chait-goli-1796736.jpg",
        urlImage5: "../../assets/Destinations/Europe/Italie/Venise/pexels-pixabay-158441.jpg",
        descriptionImage1: "Photo de Pixabay provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Venise",
        descriptionImage3: "Photo de la terrasse du restaurant de l'hotel de Venise",
        descriptionImage4: "Photo de Chait Goli provenant de Pexels",
        descriptionImage5: "Photo de Pixabay provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Poser ses valises dans un hôtel récemment ouvert dans le centre historique"
            },
            {
                pointPositif: "Profiter d'un moment de détente dans une chambre au décor mariant tradition et modernité"
            },
            {
                pointPositif: "Découvrir les espaces communs de l'hôtel au charme typique vénitien"
            },
            {
                pointPositif: "Partir à la découverte de Venise et sa région, de ses richesses culturelles et de ses trésors cachés"
            }
        ],
        descriptionHotel: "Découvrez l'Hôtel Aquarius Venice 4*, qui a récemment ouvert ses portes dans le centre historique de Venise, au cœur de Campo San Giacomo delll'Orio, situé dans le quartier de Santa Croce. Installé à l'intérieur du Palazzo Zambelli Pemma, ce prestigieux écrin reflète la beauté de Venise, à travers une cour intérieure, un jardin secret ainsi qu'une porte d'eau par laquelle vous pouvez accéder à l'hôtel depuis le canal. L'hôtel se distingue par son design contemporain marié avec élégance aux éléments authentiques et traditionnels du lieu. Vous serez charmé par ce cadre intime et reposant, idéal pour une escapade vénitienne.",
        descriptionSaveurs: "Bénéficiez de la formule petit-déjeuner pour faire le plein d'énergie dès le matin et bien démarrer vos journées. N'hésitez pas à découvrir la gastronomie locale, en vous offrant des moments gourmands. Dégustez l'une des nombreuses spécialités de la région et goûtez à de nouvelles saveurs qui sauront ravir vos papilles.",
        descriptionLoisirs: "Idéalement placé, votre hôtel sera un point de départ parfait pour explorer la Cité des Doges. De la visite de la Basilique Saint Marc à une ballade en gondole à travers les canaux, imprégnez-vous de l'atmosphère unique qui règne à Venise.",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts aller-retour entre l’aéroport et l’hôtel ne sont pas compris dans nos offres mais sont disponibles en option avec supplément pour les offres avec vol."
            }
        ],
        rdvA: "Telle une invitation à la dolce vita, la délicieuse atmosphère romantique qui flotte dans l’air des ruelles fait de Venise, la ville des amoureux par excellence. Le temps d’un week-end à deux ou pour un séjour en famille, succombez au charme de la ville renfermant un patrimoine historique et architectural incroyable que vous pourrez admirer en toute sérénité. Des incontournables attractions, comme la visite de la Basilique Saint-Marc ou du Palais des Dosges, une balade en gondole sur les eaux du Grand Canal, aux lieux moins connus comme l’île de Burano, que vous pourrez rejoindre à bord d’un vaporetto, découvrez tout le charme de la Sérénissime.",
        formulePensionComplete: false,
        formulePetitDejeuner: true,
        formuleSansPension: false,
        transfertCollectif: false,
        prixTransfertPrivatif: 20,
        prixOptionFex: 38,
        prixAssuranceMultirisquePremium: 86,
        prixAssuranceAnnulationPremium: 75
    },
    {
        idDestination: 15,
        continentDestination: "Europe",
        paysDestination: "Italie",
        villeDestination: "Rome",
        titreSejour: "Demeure historique avec rooftop",
        nomHotel: "Hôtel Degli Aranci 4*",
        urlImage1: "../../assets/Destinations/Europe/Italie/Rome/pexels-julius-silver-753639.jpg",
        urlImage2: "../../assets/Destinations/Europe/Italie/Rome/rome_hotel.jpg",
        urlImage3: "../../assets/Destinations/Europe/Italie/Rome/rome_restaurant.jpg",
        urlImage4: "../../assets/Destinations/Europe/Italie/Rome/pexels-samson-bush-2678456.jpg",
        urlImage5: "../../assets/Destinations/Europe/Italie/Rome/pexels-chait-goli-2031967.jpg",
        descriptionImage1: "Photo de Julius Silver provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Rome",
        descriptionImage3: "Photo du restaurant de l'hotel de Rome",
        descriptionImage4: "Photo de Samson Bush provenant de Pexels",
        descriptionImage5: "Photo de Chait Goli provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Résider dans le quartier calme et huppé de Parioli tout en profitant de vues sur la ville depuis la terrasse"
            },
            {
                pointPositif: "Voyager dans le temps grâce au cadre somptueux de cette villa du XIXe siècle"
            },
            {
                pointPositif: "Se prélasser sur la grande terrasse offrant des vues sur la ville"
            },
            {
                pointPositif: "S'amuser à jeter une pièce de monnaie dans la fontaine de Trévi pour faire le vœu de revoir Rome"
            }
        ],
        descriptionHotel: "C'est niché dans un jardin d'orangers et de chênes que vous apercevrez cette magnifique villa du début du XXe siècle dont le caractère et le charme ont été habilement restaurés pour vous offrir le maximum de confort entre technologie et standards hauts de gamme. Avec son restaurant traditionnel, son Spa et sa 'Terrasse des Orangers', l'hôtel Degli Aranci 4* offre à la fois de très jolies vues sur la ville et un point de départ à l'écart de l'agitation pour explorer la ville Éternelle.",
        descriptionSaveurs: "Bénéficiez de la formule petit-déjeuner, pour faire le plein d'énergie dès le matin et bien démarrer vos journées. Offrez-vous des moments gourmands pour goûter les plats raffinés du restaurant 'Il fiore d'Arancio' qui vous accueille dans une atmosphère paisible avec sa véranda et sa terrasse panoramique, entourée de chênes et d'orangers... Il offre une large sélection de plats recherchés faits de produits naturels et sublimés par les mains habiles du chef.  La Terrasse de l’hôtel, à disposition du printemps à l'automne, est l'endroit idéal pour déguster les saveurs de la cuisine méditerranéenne, pour des moments de détente, pour boire un verre ou dîner aux chandelles tout en profitant d'une viande savoureuse cuite sur le grill.",
        descriptionLoisirs: "Après avoir traversé la ville en quête de ses précieux trésors, offrez-vous un instant zen au Spa de l'hôtel. Une parenthèse bien-être bien méritée !  Et si vous avez encore de l'énergie, une salle de fitness est à votre disposition. De quoi succomber sans complexe aux subtiles saveurs de la gastronomie italienne !",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous aurez le choix entre la classe économique (inclus) ou la classe premium économique (en option avec supplément) ou la classe affaires (en option et avec supplément)."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts aller-retour entre l’aéroport et l’hôtel ne sont pas compris dans nos offres mais sont disponibles en option avec supplément pour les offres avec vol."
            }
        ],
        rdvA: "« Tous les chemins mènent à Rome »… Et si c’était celui de vos prochaines vacances ? Partez à la découverte de la Ville aux Sept Collines, l’incontournable musée à ciel ouvert, qui vous réserve de nombreuses surprises et des expériences inoubliables. Laissez-vous guider parmi les merveilles historiques, architecturales et culturelles de la ville, héritées d’un riche passé, et plongez dans un véritable voyage à travers le temps. De la Basilique Saint-Pierre au Colisée, en passant par le Vatican, vous serez subjugué par la beauté de ses monuments.",
        formulePensionComplete: false,
        formulePetitDejeuner: true,
        formuleSansPension: false,
        transfertCollectif: false,
        prixTransfertPrivatif: 22,
        prixOptionFex: 40,
        prixAssuranceMultirisquePremium: 90,
        prixAssuranceAnnulationPremium: 82
    },
    {
        idDestination: 16,
        continentDestination: "Europe",
        paysDestination: "Espagne",
        villeDestination: "Séville",
        titreSejour: "Elégance et modernité en Suite",
        nomHotel: "Patio de la Cartuja",
        urlImage1: "../../assets/Destinations/Europe/Espagne_Seville/pexels-reinhard-bruckner-4605666.jpg",
        urlImage2: "../../assets/Destinations/Europe/Espagne_Seville/patio-de-la-cartuja.jpg",
        urlImage3: "../../assets/Destinations/Europe/Espagne_Seville/corral-andaluz.jpg",
        urlImage4: "../../assets/Destinations/Europe/Espagne_Seville/seville_loisirs.jpg",
        urlImage5: "../../assets/Destinations/Europe/Espagne_Seville/pexels-nizar-ben-halilou-5110037.jpg",
        descriptionImage1: "Photo de Reinhard Bruckner provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Séville",
        descriptionImage3: "Photo du patio de l'hotel de Séville",
        descriptionImage4: "Photo de Séville",
        descriptionImage5: "Photo de Nizar Ben Halilou provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Séjourner dans un hôtel intimiste aux suites modernes et parfaitement équipées"
            },
            {
                pointPositif: "Bénéficier de la formule petit-déjeuner pour faire le plein d'énergie dès le matin"
            },
            {
                pointPositif: "Profiter d'un instant de détente dans le patio de l'hôtel"
            },
            {
                pointPositif: "Partir à la découverte de la ville, de ses richesses culturelles et de ses trésors cachés"
            }
        ],
        descriptionHotel: "Bienvenue au Patio de la Cartuja 3* qui vous accueille dans une oasis de tranquillité au cœur de la ville. Dotés d'équipements modernes et d'une décoration épurée, les suites vous assurent un séjour en tout confort, idéal pour profiter d'instants de repos après une journée riche en découvertes dans la ville. Vous vous retrouverez dans un cadre typique andalou, inspiré par le style arabe avec un magnifique patio décoré de plantes, où vous pourrez vous détendre calmement.",
        descriptionSaveurs: "Bénéficiez de la formule petit-déjeuner pour faire le plein d'énergie dès le matin et bien démarrer vos journées. Savourez un délicieux petit-déjeuner et faites le plein de vitamines avant de partir à la découverte des environs. Offrez-vous des moments gourmands en goûtant aux spécialités de la région et dégustez de nouvelles saveurs qui sauront émerveiller vos papilles.",
        descriptionLoisirs: "Envie de vous relaxer ? Vous pourrez vous détendre en vous installant dans le patio pour bouquiner et partager vos impressions sur vos visites. Faites un tour à la cathédrale de Séville avant de visiter le Real Alcazar ou d'admirer l'architecture remarquable des archives des Indes. Le centre historique de Séville et ses merveilles n'attendent que vous !",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts aller-retour entre l’aéroport et l’hôtel ne sont pas compris dans nos offres mais sont disponibles en option avec supplément pour les offres avec vol."
            }
        ],
        rdvA: "« Qui n’a jamais vu Séville n’a jamais vu de merveille »  dicton espagnol. Avec son riche passé historique, la capitale de la communauté autonome d’Andalousie vous séduira dès les premiers instants. La belle Séville a hérité des influences de la culture chrétienne et musulmane, qui ont cohabité en son sein durant plusieurs années, et a su les conserver à travers le temps. Vous en prendrez plein la vue face à ses monuments mythiques, comme l’Alcazar, inscrit au patrimoine mondial de l’UNESCO, ou la cathédrale et sa Giralda. Flânez le long du Guadalquivir, baladez-vous au cœur du quartier historique et prenez le temps d’observer les somptueux détails de la grande Plaza de España… Besoin d’une pause fraîcheur ? Détendez-vous au parc Maria Luisa, le plus célèbre de la ville. Faites également voyager vos papilles autour d’un délicieux gaspacho ou d’un cocido Andaluz… Laissez-vous séduire par la belle andalouse !",
        formulePensionComplete: false,
        formulePetitDejeuner: true,
        formuleSansPension: false,
        transfertCollectif: false,
        prixTransfertPrivatif: 14,
        prixOptionFex: 34,
        prixAssuranceMultirisquePremium: 77,
        prixAssuranceAnnulationPremium: 71
    },
    {
        idDestination: 17,
        continentDestination: "Europe",
        paysDestination: "Baléares",
        villeDestination: "Majorque",
        titreSejour: "Farniente et initmité dans une crique avec surclassement",
        nomHotel: "Son Caliu Hôtel Spa Oasis 4*",
        urlImage1: "../../assets/Destinations/Europe/Baleares_Majorque/pexels-johannes-w-63508.jpg",
        urlImage2: "../../assets/Destinations/Europe/Baleares_Majorque/habitacion-superior.jpg",
        urlImage3: "../../assets/Destinations/Europe/Baleares_Majorque/hotel-son-caliu-spa-oasis.jpg",
        urlImage4: "../../assets/Destinations/Europe/Baleares_Majorque/entrada.jpg",
        urlImage5: "../../assets/Destinations/Europe/Baleares_Majorque/pexels-jo-kassis-5522406.jpg",
        descriptionImage1: "Photo de Johannes W provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Majorque",
        descriptionImage3: "Photo de la terrasse du restaurant de l'hotel de Majorque",
        descriptionImage4: "Photo de l'entrée de l'hotel de Majorque",
        descriptionImage5: "Photo de Jo Kassis provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "S'offrir une parenthèse de douceur au Spa et profiter d'un accès à l'une des plus jolies criques de Palmanova"
            },
            {
                pointPositif: "Bénéficier d'un accueil attentionné, d'une pension de qualité dans un établissement francophone emblématique de la région"
            },
            {
                pointPositif: "Se laisser tenter par de délicieux mets avec la formule demi-pension (hors boissons)"
            },
            {
                pointPositif: "S'aventurer dans la nature majorquine et découvrir des paysages incroyables !"
            }
        ],
        descriptionHotel: "Inauguré en 1964, l’hôtel Son Caliu Spa Oasis 4* est l’ un des hôtels les plus emblématiques de la zone de Palmanova. Tout au long de son histoire, l'établissement a su tout mettre en œuvre pour toujours répondre aux besoins de ses clients notamment à travers des rénovations régulières. Situé dans une petite crique privée et entouré de jardins tropicaux , entre Palmanova et Puerto Portals, à seulement 12 kilomètres du centre de la ville de Palma, il est un point de chute idéal pour le visiteur qui aime mixer découvertes et farniente...",
        descriptionSaveurs: "Nous avons sélectionné pour vous la formule demi-pension (hors boissons).  Vous prendrez vos petits-déjeuners et vos dîners (hors boissons) au restaurant principal le Biniagual ou au Terraza tout en profitant de superbes vues mer. Le restaurant Biniagual vous reçoit et vous propose une cuisine méditerranéenne et internationale. Trois bars sont également à votre disposition pour vous rafraîchir dans une ambiance détendue comme au bar de la piscine. Le Terraza Garden Bar vous accueil dans un décor nature dans les jardins de l'hôtel.",
        descriptionLoisirs: "Les activités proposées au Son Caliu feront de vos vacances un souvenir inoubliable. Yoga, tennis, golf, plongée... C’est un hôtel idéal pour la pratique de sports, pour se détendre dans les piscines thermales ou pour assister à des soirées musicales dès le coucher du soleil... Découvrez le Spa de l'hôtel : grande piscine intérieure chauffée avec jets de massage et transats, sauna finlandais, hammam aux huiles essentielles, hammam aux sels marins, thermes romains, douches sensorielles, fontaine à glace, zone de relaxation et cours de sport (yoga, zumba, etc.). Grâce à la rénovation de votre hôtel, un tout nouveau centre de vélo a vu le jour. Vous y apprécierez l'expertise professionnelle du cycliste professionnel, Guido Eickelbeck ! Le paysage et le climat de la Méditerranée font de Majorque une destination de choix pour jouir de toutes sortes d’activités en plein air... Vous ne serez pas déçu !",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts aller-retour entre l’aéroport et l’hôtel ne sont pas compris dans nos offres. Vous pourrez sélectionner les transferts privés, disponibles en option avec supplément pour les offres avec vol."
            }
        ],
        rdvA: "Découvrez la plus grande île des Baléares, Majorque. En plein cœur de la Méditerranée, vous embarquerez pour un voyage unique, sous le signe de la détente. Laissez vous surprendre par les nombreux trésors naturels et culturels dont regorge l’île. Paysages méditerranéens sauvages, villages authentiques et plages aux eaux claires s’offrent à vous. N’hésitez pas à prendre de la hauteur au sommet du Puig Major ou sur les falaises de Cap Formentor et faites face à des panoramas merveilleux, où les champs de pins et d’oliviers viennent rencontrer la mer au loin. La belle Majorque vous invite à un séjour farniente, à partager en famille ou entre amis.",
        formulePensionComplete: true,
        formulePetitDejeuner: false,
        formuleSansPension: false,
        transfertCollectif: false,
        prixTransfertPrivatif: 17,
        prixOptionFex: 35,
        prixAssuranceMultirisquePremium: 86,
        prixAssuranceAnnulationPremium: 75
    },
    {
        idDestination: 18,
        continentDestination: "Europe",
        paysDestination: "Grèce",
        villeDestination: "Santorin",
        titreSejour: "Divine odyssée face à la mer",
        nomHotel: "Andronis Arcadia Hotel 5*",
        urlImage1: "../../assets/Destinations/Europe/Grece_Santorin/pexels-pixabay-259585.jpg",
        urlImage2: "../../assets/Destinations/Europe/Grece_Santorin/andronis-arcadia-hotel (1).jpg",
        urlImage3: "../../assets/Destinations/Europe/Grece_Santorin/santorin_restaurant.jpg",
        urlImage4: "../../assets/Destinations/Europe/Grece_Santorin/pexels-jimmy-teoh-1010646.jpg",
        urlImage5: "../../assets/Destinations/Europe/Grece_Santorin/pexels-pixabay-163864 (1).jpg",
        descriptionImage1: "Photo de Pixabay provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Santorin",
        descriptionImage3: "Photo du restaurant de l'hotel de Santorin",
        descriptionImage4: "Photo de jimmy teoh provenant de Pexels",
        descriptionImage5: "Photo de Pixabay provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Poser ses valises dans un établissement sublime, niché dans un environnement à couper le souffle"
            },
            {
                pointPositif: "Avoir le choix de se lover dans d'agréables suites ou dans une villa tout confort"
            },
            {
                pointPositif: "Faire voyager ses papilles en se délectant de mets et saveurs typiques de l'île"
            },
            {
                pointPositif: "Percer à jour les nombreux secrets de Santorin, pour des souvenirs indélébiles"
            }
        ],
        descriptionHotel: "Plongez dans l'univers somptueux et fascinant du sublime Andronis Arcadia Hotel 5* et découvrez un établissement de choix, au style élégant et à la décoration soignée, où vous pourrez vous relaxer en toute sérénité lors de votre divine échappée à Santorin. Nommé en l'honneur du dieu mythologique Pan, votre hôtel est niché à proximité de la pittoresque Oia, dans une véritable oasis de paix et de détente. Profitez du confort de ses commodités modernes et laissez-vous entraîner dans cette atmosphère parfaite.",
        descriptionSaveurs: "Prenez des forces dès le matin en vous délectant d'une savoureuse formule petit-déjeuner. Faites voyager vos papilles et tombez sous le charme de la gastronomie grecque en vous dirigeant vers le restaurant Opson où vous pourrez vous délecter de savoureux mets soigneusement élaborés par le Chef Stefanos Kolimadis qui vous invitera aux délices de la cuisine de l'Antiquité en vous proposant des plats appréciés par les plus grands philosophes tels qu'Aristote, Platon et Omiros.",
        descriptionLoisirs: "Côté loisirs votre établissement a pensé à tout dans les moindres détails pour que vous viviez un séjour de rêve. Profitez d'un doux moment de sérénité près de la somptueuse piscine où vous pourrez profiter des bienfaits du soleil de cette région de choix afin de peaufiner votre teint le tout, avec une vue imprenable sur la mer Égée.",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts aller-retour entre l’aéroport et l’hôtel ne sont pas compris dans nos offres. Vous pourrez sélectionner les transferts privés, disponibles en option avec supplément pour les offres avec vol."
            }
        ],
        rdvA: "Percez les mystères de l’Atlantide ! Santorin, seule île d’origine volcanique des Cyclades, serait l’emplacement actuel de la cité engloutie. Entre falaises ocre et noire et maisons blanchies à la chaux, Santorin, terre de contrastes, fascine les visiteurs du monde entier. La découverte des villages de l’île se fait en grande partie à pieds. Rien n’est alors plus agréable que de flâner dans les ruelles escarpées tout en profitant de panoramas saisissants. Le soir, profitez du spectacle magique qu'offre le coucher de soleil sur la Caldeira... Un moment d'émotion inoubliable.",
        formulePensionComplete: false,
        formulePetitDejeuner: true,
        formuleSansPension: false,
        transfertCollectif: false,
        prixTransfertPrivatif: 24,
        prixOptionFex: 39,
        prixAssuranceMultirisquePremium: 91,
        prixAssuranceAnnulationPremium: 82
    },
    {
        idDestination: 19,
        continentDestination: "Europe",
        paysDestination: "Suisse",
        villeDestination: "Grindelwald",
        titreSejour: "Vacances raffinées au coeur des Alpes suisses",
        nomHotel: "Romantik Hotel Schweizerhof 5*",
        urlImage1: "../../assets/Destinations/Europe/Suisse_Grindelwald/terrace-chalet-rotstocki.jpg",
        urlImage2: "../../assets/Destinations/Europe/Suisse_Grindelwald/bedroom-suite-chalet.jpg",
        urlImage3: "../../assets/Destinations/Europe/Suisse_Grindelwald/suisse_restaurant.jpg",
        urlImage4: "../../assets/Destinations/Europe/Suisse_Grindelwald/tiefverschneit-im-winter.jpg",
        urlImage5: "../../assets/Destinations/Europe/Suisse_Grindelwald/pexels-denis-linine-714256.jpg",
        descriptionImage1: "Photo de Grindelwald",
        descriptionImage2: "Photo de l'hotel de Grindelwald",
        descriptionImage3: "Photo du restaurant de l'hotel de Grindelwald",
        descriptionImage4: "Photo de l'hotel de Grindelwald",
        descriptionImage5: "Photo de Denis Linine provenant de Pexels",
        pointsPositifs: [
            {
                pointPositif: "Séjourner au beau milieu des Alpes suisses, au pied de l'Eiger"
            },
            {
                pointPositif: "S'offrir un moment bien-être au Spa après une journée sur les pistes"
            },
            {
                pointPositif: "Goûter la cuisine traditionnelle du restaurant de l'hôtel pour découvrir de nouvelles saveurs"
            },
            {
                pointPositif: "Partir découvrir la nature unique de la région, en profitant de l'air frais et des grands espaces !"
            }
        ],
        descriptionHotel: "Le Romantik Hotel Schweizerhof Resort & Spa 5* bénéficie d'un emplacement idyllique au cœur des montagnes offrant un panorama de rêve. En quelques minutes à pied, vous atteignez le centre de Grindelwald. L'hôtel a mis l'accent sur l'hospitalité, le confort et la bonne nourriture, dans une atmosphère détendue et conviviale. Vous trouverez des installations de qualité telles qu'une agréable piscine intérieure et un maginfique Spa.    ",
        descriptionSaveurs: "Profitez de votre formule demi-pension (hors boissons) pendant toute la durée de votre séjour. Le restaurant Alpterrassen propose tous les jours un copieux petit-déjeuner buffet composé d'un grand choix de plats diététiques, sucrés et salés. Pour le dîner, dégustez une sélection de plats régionaux et internationaux. Le restaurant à la carte Schmitte propose une combinaison de cuisine internationale, suisse et alpine avec une sélection d'excellents vins de la cave. Asseyez-vous et détendez-vous dans cette belle atmosphère tout en écoutant les sons du piano. Le restaurant Gaststubli est idéal pour un déjeuner léger ou un dîner traditionnel. Des spécialités locales telles que la fondue au fromage sont servies ici. En soirée, le bar de l' hôtel vaut le détour pour un sherry ou un délicieux cocktail.",
        descriptionLoisirs: "Offrez-vous un moment pour vous (ou à deux) au Spa ! Avec sa grande piscine intérieure chauffée, sa terrasse ensoleillée, son hammam, son sauna bio, son sauna finlandais, sa grotte de glacier et sa douche tropicale, le bien-être est garanti. Des soins spa sont également proposés, si vous le souhaitez ! De quoi se détendre chaque muscle avant ou après quelques descentes de pistes.",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous voyagerez en classe économique (inclus) ou en classe affaires (en option avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. Ce vol peut comporter une ou plusieurs escales."
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts aller-retour entre l’aéroport et l’hôtel ne sont pas compris dans nos offres mais sont disponibles en option avec supplément pour les offres avec vol."
            }
        ],
        rdvA: "Respirez à fond, profitez de l'air pur de la montagne et laissez vos yeux vagabonder dans les montagnes sans fin!  Dans l’Oberland bernois, au cœur de la Suisse, se trouve la sympathique station de montagne de Grindelwald , juste au pied de la célèbre face nord de l’Eiger. Depuis l'hôtel, vous pourrez rejoindre le centre-ville en quelques minutes seulement. Le village de Grindelwald propose des sentiers de randonnées et des activités comme le vélo ou du parapente. La rue principale vous propose également des boutiques et des cafés. L'endroit parfait pour profiter d'un séjour paisible et romantique dans les Alpes suisses !  On vous emmène ?",
        formulePensionComplete: true,
        formulePetitDejeuner: false,
        formuleSansPension: false,
        transfertCollectif: false,
        prixTransfertPrivatif: 30,
        prixOptionFex: 42,
        prixAssuranceMultirisquePremium: 82,
        prixAssuranceAnnulationPremium: 75
    }
]

export const DESTINATIONSOCEANIE: Destination[] = [
    {
        idDestination: 20,
        continentDestination: "Océanie",
        paysDestination: "Polynésie Française",
        villeDestination: "Tahiti",
        titreSejour: "Escale paradisiaque au bout du monde",
        nomHotel: "Pearl Resorts",
        urlImage1: "../../assets/Destinations/Oceanie/PolynesieFR_Tahiti/pexels-julius-silver-753626.jpg",
        urlImage2: "../../assets/Destinations/Oceanie/PolynesieFR_Tahiti/ocean-view-room-with.jpg",
        urlImage3: "../../assets/Destinations/Oceanie/PolynesieFR_Tahiti/radisson-plaza-resort.jpg",
        urlImage4: "../../assets/Destinations/Oceanie/PolynesieFR_Tahiti/tahiti-pearl-beach-resort.jpg",
        urlImage5: "../../assets/Destinations/Oceanie/PolynesieFR_Tahiti/radisson-plaza-resort (1).jpg",
        descriptionImage1: "Photo de Julius Silver provenant de Pexels",
        descriptionImage2: "Photo de l'hotel de Tahiti",
        descriptionImage3: "Photo de la terrasse du restaurant de l'hotel de Tahiti",
        descriptionImage4: "Photo du resort de Tahiti",
        descriptionImage5: "Photo du resort de Tahiti vue océan",
        pointsPositifs: [
            {
                pointPositif: "Découvrir la beauté de la Polynésie française, entre Tahiti et Bora Bora"
            },
            {
                pointPositif: "S'installer dans des hôtels by Pearl Resorts, dans un cadre unique, entouré d'un lagon turquoise et d'une nature préservée"
            },
            {
                pointPositif: "Poser ses valides dans des chambres raffinées et confortables, avec vue sur l'océan ou accès direct au lagon"
            },
            {
                pointPositif: "Partir à la découverte de la région si particulière de la Polynésie française à travers vos deux excursions incluses"
            }
        ],
        descriptionHotel: "Parfaitement intégré dans un décor luxuriant et doté d'une vue imprenable sur l'océan, cette adresse incontournable de la côte Est de Tahiti est située 7 kilomètres de Papeete et 20 minutes de l'aéroport. Surplombant la plage volcanique naturelle Lafayette et niché au creux de la Baie de Matavai, le Tahiti by Pearl Resort vous accueille avec une promesse de détente en toute sérénité.",
        descriptionSaveurs: "Avec la formule petit-déjeuner, vous serez libre de composer le programme comme bon vous semble ! A Tahiti vous pourrez déguster l'excellente cuisine locale et internationale servie au Hiti Mahana. C'est en bord de piscine que vous serez attablés pour partager ce repas. Envie d'une pause douceur autour d'un verre ? Dirigez-vous vers le Bay Bar, d'où vous admirerez le coucher du soleil face à l'océan.",
        descriptionLoisirs: "De nombreuses animations sont organisées à l'hôtel pour vous garantir des soirées inoubliables. Entre spectacle Marquisien et spectacle traditionnel Polynésien (selon programme). Vous allez en prendre plein les yeux ! D'autres soirées Bay Bar Sunset Live sont prévues de façon ponctuelle. Vous aurez accès à la piscine et pour les plus sportifs à la salle de fitness. Pour une parenthèse relaxante, sauna et hammam seront au programme. Un marché prend également place dans l'hôtel pour vous faire découvrir l'artisanat local. Enfin, vous pourrez emprunter les navettes mises à disposition pour rejoindre le centre-ville de Papeete.",
        modalitesVoyage: [
            {
                infosVol: "Si vous choisissez un séjour avec vols, vous aurez le choix entre la classe économique (inclus) ou la classe affaires (en option et avec supplément). Vous aurez la possibilité de choisir votre compagnie et vos horaires de vols lors de votre réservation. "
            },
            {
                infosBaggages: "Bagages, repas et autres prestations à bord de certains vols pouvant être payants. Pour savoir si le tarif du billet d’avion inclut un bagage, cliquez sur « détails » lors du choix de votre compagnie aérienne au moment de votre réservation. Si votre tarif n’inclut pas de bagage, nous vous invitons à vous rendre avant votre départ sur le site de la compagnie munis de votre référence dossier transport pour connaitre le tarif exact et acheter votre bagage supplémentaire. Certains vols peuvent s'effectuer de nuit, une arrivée matinale est donc possible."
            },
            {
                infosTransferts: "Les transferts collectifs suivants sont compris dans nos offres : - Transferts entre l'aéroport international de Papeete et l'hôtel Tahiti by Pearl Resort - Transferts entre l'hôtel Tahiti by Pearl Resort et l'aéroport domestique - Transferts entre l'aéroport de Bora Bora et l'hôtel Bora Bora by Pearl Resort"
            }
        ],
        rdvA: "Tout commence et se termine sur l'île de Tahiti. La plus connue des îles de Polynésie fait toujours autant rêver. Vous y serez accueilli à bras ouverts et découvrirez une culture étonnante et passionnante. Bercé par le clapotis de l'eau, profitez de moments de détente sur les plages idylliques, avant de plonger, avec masque et tuba, dans l'océan. Ouvrez grand vos yeux, vous n'en reviendrez pas ! Le fond marin y est particulièrement diversifié. N'hésitez pas à vous écarter de la plage pour arpenter les routes de l'île, participer aux animations locales et goûter aux saveurs exotiques.",
        formulePensionComplete: false,
        formulePetitDejeuner: true,
        formuleSansPension: false,
        transfertCollectif: true,
        prixTransfertPrivatif: 23,
        prixOptionFex: 49,
        prixAssuranceMultirisquePremium: 92,
        prixAssuranceAnnulationPremium: 86
    }
]