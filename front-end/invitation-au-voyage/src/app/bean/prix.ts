export class Prix {

    public prixAPartirDeSansVol: number;
    public prixAPartirDeAvecVol: number;
    public prixVolAuDepartDeParis: number;
	public prixVolAuDepartDeMarseille: number;
	public prixVolAuDepartDeLyon: number;
	public prixVolAuDepartDeNantes: number;

    constructor(prixAPartirDeSansVol: number, prixAPartirDeAvecVol: number, prixVolAuDepartDeParis: number, prixVolAuDepartDeMarseille: number, 
        prixVolAuDepartDeLyon: number, prixVolAuDepartDeNantes: number){
        this.prixAPartirDeSansVol = prixAPartirDeSansVol;
        this.prixAPartirDeAvecVol = prixAPartirDeAvecVol;
        this.prixVolAuDepartDeParis = prixVolAuDepartDeParis;
        this.prixVolAuDepartDeMarseille = prixVolAuDepartDeMarseille;
        this.prixVolAuDepartDeLyon = prixVolAuDepartDeLyon;
        this.prixVolAuDepartDeNantes = prixVolAuDepartDeNantes;
    }
}
