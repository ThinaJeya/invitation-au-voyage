import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Devis } from '../bean/devis';
import { ContinentsDestinationsService } from '../services/continents-destinations.service';
import { DevisService } from '../services/devis.service';
import { Prix } from '../bean/prix';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-details-voyage',
  templateUrl: './details-voyage.component.html',
  styleUrls: ['./details-voyage.component.scss']
})
export class DetailsVoyageComponent implements OnInit, OnDestroy {

  /**
   * Nom du Continent inscrit dans l'url
   */
  continentParams: string;

  /**
   * Id de la destination inscrit dans l'url
   */
  idDestinationParams: number;

  /**
   * Nom du Pays inscrit dans l'url
   */
  paysDestinationsParams: string;

  /**
   * Variable contenant la plus petite durée de séjour disponible pour la destination
   */
  plusPetiteDureeSejour: number = 0;

  /**
   * Array contenant les détails des voyages selon le nom du continent
   */
  tableauDesDestinations: any[] = [];

  /**
   * Array contenant les détails du voyage selon l'id de la destination
   */
  tableauDetailsDuVoyage: any[] = [];
  arrayPrix: Prix = new Prix(0, 0, 0, 0, 0, 0);
  arrayPrixSubscription: Subscription;
  tarifsDurees: Subscription;

  /**
   * Array détails de tarifs en fonction de la durée du séjour
   */
  arrayDureeTarifsHebergement: any[] = [];

  dateDispoJusquau = new Date();

  devisForm: FormGroup;

  transport: boolean = false;
  nbNuits: number = 0;
  villeDepartManquante: boolean = false;

  nbParticipantsAdulte: number = 2;
  nbParticipantsEnfant: number = 0;
  nbParticipantsBebe: number = 0;

  devisEstEnregistre: any = false;

  constructor(private continentsDestinationsService: ContinentsDestinationsService, private activatedRoute: ActivatedRoute, public datePipe: DatePipe,
    private devisService: DevisService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {

    this.continentsDestinationsService.chargementSpinner();

    this.recupererParametresUrl();

    this.recupererDetailsVoyage(this.tableauDesDestinations, this.idDestinationParams, this.continentParams, this.tableauDetailsDuVoyage);

    this.afficherDateDispoJusquau(this.continentParams, this.dateDispoJusquau);

    this.obtenirPlusPetiteDureeSejour(this.continentParams, this.plusPetiteDureeSejour);

    this.tarifsDurees = this.recupererTarifsDureesSejour(this.idDestinationParams);

    this.arrayPrixSubscription = this.afficherPrixFixes();

    this.initForm();

  }

  ngOnDestroy() {
    this.arrayPrixSubscription.unsubscribe();
    this.tarifsDurees.unsubscribe();
  }

  /**
   * Permet de déterminer la plus petite durée de séjour selon le continent
   * @param plusPetitSejour 
   * @returns plus petite durée de séjour pour la destination
   */
  obtenirPlusPetiteDureeSejour(continentDestination: string, plusPetitSejour: number) {
    if (continentDestination === 'Afrique' || continentDestination === 'Asie' || continentDestination === 'Amérique' || continentDestination === 'Océanie') {
      plusPetitSejour = 7;
    } else {
      plusPetitSejour = 3;
    }
    return plusPetitSejour;
  }

  /**
   * Permet de récupérer les tarifs de prix de départ pour la destination
   * @returns objet Prix (tarifs à partir de)
   */
  afficherPrixFixes(): Subscription {

    var sejourDTO =
    {
      "idDestination": this.idDestinationParams,
      "transport": true,
      "villeDepart": "Paris",
      "nbNuits": this.obtenirPlusPetiteDureeSejour(this.continentParams, this.plusPetiteDureeSejour),
      "nbParticipantsAdulte": 1,
      "nbParticipantsEnfant": 0
    };

    return this.devisService.getPrixSejourAPartirDe(sejourDTO)
      .subscribe(
        (response) => {
          this.arrayPrix.prixAPartirDeSansVol = response['prixAPartirDeSansVol'];
          this.arrayPrix.prixAPartirDeAvecVol = response['prixAPartirDeAvecVol'];
          this.arrayPrix.prixVolAuDepartDeParis = response['prixVolAuDepartDeParis'];
          this.arrayPrix.prixVolAuDepartDeMarseille = response['prixVolAuDepartDeMarseille'];
          this.arrayPrix.prixVolAuDepartDeLyon = response['prixVolAuDepartDeLyon'];
          this.arrayPrix.prixVolAuDepartDeNantes = response['prixVolAuDepartDeNantes'];
        },
        (error) => {
          console.log('Erreur : ' + error);
        }
      );
  }

  /**
   * Permet de récupérer les tarifs des séjours selon leurs durées
   * @param idDestination 
   * @returns array contenant les tarifs des séjours selon leurs durées 
   */
  recupererTarifsDureesSejour(idDestination: number): Subscription {
    return this.devisService.getPrixHebergementSelonDureesDestination(idDestination)
      .subscribe(
        (response) => {
          this.arrayDureeTarifsHebergement = response;
          this.plusPetiteDureeSejour = response[0]['dureeHebergement'];
        },
        (error) => {
          console.log('Erreur : ' + error);
        }
      );
  }

  /**
   * Permet de récupérer les paramètres d'url (nom du continent, id destination, pays de la destination)
   */
  recupererParametresUrl() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.continentParams = params.get('nom-continent');
      this.idDestinationParams = +params.get('id');
      this.paysDestinationsParams = params.get('pays-destination');

    });
  }

  /**
   * Permet de récupérer les détails du voyage selon l'id de la destination
   * @param tableauDestinationsContinent 
   * @param idParams 
   * @param continentParams 
   * @param tableauDetailsVoyage 
   */
  recupererDetailsVoyage(tableauDestinationsContinent: any[], idParams: number, continentParams: string, tableauDetailsVoyage: any[]) {
    tableauDestinationsContinent = this.continentsDestinationsService.getDestinationsSelonContinent(continentParams);

    for (let destination of tableauDestinationsContinent) {
      if (destination.idDestination === idParams) {
        tableauDetailsVoyage.push(destination);
      }
    }
  }

  /**
   * Affiche une date de disponibilité du séjour dans une fourchette de temps en fonction du continent de la destination
   * @param continentParams 
   * @param dateAModifier 
   */
  afficherDateDispoJusquau(continentParams: string, dateAModifier: Date) {
    switch (continentParams) {
      case 'Afrique':
        dateAModifier.setDate(dateAModifier.getDate() + (Math.random() * 42 - 21) + 21);
        break;
      case 'Amérique':
        dateAModifier.setDate(dateAModifier.getDate() + (Math.random() * 49 - 28) + 28);
        break;
      case 'Asie':
        dateAModifier.setDate(dateAModifier.getDate() + (Math.random() * 60 - 31) + 31);
        break;
      case 'Europe':
        dateAModifier.setDate(dateAModifier.getDate() + (Math.random() * 15 - 7) + 7);
        break;
      case 'Océanie':
        dateAModifier.setDate(dateAModifier.getDate() + (Math.random() * 90 - 30) + 30);
        break;
    }
  }

  /**
   * Permet de mettre à jour la valeur de durée du séjour au clic par l'utilisateur
   * @param value la valeur
   * @param i index 
   */
  choixDureeSejour(value: any, i: number) {
    var elements = document.getElementsByClassName('petit-carre');

    for (let j = 0; j < elements.length; j++) {
      elements[j].firstElementChild.setAttribute("style", "background-color: white");
    }

    document.getElementsByClassName('petit-carre')[i].firstElementChild.setAttribute("style", "background-color: rgb(238, 234, 234)");
    if (value > 0) {
      this.nbNuits = value;
    }
  }

  /**
   * Permet de mettre à jour le statut "vol inclu" à false
   */
  miseAjourStatutTransportFalse() {
    var divSansVol = document.getElementById('btn-sans-vol');
    var divAvecVol = document.getElementById('btn-avec-vol');

    divAvecVol.setAttribute("style", "background-color: white)");
    divSansVol.setAttribute("style", "background-color: rgb(238, 234, 234)");

    this.transport = false;
    this.villeDepartManquante = false;
  }

  /**
   * Permet de mettre à jour le statut "vol inclu" à true
   */
  miseAjourStatutTransportTrue() {
    var divSansVol = document.getElementById('btn-sans-vol');
    var divAvecVol = document.getElementById('btn-avec-vol');

    divSansVol.setAttribute("style", "background-color: white)");
    divAvecVol.setAttribute("style", "background-color: rgb(238, 234, 234)");

    this.transport = true;
  }

  miseAjourStatutVilleDepartFalse() {
    this.villeDepartManquante = false;
  }

  /**
   * Permet de créer le formulaire
   */
  initForm() {
    this.devisForm = this.formBuilder.group({
      continentDestination: this.continentParams,
      paysDestination: this.paysDestinationsParams,
      idDestination: this.idDestinationParams,
      transport: [this.transport, Validators.required],
      villeDepartVol: '',
      nbNuits: [this.nbNuits, Validators.required],
      dateDepart: ['', Validators.required],
      nbParticipantsAdulte: this.nbParticipantsAdulte,
      nbParticipantsEnfant: this.nbParticipantsEnfant,
      nbParticipantsBebe: this.nbParticipantsBebe,
      nomClient: ['', Validators.required],
      emailClient: ['', [Validators.required, Validators.email]]
    });
  }

  /**
   * Permet de soumettre le formulaire (objet Devis)
   */
  onSubmitForm() {
    const formValue = this.devisForm.value;

    this.verificationTransport(formValue);

    this.miseAJourChampsFormulaire(formValue, 'nbParticipantsAdulte', this.nbParticipantsAdulte);
    this.miseAJourChampsFormulaire(formValue, 'nbParticipantsEnfant', this.nbParticipantsEnfant);
    this.miseAJourChampsFormulaire(formValue, 'nbParticipantsBebe', this.nbParticipantsBebe);
    this.miseAJourChampsFormulaire(formValue, 'transport', this.transport);
    this.miseAJourChampsFormulaire(formValue, 'nbNuits', this.nbNuits);

    if (this.villeDepartManquante === false) {
      const newDevis = new Devis(
        formValue['continentDestination'],
        formValue['paysDestination'],
        formValue['idDestination'],
        formValue['transport'],
        formValue['villeDepartVol'],
        formValue['nbNuits'],
        formValue['dateDepart'],
        formValue['nbParticipantsAdulte'],
        formValue['nbParticipantsEnfant'],
        formValue['nbParticipantsBebe'],
        formValue['nomClient'],
        formValue['emailClient']
      );

      var nomClient = newDevis.nomClient;

      this.devisService.enregistrerDevis(newDevis)
        .subscribe(
          (response) => {
            this.devisEstEnregistre = response;

            if (this.devisEstEnregistre === true) {
              this.router.navigate(['accueil/continents/' + this.continentParams + '/destinations/' + this.paysDestinationsParams + '/' + this.idDestinationParams + '/devis/' + nomClient]);
            }

          },
          (error) => {
            console.log('Erreur : ' + error);
          }
        );
    }
  }

  // *** Méthodes de mise à jour du nombre de participants via les boutons +/- *** //

  /**
   * Permet de retirer un participant adulte
   */
  retirerAdulte() {
    if (this.nbParticipantsAdulte > 1) {
      this.nbParticipantsAdulte--;
    }
  }

  /**
   * Permet d'ajouter un participant adulte
   */
  ajouterAdulte() {
    if (this.nbParticipantsAdulte < 10) {
      this.nbParticipantsAdulte++;
    }
  }

  /**
   * Permet de retirer un participant enfant
   */
  retirerEnfant() {
    if (this.nbParticipantsEnfant > 0) {
      this.nbParticipantsEnfant--;
    }
  }

  /**
   * Permet d'ajouter un participant enfant
   */
  ajouterEnfant() {
    if (this.nbParticipantsEnfant < 10) {
      this.nbParticipantsEnfant++;
    }
  }

  /**
   * Permet de retirer un participant bébé
   */
  retirerBebe() {
    if (this.nbParticipantsBebe > 0) {
      this.nbParticipantsBebe--;
    }
  }

  /**
   * Permet d'ajouter un participant bébé
   */
  ajouterBebe() {
    if (this.nbParticipantsBebe < 10) {
      this.nbParticipantsBebe++;
    }
  }

  /**
   * Permet de vider la ville de départ si le boolean "transport" est false
   * @param formValue 
   */
  verificationTransport(formValue: any) {

    if (this.transport === true && formValue['villeDepartVol'] === "") {
      this.villeDepartManquante = true;
    }

    if (this.transport === false && formValue !== null) {
      formValue['villeDepartVol'] = '';
    }
  }

  /**
   * Permet de mettre à jour les valeurs des champs du formulaire 
   * @param formValue 
   * @param nomChampsFormValue 
   * @param variableFormulaire 
   */
  miseAJourChampsFormulaire(formValue: any, nomChampsFormValue: any, variableFormulaire: any) {
    if (formValue !== null) {
      formValue[nomChampsFormValue] = variableFormulaire;
    }
  }

}
