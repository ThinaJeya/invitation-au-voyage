
# Invitation au voyage

## README du projet invitation-au-voyage de Pirathina Boisson.

*Invitation au voyage* est un projet full-stack développé en Java / SpringBoot / Postgresql et en Angular pour la simulation d'un site de voyage proposant l'établissement de devis de voyage. 
L’objectif du projet était de proposer une application web optimisée sur une approche MVC pour la manipulation des données (recherche, lecture, création, modification).

Le projet est composé de trois parties décrites ci-dessous :

1.  Back-end

-   Langage : **Java 1.8**
-   Framework : **SpringBoot 2.4.1**
-   Design pattern : MVC (Modèle-Vue-Controller)
-   Outils autres : SonarLint, Postman

2.  Base de données
- Langage : SQL
- SGBD : Postgresql 12.3 (outil d'administration : pgAdmin 4.21)

La construction de la base de données s'est appuyée sur les bonnes pratiques de conception en vigueur (analyse des besoins du projet ; traduction des entités en relation ; détermination des associations entre les relations).

Le modèle relationnel des données de la base de données est le suivant (clés primaires en gras) :

*Destination* (**id_destination**, continent_destination, pays_destination, ville_destination, prix_base_adulte)
*Vol_depart* (**id_vol_depart**, ville_depart, prix_vol_adulte, #id_destination)
*Hotel* (**id_hotel**, nom_hotel, nom_chambre)
*Hebergement* (**id_hebergement**, prix_nuit_adulte, #id_destination, #id_hotel)
*Tarif_duree_hebergement* (**id_duree_hebergement**, duree_hebergement, prix_hebergement, #id_destination)
*Devis* (**id_devis**, continent_destination, pays_destination, nom_client, email_client, date_depart_souhaitee, transport_inclu, ville_depart, nb_nuits_souhaitees, nb_participants_adulte, nb_participants_enfant, nb_participants_bebe, montant_total_base, montant_total_vol, montant_total_hebergement, montant_total_TTC, #id_destination)

3.  Front-End

-   Langages : HTML5 / CSS3 ; TypeScript
-   Librairies / Frameworks : **Angular 11.1.1** (node js 12.9.1 ; npm 6.14.11) ; Bootstrap 4.6.0
-   Outils autres : SonarLint

***********************************************************************************
***Lecture de la vidéo démo du projet invitation-au-voyage.***
***********************************************************************************

Une vidéo de présentation de l'application web (partie client) est disponible dans le répertoire principale du projet sous gitLab. La vidéo peut y être directement lue ou être téléchargée.
Nom du fichier : App_Invitation-au-voyage_video-demo_Pirathina-Boisson.mp4

***********************************************************************************
***Instructions d’import du projet invitation-au-voyage de Pirathina Boisson.***
***********************************************************************************
1.  Import de la base de données

Prérequis : Avoir Postgresql et pgAdmin sur son poste

-   Dans pgAdmin : créer une nouvelle base de données nommée « invitation-au-voyage-db» (nom entré dans application.properties du back-end)
-   Une fois la base créée, ouvrir le query tool sur la base, puis sur open file -> ouvrir le fichier « invitation-au-voyage_SQL-script.sql ».
-   Exécuter le script. La base de données sera créée.

3.  Import du back-end

Prérequis : Avoir un IDE sur son poste (Eclipse), jdk minimum : 1.8

-   Aller sur gitLab : [https://gitlab.com/ThinaJeya/invitation-au-voyage](https://gitlab.com/ThinaJeya/invitation-au-voyage) et télécharger le fichier archive (.zip) ou récupérer le clone du repository : [https://gitlab.com/ThinaJeya/invitation-au-voyage.git](https://gitlab.com/ThinaJeya/invitation-au-voyage.git)
-   Importer le projet dans votre IDE favori.
- Configurer le fichier application.properties du projet : Renseigner l’username et le password de votre profil user Postgresql pour connecter la base de données avec le back-end.
-   Exécuter le code (run).

2.  Import du front-end

Prérequis : Avoir un IDE sur son poste (Visual Studio Code), node.js minimum : 12.9.1, npm minimum : 6.14.11, Angular minimum : 11.1.1

- Ouvrir le répertoire "invitation-au-voyage" du répertoire "front" du projet dans votre IDE favori
- Exécuter le code via la commande "ng serve"
- Dans votre navigateur favori, aller sur la page d'accueil de l'application web :  [http://localhost:4200/accueil/continents](http://localhost:4200/accueil/continents)  (modifier le port d'accès si besoin)

**Vous pouvez naviguer sur l'application web une fois que le code back-end du projet aura été exécuté dans l'IDE.**

Note : Les requêtes Postman associées au projet sont également fournies dans le projet.